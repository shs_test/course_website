This site is a collection of tutorials on web development.

As an administrator, you can edit the site by clicking on the red pencil
icon on the lower left of the screen. This is used to edit page content-- 
including adding links and videos. When you're done editing, you can click the
green button in the lower left to save, or the red button to discard 
your changes.

Known issues: the content editor (ContentTools) was not really built to
work with Meteor and occasionally misbehaves.
