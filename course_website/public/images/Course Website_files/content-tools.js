(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// client/assets/content-tools.js                                      //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
(function () {                                                         // 1
  window.FSM = {};                                                     // 2
                                                                       //
  FSM.Machine = (function () {                                         // 4
    function Machine(context) {                                        // 5
      this.context = context;                                          // 6
      this._stateTransitions = {};                                     // 7
      this._stateTransitionsAny = {};                                  // 8
      this._defaultTransition = null;                                  // 9
      this._initialState = null;                                       // 10
      this._currentState = null;                                       // 11
    }                                                                  //
                                                                       //
    Machine.prototype.addTransition = function (action, state, nextState, callback) {
      if (!nextState) {                                                // 15
        nextState = state;                                             // 16
      }                                                                //
      return this._stateTransitions[[action, state]] = [nextState, callback];
    };                                                                 //
                                                                       //
    Machine.prototype.addTransitions = function (actions, state, nextState, callback) {
      var action, _i, _len, _results;                                  // 22
      if (!nextState) {                                                // 23
        nextState = state;                                             // 24
      }                                                                //
      _results = [];                                                   // 26
      for (_i = 0, _len = actions.length; _i < _len; _i++) {           // 27
        action = actions[_i];                                          // 28
        _results.push(this.addTransition(action, state, nextState, callback));
      }                                                                //
      return _results;                                                 // 31
    };                                                                 //
                                                                       //
    Machine.prototype.addTransitionAny = function (state, nextState, callback) {
      if (!nextState) {                                                // 35
        nextState = state;                                             // 36
      }                                                                //
      return this._stateTransitionsAny[state] = [nextState, callback];
    };                                                                 //
                                                                       //
    Machine.prototype.setDefaultTransition = function (state, callback) {
      return this._defaultTransition = [state, callback];              // 42
    };                                                                 //
                                                                       //
    Machine.prototype.getTransition = function (action, state) {       // 45
      if (this._stateTransitions[[action, state]]) {                   // 46
        return this._stateTransitions[[action, state]];                // 47
      } else if (this._stateTransitionsAny[state]) {                   //
        return this._stateTransitionsAny[state];                       // 49
      } else if (this._defaultTransition) {                            //
        return this._defaultTransition;                                // 51
      }                                                                //
      throw new Error("Transition is undefined: (" + action + ", " + state + ")");
    };                                                                 //
                                                                       //
    Machine.prototype.getCurrentState = function () {                  // 56
      return this._currentState;                                       // 57
    };                                                                 //
                                                                       //
    Machine.prototype.setInitialState = function (state) {             // 60
      this._initialState = state;                                      // 61
      if (!this._currentState) {                                       // 62
        return this.reset();                                           // 63
      }                                                                //
    };                                                                 //
                                                                       //
    Machine.prototype.reset = function () {                            // 67
      return this._currentState = this._initialState;                  // 68
    };                                                                 //
                                                                       //
    Machine.prototype.process = function (action) {                    // 71
      var result;                                                      // 72
      result = this.getTransition(action, this._currentState);         // 73
      if (result[1]) {                                                 // 74
        result[1].call(this.context || (this.context = this), action);
      }                                                                //
      return this._currentState = result[0];                           // 77
    };                                                                 //
                                                                       //
    return Machine;                                                    // 80
  })();                                                                //
}).call(this);                                                         //
                                                                       //
(function () {                                                         // 86
  var ALPHA_CHARS,                                                     // 87
      ALPHA_NUMERIC_CHARS,                                             //
      ATTR_DELIM,                                                      //
      ATTR_ENTITY_DOUBLE_DELIM,                                        //
      ATTR_ENTITY_NO_DELIM,                                            //
      ATTR_ENTITY_SINGLE_DELIM,                                        //
      ATTR_NAME,                                                       //
      ATTR_NAME_FIND_VALUE,                                            //
      ATTR_OR_TAG_END,                                                 //
      ATTR_VALUE_DOUBLE_DELIM,                                         //
      ATTR_VALUE_NO_DELIM,                                             //
      ATTR_VALUE_SINGLE_DELIM,                                         //
      CHAR_OR_ENTITY_OR_TAG,                                           //
      CLOSING_TAG,                                                     //
      ENTITY,                                                          //
      ENTITY_CHARS,                                                    //
      OPENING_TAG,                                                     //
      OPENNING_OR_CLOSING_TAG,                                         //
      TAG_NAME_CLOSING,                                                //
      TAG_NAME_MUST_CLOSE,                                             //
      TAG_NAME_OPENING,                                                //
      TAG_OPENING_SELF_CLOSING,                                        //
      _Parser,                                                         //
      __slice = [].slice,                                              //
      __indexOf = [].indexOf || function (item) {                      //
    for (var i = 0, l = this.length; i < l; i++) {                     // 89
      if (i in this && this[i] === item) return i;                     // 89
    }return -1;                                                        //
  };                                                                   //
                                                                       //
  window.HTMLString = {};                                              // 91
                                                                       //
  HTMLString.String = (function () {                                   // 93
    String._parser = null;                                             // 94
                                                                       //
    function String(html, preserveWhitespace) {                        // 96
      if (preserveWhitespace == null) {                                // 97
        preserveWhitespace = false;                                    // 98
      }                                                                //
      this._preserveWhitespace = preserveWhitespace;                   // 100
      if (html) {                                                      // 101
        if (HTMLString.String._parser === null) {                      // 102
          HTMLString.String._parser = new _Parser();                   // 103
        }                                                              //
        this.characters = HTMLString.String._parser.parse(html, this._preserveWhitespace).characters;
      } else {                                                         //
        this.characters = [];                                          // 107
      }                                                                //
    }                                                                  //
                                                                       //
    String.prototype.isWhitespace = function () {                      // 111
      var c, _i, _len, _ref;                                           // 112
      _ref = this.characters;                                          // 113
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 114
        c = _ref[_i];                                                  // 115
        if (!c.isWhitespace()) {                                       // 116
          return false;                                                // 117
        }                                                              //
      }                                                                //
      return true;                                                     // 120
    };                                                                 //
                                                                       //
    String.prototype.length = function () {                            // 123
      return this.characters.length;                                   // 124
    };                                                                 //
                                                                       //
    String.prototype.preserveWhitespace = function () {                // 127
      return this._preserveWhitespace;                                 // 128
    };                                                                 //
                                                                       //
    String.prototype.capitalize = function () {                        // 131
      var c, newString;                                                // 132
      newString = this.copy();                                         // 133
      if (newString.length()) {                                        // 134
        c = newString.characters[0]._c.toUpperCase();                  // 135
        newString.characters[0]._c = c;                                // 136
      }                                                                //
      return newString;                                                // 138
    };                                                                 //
                                                                       //
    String.prototype.charAt = function (index) {                       // 141
      return this.characters[index].copy();                            // 142
    };                                                                 //
                                                                       //
    String.prototype.concat = function () {                            // 145
      var c, indexChar, inheritFormat, inheritedTags, newString, string, strings, tail, _i, _j, _k, _l, _len, _len1, _len2, _ref, _ref1;
      strings = 2 <= arguments.length ? __slice.call(arguments, 0, _i = arguments.length - 1) : (_i = 0, []), inheritFormat = arguments[_i++];
      if (!(typeof inheritFormat === 'undefined' || typeof inheritFormat === 'boolean')) {
        strings.push(inheritFormat);                                   // 149
        inheritFormat = true;                                          // 150
      }                                                                //
      newString = this.copy();                                         // 152
      for (_j = 0, _len = strings.length; _j < _len; _j++) {           // 153
        string = strings[_j];                                          // 154
        if (string.length === 0) {                                     // 155
          continue;                                                    // 156
        }                                                              //
        tail = string;                                                 // 158
        if (typeof string === 'string') {                              // 159
          tail = new HTMLString.String(string, this._preserveWhitespace);
        }                                                              //
        if (inheritFormat && newString.length()) {                     // 162
          indexChar = newString.charAt(newString.length() - 1);        // 163
          inheritedTags = indexChar.tags();                            // 164
          if (indexChar.isTag()) {                                     // 165
            inheritedTags.shift();                                     // 166
          }                                                            //
          if (typeof string !== 'string') {                            // 168
            tail = tail.copy();                                        // 169
          }                                                            //
          _ref = tail.characters;                                      // 171
          for (_k = 0, _len1 = _ref.length; _k < _len1; _k++) {        // 172
            c = _ref[_k];                                              // 173
            c.addTags.apply(c, inheritedTags);                         // 174
          }                                                            //
        }                                                              //
        _ref1 = tail.characters;                                       // 177
        for (_l = 0, _len2 = _ref1.length; _l < _len2; _l++) {         // 178
          c = _ref1[_l];                                               // 179
          newString.characters.push(c);                                // 180
        }                                                              //
      }                                                                //
      return newString;                                                // 183
    };                                                                 //
                                                                       //
    String.prototype.contains = function (substring) {                 // 186
      var c, found, from, i, _i, _len, _ref;                           // 187
      if (typeof substring === 'string') {                             // 188
        return this.text().indexOf(substring) > -1;                    // 189
      }                                                                //
      from = 0;                                                        // 191
      while (from <= this.length() - substring.length()) {             // 192
        found = true;                                                  // 193
        _ref = substring.characters;                                   // 194
        for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {    // 195
          c = _ref[i];                                                 // 196
          if (!c.eq(this.characters[i + from])) {                      // 197
            found = false;                                             // 198
            break;                                                     // 199
          }                                                            //
        }                                                              //
        if (found) {                                                   // 202
          return true;                                                 // 203
        }                                                              //
        from++;                                                        // 205
      }                                                                //
      return false;                                                    // 207
    };                                                                 //
                                                                       //
    String.prototype.endsWith = function (substring) {                 // 210
      var c, characters, i, _i, _len, _ref;                            // 211
      if (typeof substring === 'string') {                             // 212
        return substring === '' || this.text().slice(-substring.length) === substring;
      }                                                                //
      characters = this.characters.slice().reverse();                  // 215
      _ref = substring.characters.slice().reverse();                   // 216
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {      // 217
        c = _ref[i];                                                   // 218
        if (!c.eq(characters[i])) {                                    // 219
          return false;                                                // 220
        }                                                              //
      }                                                                //
      return true;                                                     // 223
    };                                                                 //
                                                                       //
    String.prototype.format = function () {                            // 226
      var c, from, i, newString, tags, to, _i;                         // 227
      from = arguments[0], to = arguments[1], tags = 3 <= arguments.length ? __slice.call(arguments, 2) : [];
      if (to < 0) {                                                    // 229
        to = this.length() + to + 1;                                   // 230
      }                                                                //
      if (from < 0) {                                                  // 232
        from = this.length() + from;                                   // 233
      }                                                                //
      newString = this.copy();                                         // 235
      for (i = _i = from; from <= to ? _i < to : _i > to; i = from <= to ? ++_i : --_i) {
        c = newString.characters[i];                                   // 237
        c.addTags.apply(c, tags);                                      // 238
      }                                                                //
      return newString;                                                // 240
    };                                                                 //
                                                                       //
    String.prototype.hasTags = function () {                           // 243
      var c, found, strict, tags, _i, _j, _len, _ref;                  // 244
      tags = 2 <= arguments.length ? __slice.call(arguments, 0, _i = arguments.length - 1) : (_i = 0, []), strict = arguments[_i++];
      if (!(typeof strict === 'undefined' || typeof strict === 'boolean')) {
        tags.push(strict);                                             // 247
        strict = false;                                                // 248
      }                                                                //
      found = false;                                                   // 250
      _ref = this.characters;                                          // 251
      for (_j = 0, _len = _ref.length; _j < _len; _j++) {              // 252
        c = _ref[_j];                                                  // 253
        if (c.hasTags.apply(c, tags)) {                                // 254
          found = true;                                                // 255
        } else {                                                       //
          if (strict) {                                                // 257
            return false;                                              // 258
          }                                                            //
        }                                                              //
      }                                                                //
      return found;                                                    // 262
    };                                                                 //
                                                                       //
    String.prototype.html = function () {                              // 265
      var c, closingTag, closingTags, head, html, openHeads, openTag, openTags, tag, _i, _j, _k, _l, _len, _len1, _len2, _len3, _len4, _m, _ref, _ref1, _ref2, _ref3;
      html = '';                                                       // 267
      openTags = [];                                                   // 268
      openHeads = [];                                                  // 269
      closingTags = [];                                                // 270
      _ref = this.characters;                                          // 271
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 272
        c = _ref[_i];                                                  // 273
        closingTags = [];                                              // 274
        _ref1 = openTags.slice().reverse();                            // 275
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {         // 276
          openTag = _ref1[_j];                                         // 277
          closingTags.push(openTag);                                   // 278
          if (!c.hasTags(openTag)) {                                   // 279
            for (_k = 0, _len2 = closingTags.length; _k < _len2; _k++) {
              closingTag = closingTags[_k];                            // 281
              html += closingTag.tail();                               // 282
              openTags.pop();                                          // 283
              openHeads.pop();                                         // 284
            }                                                          //
            closingTags = [];                                          // 286
          }                                                            //
        }                                                              //
        _ref2 = c._tags;                                               // 289
        for (_l = 0, _len3 = _ref2.length; _l < _len3; _l++) {         // 290
          tag = _ref2[_l];                                             // 291
          if (openHeads.indexOf(tag.head()) === -1) {                  // 292
            if (!tag.selfClosing()) {                                  // 293
              head = tag.head();                                       // 294
              html += head;                                            // 295
              openTags.push(tag);                                      // 296
              openHeads.push(head);                                    // 297
            }                                                          //
          }                                                            //
        }                                                              //
        if (c._tags.length > 0 && c._tags[0].selfClosing()) {          // 301
          html += c._tags[0].head();                                   // 302
        }                                                              //
        html += c.c();                                                 // 304
      }                                                                //
      _ref3 = openTags.reverse();                                      // 306
      for (_m = 0, _len4 = _ref3.length; _m < _len4; _m++) {           // 307
        tag = _ref3[_m];                                               // 308
        html += tag.tail();                                            // 309
      }                                                                //
      return html;                                                     // 311
    };                                                                 //
                                                                       //
    String.prototype.indexOf = function (substring, from) {            // 314
      var c, found, i, _i, _len, _ref;                                 // 315
      if (from == null) {                                              // 316
        from = 0;                                                      // 317
      }                                                                //
      if (from < 0) {                                                  // 319
        from = 0;                                                      // 320
      }                                                                //
      if (typeof substring === 'string') {                             // 322
        return this.text().indexOf(substring, from);                   // 323
      }                                                                //
      while (from <= this.length() - substring.length()) {             // 325
        found = true;                                                  // 326
        _ref = substring.characters;                                   // 327
        for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {    // 328
          c = _ref[i];                                                 // 329
          if (!c.eq(this.characters[i + from])) {                      // 330
            found = false;                                             // 331
            break;                                                     // 332
          }                                                            //
        }                                                              //
        if (found) {                                                   // 335
          return from;                                                 // 336
        }                                                              //
        from++;                                                        // 338
      }                                                                //
      return -1;                                                       // 340
    };                                                                 //
                                                                       //
    String.prototype.insert = function (index, substring, inheritFormat) {
      var c, head, indexChar, inheritedTags, middle, newString, tail, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2;
      if (inheritFormat == null) {                                     // 345
        inheritFormat = true;                                          // 346
      }                                                                //
      head = this.slice(0, index);                                     // 348
      tail = this.slice(index);                                        // 349
      if (index < 0) {                                                 // 350
        index = this.length() + index;                                 // 351
      }                                                                //
      middle = substring;                                              // 353
      if (typeof substring === 'string') {                             // 354
        middle = new HTMLString.String(substring, this._preserveWhitespace);
      }                                                                //
      if (inheritFormat && index > 0) {                                // 357
        indexChar = this.charAt(index - 1);                            // 358
        inheritedTags = indexChar.tags();                              // 359
        if (indexChar.isTag()) {                                       // 360
          inheritedTags.shift();                                       // 361
        }                                                              //
        if (typeof substring !== 'string') {                           // 363
          middle = middle.copy();                                      // 364
        }                                                              //
        _ref = middle.characters;                                      // 366
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 367
          c = _ref[_i];                                                // 368
          c.addTags.apply(c, inheritedTags);                           // 369
        }                                                              //
      }                                                                //
      newString = head;                                                // 372
      _ref1 = middle.characters;                                       // 373
      for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {           // 374
        c = _ref1[_j];                                                 // 375
        newString.characters.push(c);                                  // 376
      }                                                                //
      _ref2 = tail.characters;                                         // 378
      for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {           // 379
        c = _ref2[_k];                                                 // 380
        newString.characters.push(c);                                  // 381
      }                                                                //
      return newString;                                                // 383
    };                                                                 //
                                                                       //
    String.prototype.lastIndexOf = function (substring, from) {        // 386
      var c, characters, found, i, skip, _i, _j, _len, _len1;          // 387
      if (from == null) {                                              // 388
        from = 0;                                                      // 389
      }                                                                //
      if (from < 0) {                                                  // 391
        from = 0;                                                      // 392
      }                                                                //
      characters = this.characters.slice(from).reverse();              // 394
      from = 0;                                                        // 395
      if (typeof substring === 'string') {                             // 396
        if (!this.contains(substring)) {                               // 397
          return -1;                                                   // 398
        }                                                              //
        substring = substring.split('').reverse();                     // 400
        while (from <= characters.length - substring.length) {         // 401
          found = true;                                                // 402
          skip = 0;                                                    // 403
          for (i = _i = 0, _len = substring.length; _i < _len; i = ++_i) {
            c = substring[i];                                          // 405
            if (characters[i + from].isTag()) {                        // 406
              skip += 1;                                               // 407
            }                                                          //
            if (c !== characters[skip + i + from].c()) {               // 409
              found = false;                                           // 410
              break;                                                   // 411
            }                                                          //
          }                                                            //
          if (found) {                                                 // 414
            return from;                                               // 415
          }                                                            //
          from++;                                                      // 417
        }                                                              //
        return -1;                                                     // 419
      }                                                                //
      substring = substring.characters.slice().reverse();              // 421
      while (from <= characters.length - substring.length) {           // 422
        found = true;                                                  // 423
        for (i = _j = 0, _len1 = substring.length; _j < _len1; i = ++_j) {
          c = substring[i];                                            // 425
          if (!c.eq(characters[i + from])) {                           // 426
            found = false;                                             // 427
            break;                                                     // 428
          }                                                            //
        }                                                              //
        if (found) {                                                   // 431
          return from;                                                 // 432
        }                                                              //
        from++;                                                        // 434
      }                                                                //
      return -1;                                                       // 436
    };                                                                 //
                                                                       //
    String.prototype.optimize = function () {                          // 439
      var c, closingTag, closingTags, head, lastC, len, openHeads, openTag, openTags, runLength, runLengthSort, runLengths, run_length, t, tag, _i, _j, _k, _l, _len, _len1, _len2, _len3, _len4, _len5, _len6, _m, _n, _o, _ref, _ref1, _ref2, _ref3, _ref4, _ref5, _results;
      openTags = [];                                                   // 441
      openHeads = [];                                                  // 442
      lastC = null;                                                    // 443
      _ref = this.characters.slice().reverse();                        // 444
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 445
        c = _ref[_i];                                                  // 446
        c._runLengthMap = {};                                          // 447
        c._runLengthMapSize = 0;                                       // 448
        closingTags = [];                                              // 449
        _ref1 = openTags.slice().reverse();                            // 450
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {         // 451
          openTag = _ref1[_j];                                         // 452
          closingTags.push(openTag);                                   // 453
          if (!c.hasTags(openTag)) {                                   // 454
            for (_k = 0, _len2 = closingTags.length; _k < _len2; _k++) {
              closingTag = closingTags[_k];                            // 456
              openTags.pop();                                          // 457
              openHeads.pop();                                         // 458
            }                                                          //
            closingTags = [];                                          // 460
          }                                                            //
        }                                                              //
        _ref2 = c._tags;                                               // 463
        for (_l = 0, _len3 = _ref2.length; _l < _len3; _l++) {         // 464
          tag = _ref2[_l];                                             // 465
          if (openHeads.indexOf(tag.head()) === -1) {                  // 466
            if (!tag.selfClosing()) {                                  // 467
              openTags.push(tag);                                      // 468
              openHeads.push(tag.head());                              // 469
            }                                                          //
          }                                                            //
        }                                                              //
        for (_m = 0, _len4 = openTags.length; _m < _len4; _m++) {      // 473
          tag = openTags[_m];                                          // 474
          head = tag.head();                                           // 475
          if (!lastC) {                                                // 476
            c._runLengthMap[head] = [tag, 1];                          // 477
            continue;                                                  // 478
          }                                                            //
          if (!c._runLengthMap[head]) {                                // 480
            c._runLengthMap[head] = [tag, 0];                          // 481
          }                                                            //
          run_length = 0;                                              // 483
          if (lastC._runLengthMap[head]) {                             // 484
            run_length = lastC._runLengthMap[head][1];                 // 485
          }                                                            //
          c._runLengthMap[head][1] = run_length + 1;                   // 487
        }                                                              //
        lastC = c;                                                     // 489
      }                                                                //
      runLengthSort = function (a, b) {                                // 491
        return b[1] - a[1];                                            // 492
      };                                                               //
      _ref3 = this.characters;                                         // 494
      _results = [];                                                   // 495
      for (_n = 0, _len5 = _ref3.length; _n < _len5; _n++) {           // 496
        c = _ref3[_n];                                                 // 497
        len = c._tags.length;                                          // 498
        if (len > 0 && c._tags[0].selfClosing() && len < 3 || len < 2) {
          continue;                                                    // 500
        }                                                              //
        runLengths = [];                                               // 502
        _ref4 = c._runLengthMap;                                       // 503
        for (tag in babelHelpers.sanitizeForInObject(_ref4)) {         // 504
          runLength = _ref4[tag];                                      // 505
          runLengths.push(runLength);                                  // 506
        }                                                              //
        runLengths.sort(runLengthSort);                                // 508
        _ref5 = c._tags.slice();                                       // 509
        for (_o = 0, _len6 = _ref5.length; _o < _len6; _o++) {         // 510
          tag = _ref5[_o];                                             // 511
          if (!tag.selfClosing()) {                                    // 512
            c.removeTags(tag);                                         // 513
          }                                                            //
        }                                                              //
        _results.push(c.addTags.apply(c, (function () {                // 516
          var _len7, _p, _results1;                                    // 517
          _results1 = [];                                              // 518
          for (_p = 0, _len7 = runLengths.length; _p < _len7; _p++) {  // 519
            t = runLengths[_p];                                        // 520
            _results1.push(t[0]);                                      // 521
          }                                                            //
          return _results1;                                            // 523
        })()));                                                        //
      }                                                                //
      return _results;                                                 // 526
    };                                                                 //
                                                                       //
    String.prototype.slice = function (from, to) {                     // 529
      var c, newString;                                                // 530
      newString = new HTMLString.String('', this._preserveWhitespace);
      newString.characters = (function () {                            // 532
        var _i, _len, _ref, _results;                                  // 533
        _ref = this.characters.slice(from, to);                        // 534
        _results = [];                                                 // 535
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 536
          c = _ref[_i];                                                // 537
          _results.push(c.copy());                                     // 538
        }                                                              //
        return _results;                                               // 540
      }).call(this);                                                   //
      return newString;                                                // 542
    };                                                                 //
                                                                       //
    String.prototype.split = function (separator, limit) {             // 545
      var count, i, index, indexes, lastIndex, substrings, _i, _ref;   // 546
      if (separator == null) {                                         // 547
        separator = '';                                                // 548
      }                                                                //
      if (limit == null) {                                             // 550
        limit = 0;                                                     // 551
      }                                                                //
      lastIndex = 0;                                                   // 553
      count = 0;                                                       // 554
      indexes = [0];                                                   // 555
      while (true) {                                                   // 556
        if (limit > 0 && count > limit) {                              // 557
          break;                                                       // 558
        }                                                              //
        index = this.indexOf(separator, lastIndex);                    // 560
        if (index === -1 || index === this.length() - 1) {             // 561
          break;                                                       // 562
        }                                                              //
        indexes.push(index);                                           // 564
        lastIndex = index + 1;                                         // 565
      }                                                                //
      indexes.push(this.length());                                     // 567
      substrings = [];                                                 // 568
      for (i = _i = 0, _ref = indexes.length - 2; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        substrings.push(this.slice(indexes[i], indexes[i + 1]));       // 570
      }                                                                //
      return substrings;                                               // 572
    };                                                                 //
                                                                       //
    String.prototype.startsWith = function (substring) {               // 575
      var c, i, _i, _len, _ref;                                        // 576
      if (typeof substring === 'string') {                             // 577
        return this.text().slice(0, substring.length) === substring;   // 578
      }                                                                //
      _ref = substring.characters;                                     // 580
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {      // 581
        c = _ref[i];                                                   // 582
        if (!c.eq(this.characters[i])) {                               // 583
          return false;                                                // 584
        }                                                              //
      }                                                                //
      return true;                                                     // 587
    };                                                                 //
                                                                       //
    String.prototype.substr = function (from, length) {                // 590
      if (length <= 0) {                                               // 591
        return new HTMLString.String('', this._preserveWhitespace);    // 592
      }                                                                //
      if (from < 0) {                                                  // 594
        from = this.length() + from;                                   // 595
      }                                                                //
      if (length === void 0) {                                         // 597
        length = this.length() - from;                                 // 598
      }                                                                //
      return this.slice(from, from + length);                          // 600
    };                                                                 //
                                                                       //
    String.prototype.substring = function (from, to) {                 // 603
      if (to === void 0) {                                             // 604
        to = this.length();                                            // 605
      }                                                                //
      return this.slice(from, to);                                     // 607
    };                                                                 //
                                                                       //
    String.prototype.text = function () {                              // 610
      var c, text, _i, _len, _ref;                                     // 611
      text = '';                                                       // 612
      _ref = this.characters;                                          // 613
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 614
        c = _ref[_i];                                                  // 615
        if (c.isTag()) {                                               // 616
          if (c.isTag('br')) {                                         // 617
            text += '\n';                                              // 618
          }                                                            //
          continue;                                                    // 620
        }                                                              //
        if (c.c() === '&nbsp;') {                                      // 622
          text += c.c();                                               // 623
          continue;                                                    // 624
        }                                                              //
        text += c.c();                                                 // 626
      }                                                                //
      return this.constructor.decode(text);                            // 628
    };                                                                 //
                                                                       //
    String.prototype.toLowerCase = function () {                       // 631
      var c, newString, _i, _len, _ref;                                // 632
      newString = this.copy();                                         // 633
      _ref = newString.characters;                                     // 634
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 635
        c = _ref[_i];                                                  // 636
        if (c._c.length === 1) {                                       // 637
          c._c = c._c.toLowerCase();                                   // 638
        }                                                              //
      }                                                                //
      return newString;                                                // 641
    };                                                                 //
                                                                       //
    String.prototype.toUpperCase = function () {                       // 644
      var c, newString, _i, _len, _ref;                                // 645
      newString = this.copy();                                         // 646
      _ref = newString.characters;                                     // 647
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 648
        c = _ref[_i];                                                  // 649
        if (c._c.length === 1) {                                       // 650
          c._c = c._c.toUpperCase();                                   // 651
        }                                                              //
      }                                                                //
      return newString;                                                // 654
    };                                                                 //
                                                                       //
    String.prototype.trim = function () {                              // 657
      var c, from, newString, to, _i, _j, _len, _len1, _ref, _ref1;    // 658
      _ref = this.characters;                                          // 659
      for (from = _i = 0, _len = _ref.length; _i < _len; from = ++_i) {
        c = _ref[from];                                                // 661
        if (!c.isWhitespace()) {                                       // 662
          break;                                                       // 663
        }                                                              //
      }                                                                //
      _ref1 = this.characters.slice().reverse();                       // 666
      for (to = _j = 0, _len1 = _ref1.length; _j < _len1; to = ++_j) {
        c = _ref1[to];                                                 // 668
        if (!c.isWhitespace()) {                                       // 669
          break;                                                       // 670
        }                                                              //
      }                                                                //
      to = this.length() - to - 1;                                     // 673
      newString = new HTMLString.String('', this._preserveWhitespace);
      newString.characters = (function () {                            // 675
        var _k, _len2, _ref2, _results;                                // 676
        _ref2 = this.characters.slice(from, +to + 1 || 9e9);           // 677
        _results = [];                                                 // 678
        for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {         // 679
          c = _ref2[_k];                                               // 680
          _results.push(c.copy());                                     // 681
        }                                                              //
        return _results;                                               // 683
      }).call(this);                                                   //
      return newString;                                                // 685
    };                                                                 //
                                                                       //
    String.prototype.trimLeft = function () {                          // 688
      var c, from, newString, to, _i, _len, _ref;                      // 689
      to = this.length() - 1;                                          // 690
      _ref = this.characters;                                          // 691
      for (from = _i = 0, _len = _ref.length; _i < _len; from = ++_i) {
        c = _ref[from];                                                // 693
        if (!c.isWhitespace()) {                                       // 694
          break;                                                       // 695
        }                                                              //
      }                                                                //
      newString = new HTMLString.String('', this._preserveWhitespace);
      newString.characters = (function () {                            // 699
        var _j, _len1, _ref1, _results;                                // 700
        _ref1 = this.characters.slice(from, +to + 1 || 9e9);           // 701
        _results = [];                                                 // 702
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {         // 703
          c = _ref1[_j];                                               // 704
          _results.push(c.copy());                                     // 705
        }                                                              //
        return _results;                                               // 707
      }).call(this);                                                   //
      return newString;                                                // 709
    };                                                                 //
                                                                       //
    String.prototype.trimRight = function () {                         // 712
      var c, from, newString, to, _i, _len, _ref;                      // 713
      from = 0;                                                        // 714
      _ref = this.characters.slice().reverse();                        // 715
      for (to = _i = 0, _len = _ref.length; _i < _len; to = ++_i) {    // 716
        c = _ref[to];                                                  // 717
        if (!c.isWhitespace()) {                                       // 718
          break;                                                       // 719
        }                                                              //
      }                                                                //
      to = this.length() - to - 1;                                     // 722
      newString = new HTMLString.String('', this._preserveWhitespace);
      newString.characters = (function () {                            // 724
        var _j, _len1, _ref1, _results;                                // 725
        _ref1 = this.characters.slice(from, +to + 1 || 9e9);           // 726
        _results = [];                                                 // 727
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {         // 728
          c = _ref1[_j];                                               // 729
          _results.push(c.copy());                                     // 730
        }                                                              //
        return _results;                                               // 732
      }).call(this);                                                   //
      return newString;                                                // 734
    };                                                                 //
                                                                       //
    String.prototype.unformat = function () {                          // 737
      var c, from, i, newString, tags, to, _i;                         // 738
      from = arguments[0], to = arguments[1], tags = 3 <= arguments.length ? __slice.call(arguments, 2) : [];
      if (to < 0) {                                                    // 740
        to = this.length() + to + 1;                                   // 741
      }                                                                //
      if (from < 0) {                                                  // 743
        from = this.length() + from;                                   // 744
      }                                                                //
      newString = this.copy();                                         // 746
      for (i = _i = from; from <= to ? _i < to : _i > to; i = from <= to ? ++_i : --_i) {
        c = newString.characters[i];                                   // 748
        c.removeTags.apply(c, tags);                                   // 749
      }                                                                //
      return newString;                                                // 751
    };                                                                 //
                                                                       //
    String.prototype.copy = function () {                              // 754
      var c, stringCopy;                                               // 755
      stringCopy = new HTMLString.String('', this._preserveWhitespace);
      stringCopy.characters = (function () {                           // 757
        var _i, _len, _ref, _results;                                  // 758
        _ref = this.characters;                                        // 759
        _results = [];                                                 // 760
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 761
          c = _ref[_i];                                                // 762
          _results.push(c.copy());                                     // 763
        }                                                              //
        return _results;                                               // 765
      }).call(this);                                                   //
      return stringCopy;                                               // 767
    };                                                                 //
                                                                       //
    String.encode = function (string) {                                // 770
      var textarea;                                                    // 771
      textarea = document.createElement('textarea');                   // 772
      textarea.textContent = string;                                   // 773
      return textarea.innerHTML;                                       // 774
    };                                                                 //
                                                                       //
    String.decode = function (string) {                                // 777
      var textarea;                                                    // 778
      textarea = document.createElement('textarea');                   // 779
      textarea.innerHTML = string;                                     // 780
      return textarea.textContent;                                     // 781
    };                                                                 //
                                                                       //
    return String;                                                     // 784
  })();                                                                //
                                                                       //
  ALPHA_CHARS = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz-_$'.split('');
                                                                       //
  ALPHA_NUMERIC_CHARS = ALPHA_CHARS.concat('1234567890'.split(''));    // 790
                                                                       //
  ENTITY_CHARS = ALPHA_NUMERIC_CHARS.concat(['#']);                    // 792
                                                                       //
  CHAR_OR_ENTITY_OR_TAG = 1;                                           // 794
                                                                       //
  ENTITY = 2;                                                          // 796
                                                                       //
  OPENNING_OR_CLOSING_TAG = 3;                                         // 798
                                                                       //
  OPENING_TAG = 4;                                                     // 800
                                                                       //
  CLOSING_TAG = 5;                                                     // 802
                                                                       //
  TAG_NAME_OPENING = 6;                                                // 804
                                                                       //
  TAG_NAME_CLOSING = 7;                                                // 806
                                                                       //
  TAG_OPENING_SELF_CLOSING = 8;                                        // 808
                                                                       //
  TAG_NAME_MUST_CLOSE = 9;                                             // 810
                                                                       //
  ATTR_OR_TAG_END = 10;                                                // 812
                                                                       //
  ATTR_NAME = 11;                                                      // 814
                                                                       //
  ATTR_NAME_FIND_VALUE = 12;                                           // 816
                                                                       //
  ATTR_DELIM = 13;                                                     // 818
                                                                       //
  ATTR_VALUE_SINGLE_DELIM = 14;                                        // 820
                                                                       //
  ATTR_VALUE_DOUBLE_DELIM = 15;                                        // 822
                                                                       //
  ATTR_VALUE_NO_DELIM = 16;                                            // 824
                                                                       //
  ATTR_ENTITY_NO_DELIM = 17;                                           // 826
                                                                       //
  ATTR_ENTITY_SINGLE_DELIM = 18;                                       // 828
                                                                       //
  ATTR_ENTITY_DOUBLE_DELIM = 19;                                       // 830
                                                                       //
  _Parser = (function () {                                             // 832
    function _Parser() {                                               // 833
      this.fsm = new FSM.Machine(this);                                // 834
      this.fsm.setInitialState(CHAR_OR_ENTITY_OR_TAG);                 // 835
      this.fsm.addTransitionAny(CHAR_OR_ENTITY_OR_TAG, null, function (c) {
        return this._pushChar(c);                                      // 837
      });                                                              //
      this.fsm.addTransition('<', CHAR_OR_ENTITY_OR_TAG, OPENNING_OR_CLOSING_TAG);
      this.fsm.addTransition('&', CHAR_OR_ENTITY_OR_TAG, ENTITY);      // 840
      this.fsm.addTransitions(ENTITY_CHARS, ENTITY, null, function (c) {
        return this.entity += c;                                       // 842
      });                                                              //
      this.fsm.addTransition(';', ENTITY, CHAR_OR_ENTITY_OR_TAG, function () {
        this._pushChar("&" + this.entity + ";");                       // 845
        return this.entity = '';                                       // 846
      });                                                              //
      this.fsm.addTransitions([' ', '\n'], OPENNING_OR_CLOSING_TAG);   // 848
      this.fsm.addTransitions(ALPHA_CHARS, OPENNING_OR_CLOSING_TAG, OPENING_TAG, function () {
        return this._back();                                           // 850
      });                                                              //
      this.fsm.addTransition('/', OPENNING_OR_CLOSING_TAG, CLOSING_TAG);
      this.fsm.addTransitions([' ', '\n'], OPENING_TAG);               // 853
      this.fsm.addTransitions(ALPHA_CHARS, OPENING_TAG, TAG_NAME_OPENING, function () {
        return this._back();                                           // 855
      });                                                              //
      this.fsm.addTransitions([' ', '\n'], CLOSING_TAG);               // 857
      this.fsm.addTransitions(ALPHA_CHARS, CLOSING_TAG, TAG_NAME_CLOSING, function () {
        return this._back();                                           // 859
      });                                                              //
      this.fsm.addTransitions(ALPHA_NUMERIC_CHARS, TAG_NAME_OPENING, null, function (c) {
        return this.tagName += c;                                      // 862
      });                                                              //
      this.fsm.addTransitions([' ', '\n'], TAG_NAME_OPENING, ATTR_OR_TAG_END);
      this.fsm.addTransition('/', TAG_NAME_OPENING, TAG_OPENING_SELF_CLOSING, function () {
        return this.selfClosing = true;                                // 866
      });                                                              //
      this.fsm.addTransition('>', TAG_NAME_OPENING, CHAR_OR_ENTITY_OR_TAG, function () {
        return this._pushTag();                                        // 869
      });                                                              //
      this.fsm.addTransitions([' ', '\n'], TAG_OPENING_SELF_CLOSING);  // 871
      this.fsm.addTransition('>', TAG_OPENING_SELF_CLOSING, CHAR_OR_ENTITY_OR_TAG, function () {
        return this._pushTag();                                        // 873
      });                                                              //
      this.fsm.addTransitions([' ', '\n'], ATTR_OR_TAG_END);           // 875
      this.fsm.addTransition('/', ATTR_OR_TAG_END, TAG_OPENING_SELF_CLOSING, function () {
        return this.selfClosing = true;                                // 877
      });                                                              //
      this.fsm.addTransition('>', ATTR_OR_TAG_END, CHAR_OR_ENTITY_OR_TAG, function () {
        return this._pushTag();                                        // 880
      });                                                              //
      this.fsm.addTransitions(ALPHA_CHARS, ATTR_OR_TAG_END, ATTR_NAME, function () {
        return this._back();                                           // 883
      });                                                              //
      this.fsm.addTransitions(ALPHA_NUMERIC_CHARS, TAG_NAME_CLOSING, null, function (c) {
        return this.tagName += c;                                      // 886
      });                                                              //
      this.fsm.addTransitions([' ', '\n'], TAG_NAME_CLOSING, TAG_NAME_MUST_CLOSE);
      this.fsm.addTransition('>', TAG_NAME_CLOSING, CHAR_OR_ENTITY_OR_TAG, function () {
        return this._popTag();                                         // 890
      });                                                              //
      this.fsm.addTransitions([' ', '\n'], TAG_NAME_MUST_CLOSE);       // 892
      this.fsm.addTransition('>', TAG_NAME_MUST_CLOSE, CHAR_OR_ENTITY_OR_TAG, function () {
        return this._popTag();                                         // 894
      });                                                              //
      this.fsm.addTransitions(ALPHA_NUMERIC_CHARS, ATTR_NAME, null, function (c) {
        return this.attributeName += c;                                // 897
      });                                                              //
      this.fsm.addTransitions([' ', '\n'], ATTR_NAME, ATTR_NAME_FIND_VALUE);
      this.fsm.addTransition('=', ATTR_NAME, ATTR_DELIM);              // 900
      this.fsm.addTransitions([' ', '\n'], ATTR_NAME_FIND_VALUE);      // 901
      this.fsm.addTransition('=', ATTR_NAME_FIND_VALUE, ATTR_DELIM);   // 902
      this.fsm.addTransitions('>', ATTR_NAME, ATTR_OR_TAG_END, function () {
        this._pushAttribute();                                         // 904
        return this._back();                                           // 905
      });                                                              //
      this.fsm.addTransitionAny(ATTR_NAME_FIND_VALUE, ATTR_OR_TAG_END, function () {
        this._pushAttribute();                                         // 908
        return this._back();                                           // 909
      });                                                              //
      this.fsm.addTransitions([' ', '\n'], ATTR_DELIM);                // 911
      this.fsm.addTransition('\'', ATTR_DELIM, ATTR_VALUE_SINGLE_DELIM);
      this.fsm.addTransition('"', ATTR_DELIM, ATTR_VALUE_DOUBLE_DELIM);
      this.fsm.addTransitions(ALPHA_NUMERIC_CHARS.concat(['&'], ATTR_DELIM, ATTR_VALUE_NO_DELIM, function () {
        return this._back();                                           // 915
      }));                                                             //
      this.fsm.addTransition(' ', ATTR_VALUE_NO_DELIM, ATTR_OR_TAG_END, function () {
        return this._pushAttribute();                                  // 918
      });                                                              //
      this.fsm.addTransitions(['/', '>'], ATTR_VALUE_NO_DELIM, ATTR_OR_TAG_END, function () {
        this._back();                                                  // 921
        return this._pushAttribute();                                  // 922
      });                                                              //
      this.fsm.addTransition('&', ATTR_VALUE_NO_DELIM, ATTR_ENTITY_NO_DELIM);
      this.fsm.addTransitionAny(ATTR_VALUE_NO_DELIM, null, function (c) {
        return this.attributeValue += c;                               // 926
      });                                                              //
      this.fsm.addTransition('\'', ATTR_VALUE_SINGLE_DELIM, ATTR_OR_TAG_END, function () {
        return this._pushAttribute();                                  // 929
      });                                                              //
      this.fsm.addTransition('&', ATTR_VALUE_SINGLE_DELIM, ATTR_ENTITY_SINGLE_DELIM);
      this.fsm.addTransitionAny(ATTR_VALUE_SINGLE_DELIM, null, function (c) {
        return this.attributeValue += c;                               // 933
      });                                                              //
      this.fsm.addTransition('"', ATTR_VALUE_DOUBLE_DELIM, ATTR_OR_TAG_END, function () {
        return this._pushAttribute();                                  // 936
      });                                                              //
      this.fsm.addTransition('&', ATTR_VALUE_DOUBLE_DELIM, ATTR_ENTITY_DOUBLE_DELIM);
      this.fsm.addTransitionAny(ATTR_VALUE_DOUBLE_DELIM, null, function (c) {
        return this.attributeValue += c;                               // 940
      });                                                              //
      this.fsm.addTransitions(ENTITY_CHARS, ATTR_ENTITY_NO_DELIM, null, function (c) {
        return this.entity += c;                                       // 943
      });                                                              //
      this.fsm.addTransitions(ENTITY_CHARS, ATTR_ENTITY_SINGLE_DELIM, function (c) {
        return this.entity += c;                                       // 946
      });                                                              //
      this.fsm.addTransitions(ENTITY_CHARS, ATTR_ENTITY_DOUBLE_DELIM, null, function (c) {
        return this.entity += c;                                       // 949
      });                                                              //
      this.fsm.addTransition(';', ATTR_ENTITY_NO_DELIM, ATTR_VALUE_NO_DELIM, function () {
        this.attributeValue += "&" + this.entity + ";";                // 952
        return this.entity = '';                                       // 953
      });                                                              //
      this.fsm.addTransition(';', ATTR_ENTITY_SINGLE_DELIM, ATTR_VALUE_SINGLE_DELIM, function () {
        this.attributeValue += "&" + this.entity + ";";                // 956
        return this.entity = '';                                       // 957
      });                                                              //
      this.fsm.addTransition(';', ATTR_ENTITY_DOUBLE_DELIM, ATTR_VALUE_DOUBLE_DELIM, function () {
        this.attributeValue += "&" + this.entity + ";";                // 960
        return this.entity = '';                                       // 961
      });                                                              //
    }                                                                  //
                                                                       //
    _Parser.prototype._back = function () {                            // 965
      return this.head--;                                              // 966
    };                                                                 //
                                                                       //
    _Parser.prototype._pushAttribute = function () {                   // 969
      this.attributes[this.attributeName] = this.attributeValue;       // 970
      this.attributeName = '';                                         // 971
      return this.attributeValue = '';                                 // 972
    };                                                                 //
                                                                       //
    _Parser.prototype._pushChar = function (c) {                       // 975
      var character, lastCharacter;                                    // 976
      character = new HTMLString.Character(c, this.tags);              // 977
      if (this._preserveWhitespace) {                                  // 978
        this.string.characters.push(character);                        // 979
        return;                                                        // 980
      }                                                                //
      if (this.string.length() && !character.isTag() && !character.isEntity() && character.isWhitespace()) {
        lastCharacter = this.string.characters[this.string.length() - 1];
        if (lastCharacter.isWhitespace() && !lastCharacter.isTag() && !lastCharacter.isEntity()) {
          return;                                                      // 985
        }                                                              //
      }                                                                //
      return this.string.characters.push(character);                   // 988
    };                                                                 //
                                                                       //
    _Parser.prototype._pushTag = function () {                         // 991
      var tag, _ref;                                                   // 992
      tag = new HTMLString.Tag(this.tagName, this.attributes);         // 993
      this.tags.push(tag);                                             // 994
      if (tag.selfClosing()) {                                         // 995
        this._pushChar('');                                            // 996
        this.tags.pop();                                               // 997
        if (!this.selfClosed && (_ref = this.tagName, __indexOf.call(HTMLString.Tag.SELF_CLOSING, _ref) >= 0)) {
          this.fsm.reset();                                            // 999
        }                                                              //
      }                                                                //
      this.tagName = '';                                               // 1002
      this.selfClosed = false;                                         // 1003
      return this.attributes = [];                                     // 1004
    };                                                                 //
                                                                       //
    _Parser.prototype._popTag = function () {                          // 1007
      var character, tag;                                              // 1008
      while (true) {                                                   // 1009
        tag = this.tags.pop();                                         // 1010
        if (this.string.length()) {                                    // 1011
          character = this.string.characters[this.string.length() - 1];
          if (!character.isTag() && !character.isEntity() && character.isWhitespace()) {
            character.removeTags(tag);                                 // 1014
          }                                                            //
        }                                                              //
        if (tag.name() === this.tagName.toLowerCase()) {               // 1017
          break;                                                       // 1018
        }                                                              //
      }                                                                //
      return this.tagName = '';                                        // 1021
    };                                                                 //
                                                                       //
    _Parser.prototype.parse = function (html, preserveWhitespace) {    // 1024
      var character, error;                                            // 1025
      this._preserveWhitespace = preserveWhitespace;                   // 1026
      this.reset();                                                    // 1027
      html = this.preprocess(html);                                    // 1028
      this.fsm.parser = this;                                          // 1029
      while (this.head < html.length) {                                // 1030
        character = html[this.head];                                   // 1031
        try {                                                          // 1032
          this.fsm.process(character);                                 // 1033
        } catch (_error) {                                             //
          error = _error;                                              // 1035
          throw new Error("Error at char " + this.head + " >> " + error);
        }                                                              //
        this.head++;                                                   // 1038
      }                                                                //
      return this.string;                                              // 1040
    };                                                                 //
                                                                       //
    _Parser.prototype.preprocess = function (html) {                   // 1043
      html = html.replace(/\r\n/g, '\n').replace(/\r/g, '\n');         // 1044
      html = html.replace(/<!--[\s\S]*?-->/g, '');                     // 1045
      if (!this._preserveWhitespace) {                                 // 1046
        html = html.replace(/\s+/g, ' ');                              // 1047
      }                                                                //
      return html;                                                     // 1049
    };                                                                 //
                                                                       //
    _Parser.prototype.reset = function () {                            // 1052
      this.fsm.reset();                                                // 1053
      this.head = 0;                                                   // 1054
      this.string = new HTMLString.String();                           // 1055
      this.entity = '';                                                // 1056
      this.tags = [];                                                  // 1057
      this.tagName = '';                                               // 1058
      this.selfClosing = false;                                        // 1059
      this.attributes = {};                                            // 1060
      this.attributeName = '';                                         // 1061
      return this.attributeValue = '';                                 // 1062
    };                                                                 //
                                                                       //
    return _Parser;                                                    // 1065
  })();                                                                //
                                                                       //
  HTMLString.Tag = (function () {                                      // 1069
    function Tag(name, attributes) {                                   // 1070
      var k, v;                                                        // 1071
      this._name = name.toLowerCase();                                 // 1072
      this._selfClosing = HTMLString.Tag.SELF_CLOSING[this._name] === true;
      this._head = null;                                               // 1074
      this._attributes = {};                                           // 1075
      for (k in babelHelpers.sanitizeForInObject(attributes)) {        // 1076
        v = attributes[k];                                             // 1077
        this._attributes[k] = v;                                       // 1078
      }                                                                //
    }                                                                  //
                                                                       //
    Tag.SELF_CLOSING = {                                               // 1082
      'area': true,                                                    // 1083
      'base': true,                                                    // 1084
      'br': true,                                                      // 1085
      'hr': true,                                                      // 1086
      'img': true,                                                     // 1087
      'input': true,                                                   // 1088
      'link meta': true,                                               // 1089
      'wbr': true                                                      // 1090
    };                                                                 //
                                                                       //
    Tag.prototype.head = function () {                                 // 1093
      var components, k, v, _ref;                                      // 1094
      if (!this._head) {                                               // 1095
        components = [];                                               // 1096
        _ref = this._attributes;                                       // 1097
        for (k in babelHelpers.sanitizeForInObject(_ref)) {            // 1098
          v = _ref[k];                                                 // 1099
          if (v) {                                                     // 1100
            components.push("" + k + "=\"" + v + "\"");                // 1101
          } else {                                                     //
            components.push("" + k);                                   // 1103
          }                                                            //
        }                                                              //
        components.sort();                                             // 1106
        components.unshift(this._name);                                // 1107
        this._head = "<" + components.join(' ') + ">";                 // 1108
      }                                                                //
      return this._head;                                               // 1110
    };                                                                 //
                                                                       //
    Tag.prototype.name = function () {                                 // 1113
      return this._name;                                               // 1114
    };                                                                 //
                                                                       //
    Tag.prototype.selfClosing = function () {                          // 1117
      return this._selfClosing;                                        // 1118
    };                                                                 //
                                                                       //
    Tag.prototype.tail = function () {                                 // 1121
      if (this._selfClosing) {                                         // 1122
        return '';                                                     // 1123
      }                                                                //
      return "</" + this._name + ">";                                  // 1125
    };                                                                 //
                                                                       //
    Tag.prototype.attr = function (name, value) {                      // 1128
      if (value === void 0) {                                          // 1129
        return this._attributes[name];                                 // 1130
      }                                                                //
      this._attributes[name] = value;                                  // 1132
      return this._head = null;                                        // 1133
    };                                                                 //
                                                                       //
    Tag.prototype.removeAttr = function (name) {                       // 1136
      if (this._attributes[name] === void 0) {                         // 1137
        return;                                                        // 1138
      }                                                                //
      return delete this._attributes[name];                            // 1140
    };                                                                 //
                                                                       //
    Tag.prototype.copy = function () {                                 // 1143
      return new HTMLString.Tag(this._name, this._attributes);         // 1144
    };                                                                 //
                                                                       //
    return Tag;                                                        // 1147
  })();                                                                //
                                                                       //
  HTMLString.Character = (function () {                                // 1151
    function Character(c, tags) {                                      // 1152
      this._c = c;                                                     // 1153
      if (c.length > 1) {                                              // 1154
        this._c = c.toLowerCase();                                     // 1155
      }                                                                //
      this._tags = [];                                                 // 1157
      this.addTags.apply(this, tags);                                  // 1158
    }                                                                  //
                                                                       //
    Character.prototype.c = function () {                              // 1161
      return this._c;                                                  // 1162
    };                                                                 //
                                                                       //
    Character.prototype.isEntity = function () {                       // 1165
      return this._c.length > 1;                                       // 1166
    };                                                                 //
                                                                       //
    Character.prototype.isTag = function (tagName) {                   // 1169
      if (this._tags.length === 0 || !this._tags[0].selfClosing()) {   // 1170
        return false;                                                  // 1171
      }                                                                //
      if (tagName && this._tags[0].name() !== tagName) {               // 1173
        return false;                                                  // 1174
      }                                                                //
      return true;                                                     // 1176
    };                                                                 //
                                                                       //
    Character.prototype.isWhitespace = function () {                   // 1179
      var _ref;                                                        // 1180
      return (_ref = this._c) === ' ' || _ref === '\n' || _ref === '&nbsp;' || this.isTag('br');
    };                                                                 //
                                                                       //
    Character.prototype.tags = function () {                           // 1184
      var t;                                                           // 1185
      return (function () {                                            // 1186
        var _i, _len, _ref, _results;                                  // 1187
        _ref = this._tags;                                             // 1188
        _results = [];                                                 // 1189
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 1190
          t = _ref[_i];                                                // 1191
          _results.push(t.copy());                                     // 1192
        }                                                              //
        return _results;                                               // 1194
      }).call(this);                                                   //
    };                                                                 //
                                                                       //
    Character.prototype.addTags = function () {                        // 1198
      var tag, tags, _i, _len, _results;                               // 1199
      tags = 1 <= arguments.length ? __slice.call(arguments, 0) : [];  // 1200
      _results = [];                                                   // 1201
      for (_i = 0, _len = tags.length; _i < _len; _i++) {              // 1202
        tag = tags[_i];                                                // 1203
        if (tag.selfClosing()) {                                       // 1204
          if (!this.isTag()) {                                         // 1205
            this._tags.unshift(tag.copy());                            // 1206
          }                                                            //
          continue;                                                    // 1208
        }                                                              //
        _results.push(this._tags.push(tag.copy()));                    // 1210
      }                                                                //
      return _results;                                                 // 1212
    };                                                                 //
                                                                       //
    Character.prototype.eq = function (c) {                            // 1215
      var tag, tags, _i, _j, _len, _len1, _ref, _ref1;                 // 1216
      if (this.c() !== c.c()) {                                        // 1217
        return false;                                                  // 1218
      }                                                                //
      if (this._tags.length !== c._tags.length) {                      // 1220
        return false;                                                  // 1221
      }                                                                //
      tags = {};                                                       // 1223
      _ref = this._tags;                                               // 1224
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 1225
        tag = _ref[_i];                                                // 1226
        tags[tag.head()] = true;                                       // 1227
      }                                                                //
      _ref1 = c._tags;                                                 // 1229
      for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {           // 1230
        tag = _ref1[_j];                                               // 1231
        if (!tags[tag.head()]) {                                       // 1232
          return false;                                                // 1233
        }                                                              //
      }                                                                //
      return true;                                                     // 1236
    };                                                                 //
                                                                       //
    Character.prototype.hasTags = function () {                        // 1239
      var tag, tagHeads, tagNames, tags, _i, _j, _len, _len1, _ref;    // 1240
      tags = 1 <= arguments.length ? __slice.call(arguments, 0) : [];  // 1241
      tagNames = {};                                                   // 1242
      tagHeads = {};                                                   // 1243
      _ref = this._tags;                                               // 1244
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 1245
        tag = _ref[_i];                                                // 1246
        tagNames[tag.name()] = true;                                   // 1247
        tagHeads[tag.head()] = true;                                   // 1248
      }                                                                //
      for (_j = 0, _len1 = tags.length; _j < _len1; _j++) {            // 1250
        tag = tags[_j];                                                // 1251
        if (typeof tag === 'string') {                                 // 1252
          if (tagNames[tag] === void 0) {                              // 1253
            return false;                                              // 1254
          }                                                            //
        } else {                                                       //
          if (tagHeads[tag.head()] === void 0) {                       // 1257
            return false;                                              // 1258
          }                                                            //
        }                                                              //
      }                                                                //
      return true;                                                     // 1262
    };                                                                 //
                                                                       //
    Character.prototype.removeTags = function () {                     // 1265
      var heads, names, newTags, tag, tags, _i, _len;                  // 1266
      tags = 1 <= arguments.length ? __slice.call(arguments, 0) : [];  // 1267
      if (tags.length === 0) {                                         // 1268
        this._tags = [];                                               // 1269
        return;                                                        // 1270
      }                                                                //
      names = {};                                                      // 1272
      heads = {};                                                      // 1273
      for (_i = 0, _len = tags.length; _i < _len; _i++) {              // 1274
        tag = tags[_i];                                                // 1275
        if (typeof tag === 'string') {                                 // 1276
          names[tag] = tag;                                            // 1277
        } else {                                                       //
          heads[tag.head()] = tag;                                     // 1279
        }                                                              //
      }                                                                //
      newTags = [];                                                    // 1282
      return this._tags = this._tags.filter(function (tag) {           // 1283
        if (!heads[tag.head()] && !names[tag.name()]) {                // 1284
          return tag;                                                  // 1285
        }                                                              //
      });                                                              //
    };                                                                 //
                                                                       //
    Character.prototype.copy = function () {                           // 1290
      var t;                                                           // 1291
      return new HTMLString.Character(this._c, (function () {          // 1292
        var _i, _len, _ref, _results;                                  // 1293
        _ref = this._tags;                                             // 1294
        _results = [];                                                 // 1295
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 1296
          t = _ref[_i];                                                // 1297
          _results.push(t.copy());                                     // 1298
        }                                                              //
        return _results;                                               // 1300
      }).call(this));                                                  //
    };                                                                 //
                                                                       //
    return Character;                                                  // 1304
  })();                                                                //
}).call(this);                                                         //
                                                                       //
(function () {                                                         // 1310
  var SELF_CLOSING_NODE_NAMES,                                         // 1311
      _containedBy,                                                    //
      _getChildNodeAndOffset,                                          //
      _getNodeRange,                                                   //
      _getOffsetOfChildNode,                                           //
      __indexOf = [].indexOf || function (item) {                      //
    for (var i = 0, l = this.length; i < l; i++) {                     // 1312
      if (i in this && this[i] === item) return i;                     // 1312
    }return -1;                                                        //
  };                                                                   //
                                                                       //
  window.ContentSelect = {};                                           // 1314
                                                                       //
  ContentSelect.Range = (function () {                                 // 1316
    function Range(from, to) {                                         // 1317
      this.set(from, to);                                              // 1318
    }                                                                  //
                                                                       //
    Range.prototype.isCollapsed = function () {                        // 1321
      return this._from === this._to;                                  // 1322
    };                                                                 //
                                                                       //
    Range.prototype.span = function () {                               // 1325
      return this._to - this._from;                                    // 1326
    };                                                                 //
                                                                       //
    Range.prototype.collapse = function () {                           // 1329
      return this._to = this._from;                                    // 1330
    };                                                                 //
                                                                       //
    Range.prototype.eq = function (range) {                            // 1333
      return this.get()[0] === range.get()[0] && this.get()[1] === range.get()[1];
    };                                                                 //
                                                                       //
    Range.prototype.get = function () {                                // 1337
      return [this._from, this._to];                                   // 1338
    };                                                                 //
                                                                       //
    Range.prototype.select = function (element) {                      // 1341
      var docRange, endNode, endOffset, startNode, startOffset, _ref, _ref1;
      ContentSelect.Range.unselectAll();                               // 1343
      docRange = document.createRange();                               // 1344
      _ref = _getChildNodeAndOffset(element, this._from), startNode = _ref[0], startOffset = _ref[1];
      _ref1 = _getChildNodeAndOffset(element, this._to), endNode = _ref1[0], endOffset = _ref1[1];
      docRange.setStart(startNode, startOffset);                       // 1347
      docRange.setEnd(endNode, endOffset);                             // 1348
      return window.getSelection().addRange(docRange);                 // 1349
    };                                                                 //
                                                                       //
    Range.prototype.set = function (from, to) {                        // 1352
      from = Math.max(0, from);                                        // 1353
      to = Math.max(0, to);                                            // 1354
      this._from = Math.min(from, to);                                 // 1355
      return this._to = Math.max(from, to);                            // 1356
    };                                                                 //
                                                                       //
    Range.prepareElement = function (element) {                        // 1359
      var i, node, selfClosingNodes, _i, _len, _results;               // 1360
      selfClosingNodes = element.querySelectorAll(SELF_CLOSING_NODE_NAMES.join(', '));
      _results = [];                                                   // 1362
      for (i = _i = 0, _len = selfClosingNodes.length; _i < _len; i = ++_i) {
        node = selfClosingNodes[i];                                    // 1364
        node.parentNode.insertBefore(document.createTextNode(''), node);
        if (i < selfClosingNodes.length - 1) {                         // 1366
          _results.push(node.parentNode.insertBefore(document.createTextNode(''), node.nextSibling));
        } else {                                                       //
          _results.push(void 0);                                       // 1369
        }                                                              //
      }                                                                //
      return _results;                                                 // 1372
    };                                                                 //
                                                                       //
    Range.query = function (element) {                                 // 1375
      var docRange, endNode, endOffset, range, startNode, startOffset, _ref;
      range = new ContentSelect.Range(0, 0);                           // 1377
      try {                                                            // 1378
        docRange = window.getSelection().getRangeAt(0);                // 1379
      } catch (_error) {                                               //
        return range;                                                  // 1381
      }                                                                //
      if (element.firstChild === null && element.lastChild === null) {
        return range;                                                  // 1384
      }                                                                //
      if (!_containedBy(docRange.startContainer, element)) {           // 1386
        return range;                                                  // 1387
      }                                                                //
      if (!_containedBy(docRange.endContainer, element)) {             // 1389
        return range;                                                  // 1390
      }                                                                //
      _ref = _getNodeRange(element, docRange), startNode = _ref[0], startOffset = _ref[1], endNode = _ref[2], endOffset = _ref[3];
      range.set(_getOffsetOfChildNode(element, startNode) + startOffset, _getOffsetOfChildNode(element, endNode) + endOffset);
      return range;                                                    // 1394
    };                                                                 //
                                                                       //
    Range.rect = function () {                                         // 1397
      var docRange, marker, rect;                                      // 1398
      try {                                                            // 1399
        docRange = window.getSelection().getRangeAt(0);                // 1400
      } catch (_error) {                                               //
        return null;                                                   // 1402
      }                                                                //
      if (docRange.collapsed) {                                        // 1404
        marker = document.createElement('span');                       // 1405
        docRange.insertNode(marker);                                   // 1406
        rect = marker.getBoundingClientRect();                         // 1407
        marker.parentNode.removeChild(marker);                         // 1408
        return rect;                                                   // 1409
      } else {                                                         //
        return docRange.getBoundingClientRect();                       // 1411
      }                                                                //
    };                                                                 //
                                                                       //
    Range.unselectAll = function () {                                  // 1415
      if (window.getSelection()) {                                     // 1416
        return window.getSelection().removeAllRanges();                // 1417
      }                                                                //
    };                                                                 //
                                                                       //
    return Range;                                                      // 1421
  })();                                                                //
                                                                       //
  SELF_CLOSING_NODE_NAMES = ['br', 'img', 'input'];                    // 1425
                                                                       //
  _containedBy = function (nodeA, nodeB) {                             // 1427
    while (nodeA) {                                                    // 1428
      if (nodeA === nodeB) {                                           // 1429
        return true;                                                   // 1430
      }                                                                //
      nodeA = nodeA.parentNode;                                        // 1432
    }                                                                  //
    return false;                                                      // 1434
  };                                                                   //
                                                                       //
  _getChildNodeAndOffset = function (parentNode, parentOffset) {       // 1437
    var childNode, childOffset, childStack, n, _ref;                   // 1438
    if (parentNode.childNodes.length === 0) {                          // 1439
      return [parentNode, parentOffset];                               // 1440
    }                                                                  //
    childNode = null;                                                  // 1442
    childOffset = parentOffset;                                        // 1443
    childStack = (function () {                                        // 1444
      var _i, _len, _ref, _results;                                    // 1445
      _ref = parentNode.childNodes;                                    // 1446
      _results = [];                                                   // 1447
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 1448
        n = _ref[_i];                                                  // 1449
        _results.push(n);                                              // 1450
      }                                                                //
      return _results;                                                 // 1452
    })();                                                              //
    while (childStack.length > 0) {                                    // 1454
      childNode = childStack.shift();                                  // 1455
      switch (childNode.nodeType) {                                    // 1456
        case Node.TEXT_NODE:                                           // 1457
          if (childNode.textContent.length >= childOffset) {           // 1458
            return [childNode, childOffset];                           // 1459
          }                                                            //
          childOffset -= childNode.textContent.length;                 // 1461
          break;                                                       // 1462
        case Node.ELEMENT_NODE:                                        // 1462
          if ((_ref = childNode.nodeName.toLowerCase(), __indexOf.call(SELF_CLOSING_NODE_NAMES, _ref) >= 0)) {
            if (childOffset === 0) {                                   // 1465
              return [childNode, 0];                                   // 1466
            } else {                                                   //
              childOffset = Math.max(0, childOffset - 1);              // 1468
            }                                                          //
          } else {                                                     //
            if (childNode.childNodes) {                                // 1471
              Array.prototype.unshift.apply(childStack, (function () {
                var _i, _len, _ref1, _results;                         // 1473
                _ref1 = childNode.childNodes;                          // 1474
                _results = [];                                         // 1475
                for (_i = 0, _len = _ref1.length; _i < _len; _i++) {   // 1476
                  n = _ref1[_i];                                       // 1477
                  _results.push(n);                                    // 1478
                }                                                      //
                return _results;                                       // 1480
              })());                                                   //
            }                                                          //
          }                                                            //
      }                                                                // 1483
    }                                                                  //
    return [childNode, childOffset];                                   // 1486
  };                                                                   //
                                                                       //
  _getOffsetOfChildNode = function (parentNode, childNode) {           // 1489
    var childStack, n, offset, otherChildNode, _ref, _ref1;            // 1490
    if (parentNode.childNodes.length === 0) {                          // 1491
      return 0;                                                        // 1492
    }                                                                  //
    offset = 0;                                                        // 1494
    childStack = (function () {                                        // 1495
      var _i, _len, _ref, _results;                                    // 1496
      _ref = parentNode.childNodes;                                    // 1497
      _results = [];                                                   // 1498
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 1499
        n = _ref[_i];                                                  // 1500
        _results.push(n);                                              // 1501
      }                                                                //
      return _results;                                                 // 1503
    })();                                                              //
    while (childStack.length > 0) {                                    // 1505
      otherChildNode = childStack.shift();                             // 1506
      if (otherChildNode === childNode) {                              // 1507
        if ((_ref = otherChildNode.nodeName.toLowerCase(), __indexOf.call(SELF_CLOSING_NODE_NAMES, _ref) >= 0)) {
          return offset + 1;                                           // 1509
        }                                                              //
        return offset;                                                 // 1511
      }                                                                //
      switch (otherChildNode.nodeType) {                               // 1513
        case Node.TEXT_NODE:                                           // 1514
          offset += otherChildNode.textContent.length;                 // 1515
          break;                                                       // 1516
        case Node.ELEMENT_NODE:                                        // 1516
          if ((_ref1 = otherChildNode.nodeName.toLowerCase(), __indexOf.call(SELF_CLOSING_NODE_NAMES, _ref1) >= 0)) {
            offset += 1;                                               // 1519
          } else {                                                     //
            if (otherChildNode.childNodes) {                           // 1521
              Array.prototype.unshift.apply(childStack, (function () {
                var _i, _len, _ref2, _results;                         // 1523
                _ref2 = otherChildNode.childNodes;                     // 1524
                _results = [];                                         // 1525
                for (_i = 0, _len = _ref2.length; _i < _len; _i++) {   // 1526
                  n = _ref2[_i];                                       // 1527
                  _results.push(n);                                    // 1528
                }                                                      //
                return _results;                                       // 1530
              })());                                                   //
            }                                                          //
          }                                                            //
      }                                                                // 1533
    }                                                                  //
    return offset;                                                     // 1536
  };                                                                   //
                                                                       //
  _getNodeRange = function (element, docRange) {                       // 1539
    var childNode, childNodes, endNode, endOffset, endRange, i, startNode, startOffset, startRange, _i, _j, _len, _len1, _ref;
    childNodes = element.childNodes;                                   // 1541
    startRange = docRange.cloneRange();                                // 1542
    startRange.collapse(true);                                         // 1543
    endRange = docRange.cloneRange();                                  // 1544
    endRange.collapse(false);                                          // 1545
    startNode = startRange.startContainer;                             // 1546
    startOffset = startRange.startOffset;                              // 1547
    endNode = endRange.endContainer;                                   // 1548
    endOffset = endRange.endOffset;                                    // 1549
    if (!startRange.comparePoint) {                                    // 1550
      return [startNode, startOffset, endNode, endOffset];             // 1551
    }                                                                  //
    if (startNode === element) {                                       // 1553
      startNode = childNodes[childNodes.length - 1];                   // 1554
      startOffset = startNode.textContent.length;                      // 1555
      for (i = _i = 0, _len = childNodes.length; _i < _len; i = ++_i) {
        childNode = childNodes[i];                                     // 1557
        if (startRange.comparePoint(childNode, 0) !== 1) {             // 1558
          continue;                                                    // 1559
        }                                                              //
        if (i === 0) {                                                 // 1561
          startNode = childNode;                                       // 1562
          startOffset = 0;                                             // 1563
        } else {                                                       //
          startNode = childNodes[i - 1];                               // 1565
          startOffset = childNode.textContent.length;                  // 1566
        }                                                              //
        if ((_ref = startNode.nodeName.toLowerCase, __indexOf.call(SELF_CLOSING_NODE_NAMES, _ref) >= 0)) {
          startOffset = 1;                                             // 1569
        }                                                              //
        break;                                                         // 1571
      }                                                                //
    }                                                                  //
    if (docRange.collapsed) {                                          // 1574
      return [startNode, startOffset, startNode, startOffset];         // 1575
    }                                                                  //
    if (endNode === element) {                                         // 1577
      endNode = childNodes[childNodes.length - 1];                     // 1578
      endOffset = endNode.textContent.length;                          // 1579
      for (i = _j = 0, _len1 = childNodes.length; _j < _len1; i = ++_j) {
        childNode = childNodes[i];                                     // 1581
        if (endRange.comparePoint(childNode, 0) !== 1) {               // 1582
          continue;                                                    // 1583
        }                                                              //
        if (i === 0) {                                                 // 1585
          endNode = childNode;                                         // 1586
        } else {                                                       //
          endNode = childNodes[i - 1];                                 // 1588
        }                                                              //
        endOffset = childNode.textContent.length + 1;                  // 1590
      }                                                                //
    }                                                                  //
    return [startNode, startOffset, endNode, endOffset];               // 1593
  };                                                                   //
}).call(this);                                                         //
                                                                       //
(function () {                                                         // 1598
  var C,                                                               // 1599
      _Root,                                                           //
      _TagNames,                                                       //
      _mergers,                                                        //
      __slice = [].slice,                                              //
      __indexOf = [].indexOf || function (item) {                      //
    for (var i = 0, l = this.length; i < l; i++) {                     // 1601
      if (i in this && this[i] === item) return i;                     // 1601
    }return -1;                                                        //
  },                                                                   //
      __hasProp = ({}).hasOwnProperty,                                 //
      __extends = function (child, parent) {                           //
    for (var key in babelHelpers.sanitizeForInObject(parent)) {        // 1603
      if (__hasProp.call(parent, key)) child[key] = parent[key];       // 1603
    }function ctor() {                                                 //
      this.constructor = child;                                        // 1603
    }ctor.prototype = parent.prototype;child.prototype = new ctor();child.__super__ = parent.prototype;return child;
  },                                                                   //
      __bind = function (fn, me) {                                     //
    return function () {                                               // 1604
      return fn.apply(me, arguments);                                  // 1604
    };                                                                 //
  };                                                                   //
                                                                       //
  window.ContentEdit = {                                               // 1606
    DEFAULT_MAX_ELEMENT_WIDTH: 800,                                    // 1607
    DEFAULT_MIN_ELEMENT_WIDTH: 80,                                     // 1608
    DRAG_HOLD_DURATION: 500,                                           // 1609
    DROP_EDGE_SIZE: 50,                                                // 1610
    HELPER_CHAR_LIMIT: 250,                                            // 1611
    INDENT: '    ',                                                    // 1612
    LANGUAGE: 'en',                                                    // 1613
    RESIZE_CORNER_SIZE: 15,                                            // 1614
    _translations: {},                                                 // 1615
    _: function (s) {                                                  // 1616
      var lang;                                                        // 1617
      lang = ContentEdit.LANGUAGE;                                     // 1618
      if (ContentEdit._translations[lang] && ContentEdit._translations[lang][s]) {
        return ContentEdit._translations[lang][s];                     // 1620
      }                                                                //
      return s;                                                        // 1622
    },                                                                 //
    addTranslations: function (language, translations) {               // 1624
      return ContentEdit._translations[language] = translations;       // 1625
    },                                                                 //
    addCSSClass: function (domElement, className) {                    // 1627
      var c, classAttr, classNames;                                    // 1628
      if (domElement.classList) {                                      // 1629
        domElement.classList.add(className);                           // 1630
        return;                                                        // 1631
      }                                                                //
      classAttr = domElement.getAttribute('class');                    // 1633
      if (classAttr) {                                                 // 1634
        classNames = (function () {                                    // 1635
          var _i, _len, _ref, _results;                                // 1636
          _ref = classAttr.split(' ');                                 // 1637
          _results = [];                                               // 1638
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {          // 1639
            c = _ref[_i];                                              // 1640
            _results.push(c);                                          // 1641
          }                                                            //
          return _results;                                             // 1643
        })();                                                          //
        if (classNames.indexOf(className) === -1) {                    // 1645
          return domElement.setAttribute('class', "" + classAttr + " " + className);
        }                                                              //
      } else {                                                         //
        return domElement.setAttribute('class', className);            // 1649
      }                                                                //
    },                                                                 //
    attributesToString: function (attributes) {                        // 1652
      var attributeStrings, name, names, value, _i, _len;              // 1653
      if (!attributes) {                                               // 1654
        return '';                                                     // 1655
      }                                                                //
      names = (function () {                                           // 1657
        var _results;                                                  // 1658
        _results = [];                                                 // 1659
        for (name in babelHelpers.sanitizeForInObject(attributes)) {   // 1660
          _results.push(name);                                         // 1661
        }                                                              //
        return _results;                                               // 1663
      })();                                                            //
      names.sort();                                                    // 1665
      attributeStrings = [];                                           // 1666
      for (_i = 0, _len = names.length; _i < _len; _i++) {             // 1667
        name = names[_i];                                              // 1668
        value = attributes[name];                                      // 1669
        if (value === '') {                                            // 1670
          attributeStrings.push(name);                                 // 1671
        } else {                                                       //
          attributeStrings.push("" + name + "=\"" + value + "\"");     // 1673
        }                                                              //
      }                                                                //
      return attributeStrings.join(' ');                               // 1676
    },                                                                 //
    removeCSSClass: function (domElement, className) {                 // 1678
      var c, classAttr, classNameIndex, classNames;                    // 1679
      if (domElement.classList) {                                      // 1680
        domElement.classList.remove(className);                        // 1681
        if (domElement.classList.length === 0) {                       // 1682
          domElement.removeAttribute('class');                         // 1683
        }                                                              //
        return;                                                        // 1685
      }                                                                //
      classAttr = domElement.getAttribute('class');                    // 1687
      if (classAttr) {                                                 // 1688
        classNames = (function () {                                    // 1689
          var _i, _len, _ref, _results;                                // 1690
          _ref = classAttr.split(' ');                                 // 1691
          _results = [];                                               // 1692
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {          // 1693
            c = _ref[_i];                                              // 1694
            _results.push(c);                                          // 1695
          }                                                            //
          return _results;                                             // 1697
        })();                                                          //
        classNameIndex = classNames.indexOf(className);                // 1699
        if (classNameIndex > -1) {                                     // 1700
          classNames.splice(classNameIndex, 1);                        // 1701
          if (classNames.length) {                                     // 1702
            return domElement.setAttribute('class', classNames.join(' '));
          } else {                                                     //
            return domElement.removeAttribute('class');                // 1705
          }                                                            //
        }                                                              //
      } else {                                                         //
        return domElement.setAttribute('class', className);            // 1709
      }                                                                //
    }                                                                  //
  };                                                                   //
                                                                       //
  if (!(C = (function () {                                             // 1714
    function C() {}                                                    // 1715
                                                                       //
    return C;                                                          // 1717
  })()).name) {                                                        //
    Object.defineProperty(Function.prototype, 'name', {                // 1720
      get: function () {                                               // 1721
        var name;                                                      // 1722
        name = this.toString().match(/^\s*function\s*(\S*)\s*\(/)[1];  // 1723
        Object.defineProperty(this, 'name', {                          // 1724
          value: name                                                  // 1725
        });                                                            //
        return name;                                                   // 1727
      }                                                                //
    });                                                                //
  }                                                                    //
                                                                       //
  _TagNames = (function () {                                           // 1732
    function _TagNames() {                                             // 1733
      this._tagNames = {};                                             // 1734
    }                                                                  //
                                                                       //
    _TagNames.prototype.register = function () {                       // 1737
      var cls, tagName, tagNames, _i, _len, _results;                  // 1738
      cls = arguments[0], tagNames = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      _results = [];                                                   // 1740
      for (_i = 0, _len = tagNames.length; _i < _len; _i++) {          // 1741
        tagName = tagNames[_i];                                        // 1742
        _results.push(this._tagNames[tagName.toLowerCase()] = cls);    // 1743
      }                                                                //
      return _results;                                                 // 1745
    };                                                                 //
                                                                       //
    _TagNames.prototype.match = function (tagName) {                   // 1748
      if (this._tagNames[tagName.toLowerCase()]) {                     // 1749
        return this._tagNames[tagName.toLowerCase()];                  // 1750
      }                                                                //
      return ContentEdit.Static;                                       // 1752
    };                                                                 //
                                                                       //
    return _TagNames;                                                  // 1755
  })();                                                                //
                                                                       //
  ContentEdit.TagNames = (function () {                                // 1759
    var instance;                                                      // 1760
                                                                       //
    function TagNames() {}                                             // 1762
                                                                       //
    instance = null;                                                   // 1764
                                                                       //
    TagNames.get = function () {                                       // 1766
      return instance != null ? instance : instance = new _TagNames();
    };                                                                 //
                                                                       //
    return TagNames;                                                   // 1770
  })();                                                                //
                                                                       //
  ContentEdit.Node = (function () {                                    // 1774
    function Node() {                                                  // 1775
      this._bindings = {};                                             // 1776
      this._parent = null;                                             // 1777
      this._modified = null;                                           // 1778
    }                                                                  //
                                                                       //
    Node.prototype.lastModified = function () {                        // 1781
      return this._modified;                                           // 1782
    };                                                                 //
                                                                       //
    Node.prototype.parent = function () {                              // 1785
      return this._parent;                                             // 1786
    };                                                                 //
                                                                       //
    Node.prototype.parents = function () {                             // 1789
      var parent, parents;                                             // 1790
      parents = [];                                                    // 1791
      parent = this._parent;                                           // 1792
      while (parent) {                                                 // 1793
        parents.push(parent);                                          // 1794
        parent = parent._parent;                                       // 1795
      }                                                                //
      return parents;                                                  // 1797
    };                                                                 //
                                                                       //
    Node.prototype.html = function (indent) {                          // 1800
      if (indent == null) {                                            // 1801
        indent = '';                                                   // 1802
      }                                                                //
      throw new Error('`html` not implemented');                       // 1804
    };                                                                 //
                                                                       //
    Node.prototype.bind = function (eventName, callback) {             // 1807
      if (this._bindings[eventName] === void 0) {                      // 1808
        this._bindings[eventName] = [];                                // 1809
      }                                                                //
      this._bindings[eventName].push(callback);                        // 1811
      return callback;                                                 // 1812
    };                                                                 //
                                                                       //
    Node.prototype.trigger = function () {                             // 1815
      var args, callback, eventName, _i, _len, _ref, _results;         // 1816
      eventName = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      if (!this._bindings[eventName]) {                                // 1818
        return;                                                        // 1819
      }                                                                //
      _ref = this._bindings[eventName];                                // 1821
      _results = [];                                                   // 1822
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 1823
        callback = _ref[_i];                                           // 1824
        if (!callback) {                                               // 1825
          continue;                                                    // 1826
        }                                                              //
        _results.push(callback.call.apply(callback, [this].concat(__slice.call(args))));
      }                                                                //
      return _results;                                                 // 1830
    };                                                                 //
                                                                       //
    Node.prototype.unbind = function (eventName, callback) {           // 1833
      var i, suspect, _i, _len, _ref, _results;                        // 1834
      if (!eventName) {                                                // 1835
        this._bindings = {};                                           // 1836
        return;                                                        // 1837
      }                                                                //
      if (!callback) {                                                 // 1839
        this._bindings[eventName] = void 0;                            // 1840
        return;                                                        // 1841
      }                                                                //
      if (!this._bindings[eventName]) {                                // 1843
        return;                                                        // 1844
      }                                                                //
      _ref = this._bindings[eventName];                                // 1846
      _results = [];                                                   // 1847
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {      // 1848
        suspect = _ref[i];                                             // 1849
        if (suspect === callback) {                                    // 1850
          _results.push(this._bindings[eventName].splice(i, 1));       // 1851
        } else {                                                       //
          _results.push(void 0);                                       // 1853
        }                                                              //
      }                                                                //
      return _results;                                                 // 1856
    };                                                                 //
                                                                       //
    Node.prototype.commit = function () {                              // 1859
      this._modified = null;                                           // 1860
      return ContentEdit.Root.get().trigger('commit', this);           // 1861
    };                                                                 //
                                                                       //
    Node.prototype.taint = function () {                               // 1864
      var now, parent, root, _i, _len, _ref;                           // 1865
      now = Date.now();                                                // 1866
      this._modified = now;                                            // 1867
      _ref = this.parents();                                           // 1868
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 1869
        parent = _ref[_i];                                             // 1870
        parent._modified = now;                                        // 1871
      }                                                                //
      root = ContentEdit.Root.get();                                   // 1873
      root._modified = now;                                            // 1874
      return root.trigger('taint', this);                              // 1875
    };                                                                 //
                                                                       //
    Node.prototype.closest = function (testFunc) {                     // 1878
      var parent;                                                      // 1879
      parent = this.parent();                                          // 1880
      while (parent && !testFunc(parent)) {                            // 1881
        if (parent.parent) {                                           // 1882
          parent = parent.parent();                                    // 1883
        } else {                                                       //
          parent = null;                                               // 1885
        }                                                              //
      }                                                                //
      return parent;                                                   // 1888
    };                                                                 //
                                                                       //
    Node.prototype.next = function () {                                // 1891
      var children, index, node, _i, _len, _ref;                       // 1892
      if (this.children && this.children.length > 0) {                 // 1893
        return this.children[0];                                       // 1894
      }                                                                //
      _ref = [this].concat(this.parents());                            // 1896
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 1897
        node = _ref[_i];                                               // 1898
        if (!node.parent()) {                                          // 1899
          return null;                                                 // 1900
        }                                                              //
        children = node.parent().children;                             // 1902
        index = children.indexOf(node);                                // 1903
        if (index < children.length - 1) {                             // 1904
          return children[index + 1];                                  // 1905
        }                                                              //
      }                                                                //
    };                                                                 //
                                                                       //
    Node.prototype.nextContent = function () {                         // 1910
      return this.nextWithTest(function (node) {                       // 1911
        return node.content !== void 0;                                // 1912
      });                                                              //
    };                                                                 //
                                                                       //
    Node.prototype.nextSibling = function () {                         // 1916
      var index;                                                       // 1917
      index = this.parent().children.indexOf(this);                    // 1918
      if (index === this.parent().children.length - 1) {               // 1919
        return null;                                                   // 1920
      }                                                                //
      return this.parent().children[index + 1];                        // 1922
    };                                                                 //
                                                                       //
    Node.prototype.nextWithTest = function (testFunc) {                // 1925
      var node;                                                        // 1926
      node = this;                                                     // 1927
      while (node) {                                                   // 1928
        node = node.next();                                            // 1929
        if (node && testFunc(node)) {                                  // 1930
          return node;                                                 // 1931
        }                                                              //
      }                                                                //
    };                                                                 //
                                                                       //
    Node.prototype.previous = function () {                            // 1936
      var children, node;                                              // 1937
      if (!this.parent()) {                                            // 1938
        return null;                                                   // 1939
      }                                                                //
      children = this.parent().children;                               // 1941
      if (children[0] === this) {                                      // 1942
        return this.parent();                                          // 1943
      }                                                                //
      node = children[children.indexOf(this) - 1];                     // 1945
      while (node.children && node.children.length) {                  // 1946
        node = node.children[node.children.length - 1];                // 1947
      }                                                                //
      return node;                                                     // 1949
    };                                                                 //
                                                                       //
    Node.prototype.previousContent = function () {                     // 1952
      var node;                                                        // 1953
      return node = this.previousWithTest(function (node) {            // 1954
        return node.content !== void 0;                                // 1955
      });                                                              //
    };                                                                 //
                                                                       //
    Node.prototype.previousSibling = function () {                     // 1959
      var index;                                                       // 1960
      index = this.parent().children.indexOf(this);                    // 1961
      if (index === 0) {                                               // 1962
        return null;                                                   // 1963
      }                                                                //
      return this.parent().children[index - 1];                        // 1965
    };                                                                 //
                                                                       //
    Node.prototype.previousWithTest = function (testFunc) {            // 1968
      var node;                                                        // 1969
      node = this;                                                     // 1970
      while (node) {                                                   // 1971
        node = node.previous();                                        // 1972
        if (node && testFunc(node)) {                                  // 1973
          return node;                                                 // 1974
        }                                                              //
      }                                                                //
    };                                                                 //
                                                                       //
    Node.extend = function (cls) {                                     // 1979
      var key, value, _ref;                                            // 1980
      _ref = cls.prototype;                                            // 1981
      for (key in babelHelpers.sanitizeForInObject(_ref)) {            // 1982
        value = _ref[key];                                             // 1983
        if (key === 'constructor') {                                   // 1984
          continue;                                                    // 1985
        }                                                              //
        this.prototype[key] = value;                                   // 1987
      }                                                                //
      for (key in babelHelpers.sanitizeForInObject(cls)) {             // 1989
        value = cls[key];                                              // 1990
        if (__indexOf.call('__super__', key) >= 0) {                   // 1991
          continue;                                                    // 1992
        }                                                              //
        this.prototype[key] = value;                                   // 1994
      }                                                                //
      return this;                                                     // 1996
    };                                                                 //
                                                                       //
    Node.fromDOMElement = function (domElement) {                      // 1999
      throw new Error('`fromDOMElement` not implemented');             // 2000
    };                                                                 //
                                                                       //
    return Node;                                                       // 2003
  })();                                                                //
                                                                       //
  ContentEdit.NodeCollection = (function (_super) {                    // 2007
    __extends(NodeCollection, _super);                                 // 2008
                                                                       //
    function NodeCollection() {                                        // 2010
      NodeCollection.__super__.constructor.call(this);                 // 2011
      this.children = [];                                              // 2012
    }                                                                  //
                                                                       //
    NodeCollection.prototype.descendants = function () {               // 2015
      var descendants, node, nodeStack;                                // 2016
      descendants = [];                                                // 2017
      nodeStack = this.children.slice();                               // 2018
      while (nodeStack.length > 0) {                                   // 2019
        node = nodeStack.shift();                                      // 2020
        descendants.push(node);                                        // 2021
        if (node.children && node.children.length > 0) {               // 2022
          nodeStack = node.children.slice().concat(nodeStack);         // 2023
        }                                                              //
      }                                                                //
      return descendants;                                              // 2026
    };                                                                 //
                                                                       //
    NodeCollection.prototype.isMounted = function () {                 // 2029
      return false;                                                    // 2030
    };                                                                 //
                                                                       //
    NodeCollection.prototype.attach = function (node, index) {         // 2033
      if (node.parent()) {                                             // 2034
        node.parent().detach(node);                                    // 2035
      }                                                                //
      node._parent = this;                                             // 2037
      if (index !== void 0) {                                          // 2038
        this.children.splice(index, 0, node);                          // 2039
      } else {                                                         //
        this.children.push(node);                                      // 2041
      }                                                                //
      if (node.mount && this.isMounted()) {                            // 2043
        node.mount();                                                  // 2044
      }                                                                //
      this.taint();                                                    // 2046
      return ContentEdit.Root.get().trigger('attach', this, node);     // 2047
    };                                                                 //
                                                                       //
    NodeCollection.prototype.commit = function () {                    // 2050
      var descendant, _i, _len, _ref;                                  // 2051
      _ref = this.descendants();                                       // 2052
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 2053
        descendant = _ref[_i];                                         // 2054
        descendant._modified = null;                                   // 2055
      }                                                                //
      this._modified = null;                                           // 2057
      return ContentEdit.Root.get().trigger('commit', this);           // 2058
    };                                                                 //
                                                                       //
    NodeCollection.prototype.detach = function (node) {                // 2061
      var nodeIndex;                                                   // 2062
      nodeIndex = this.children.indexOf(node);                         // 2063
      if (nodeIndex === -1) {                                          // 2064
        return;                                                        // 2065
      }                                                                //
      if (node.unmount && this.isMounted() && node.isMounted()) {      // 2067
        node.unmount();                                                // 2068
      }                                                                //
      this.children.splice(nodeIndex, 1);                              // 2070
      node._parent = null;                                             // 2071
      this.taint();                                                    // 2072
      return ContentEdit.Root.get().trigger('detach', this, node);     // 2073
    };                                                                 //
                                                                       //
    return NodeCollection;                                             // 2076
  })(ContentEdit.Node);                                                //
                                                                       //
  ContentEdit.Element = (function (_super) {                           // 2080
    __extends(Element, _super);                                        // 2081
                                                                       //
    function Element(tagName, attributes) {                            // 2083
      Element.__super__.constructor.call(this);                        // 2084
      this._tagName = tagName.toLowerCase();                           // 2085
      this._attributes = attributes ? attributes : {};                 // 2086
      this._domElement = null;                                         // 2087
    }                                                                  //
                                                                       //
    Element.prototype.attributes = function () {                       // 2090
      var attributes, name, value, _ref;                               // 2091
      attributes = {};                                                 // 2092
      _ref = this._attributes;                                         // 2093
      for (name in babelHelpers.sanitizeForInObject(_ref)) {           // 2094
        value = _ref[name];                                            // 2095
        attributes[name] = value;                                      // 2096
      }                                                                //
      return attributes;                                               // 2098
    };                                                                 //
                                                                       //
    Element.prototype.cssTypeName = function () {                      // 2101
      return 'element';                                                // 2102
    };                                                                 //
                                                                       //
    Element.prototype.domElement = function () {                       // 2105
      return this._domElement;                                         // 2106
    };                                                                 //
                                                                       //
    Element.prototype.isFocused = function () {                        // 2109
      return ContentEdit.Root.get().focused() === this;                // 2110
    };                                                                 //
                                                                       //
    Element.prototype.isMounted = function () {                        // 2113
      return this._domElement !== null;                                // 2114
    };                                                                 //
                                                                       //
    Element.prototype.typeName = function () {                         // 2117
      return 'Element';                                                // 2118
    };                                                                 //
                                                                       //
    Element.prototype.addCSSClass = function (className) {             // 2121
      var modified;                                                    // 2122
      modified = false;                                                // 2123
      if (!this.hasCSSClass(className)) {                              // 2124
        modified = true;                                               // 2125
        if (this.attr('class')) {                                      // 2126
          this.attr('class', "" + this.attr('class') + " " + className);
        } else {                                                       //
          this.attr('class', className);                               // 2129
        }                                                              //
      }                                                                //
      this._addCSSClass(className);                                    // 2132
      if (modified) {                                                  // 2133
        return this.taint();                                           // 2134
      }                                                                //
    };                                                                 //
                                                                       //
    Element.prototype.attr = function (name, value) {                  // 2138
      name = name.toLowerCase();                                       // 2139
      if (value === void 0) {                                          // 2140
        return this._attributes[name];                                 // 2141
      }                                                                //
      this._attributes[name] = value;                                  // 2143
      if (this.isMounted() && name.toLowerCase() !== 'class') {        // 2144
        this._domElement.setAttribute(name, value);                    // 2145
      }                                                                //
      return this.taint();                                             // 2147
    };                                                                 //
                                                                       //
    Element.prototype.blur = function () {                             // 2150
      var root;                                                        // 2151
      root = ContentEdit.Root.get();                                   // 2152
      if (this.isFocused()) {                                          // 2153
        this._removeCSSClass('ce-element--focused');                   // 2154
        root._focused = null;                                          // 2155
        return root.trigger('blur', this);                             // 2156
      }                                                                //
    };                                                                 //
                                                                       //
    Element.prototype.createDraggingDOMElement = function () {         // 2160
      var helper;                                                      // 2161
      if (!this.isMounted()) {                                         // 2162
        return;                                                        // 2163
      }                                                                //
      helper = document.createElement('div');                          // 2165
      helper.setAttribute('class', "ce-drag-helper ce-drag-helper--type-" + this.cssTypeName());
      helper.setAttribute('data-ce-type', ContentEdit._(this.typeName()));
      return helper;                                                   // 2168
    };                                                                 //
                                                                       //
    Element.prototype.drag = function (x, y) {                         // 2171
      var root;                                                        // 2172
      if (!this.isMounted()) {                                         // 2173
        return;                                                        // 2174
      }                                                                //
      root = ContentEdit.Root.get();                                   // 2176
      root.startDragging(this, x, y);                                  // 2177
      return root.trigger('drag', this);                               // 2178
    };                                                                 //
                                                                       //
    Element.prototype.drop = function (element, placement) {           // 2181
      var root;                                                        // 2182
      root = ContentEdit.Root.get();                                   // 2183
      if (element) {                                                   // 2184
        element._removeCSSClass('ce-element--drop');                   // 2185
        element._removeCSSClass("ce-element--drop-" + placement[0]);   // 2186
        element._removeCSSClass("ce-element--drop-" + placement[1]);   // 2187
        if (this.constructor.droppers[element.constructor.name]) {     // 2188
          this.constructor.droppers[element.constructor.name](this, element, placement);
          root.trigger('drop', this, element, placement);              // 2190
          return;                                                      // 2191
        } else if (element.constructor.droppers[this.constructor.name]) {
          element.constructor.droppers[this.constructor.name](this, element, placement);
          root.trigger('drop', this, element, placement);              // 2194
          return;                                                      // 2195
        }                                                              //
      }                                                                //
      return root.trigger('drop', this, null, null);                   // 2198
    };                                                                 //
                                                                       //
    Element.prototype.focus = function (supressDOMFocus) {             // 2201
      var root;                                                        // 2202
      root = ContentEdit.Root.get();                                   // 2203
      if (this.isFocused()) {                                          // 2204
        return;                                                        // 2205
      }                                                                //
      if (root.focused()) {                                            // 2207
        root.focused().blur();                                         // 2208
      }                                                                //
      this._addCSSClass('ce-element--focused');                        // 2210
      root._focused = this;                                            // 2211
      if (this.isMounted() && !supressDOMFocus) {                      // 2212
        this.domElement().focus();                                     // 2213
      }                                                                //
      return root.trigger('focus', this);                              // 2215
    };                                                                 //
                                                                       //
    Element.prototype.hasCSSClass = function (className) {             // 2218
      var c, classNames;                                               // 2219
      if (this.attr('class')) {                                        // 2220
        classNames = (function () {                                    // 2221
          var _i, _len, _ref, _results;                                // 2222
          _ref = this.attr('class').split(' ');                        // 2223
          _results = [];                                               // 2224
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {          // 2225
            c = _ref[_i];                                              // 2226
            _results.push(c);                                          // 2227
          }                                                            //
          return _results;                                             // 2229
        }).call(this);                                                 //
        if (classNames.indexOf(className) > -1) {                      // 2231
          return true;                                                 // 2232
        }                                                              //
      }                                                                //
      return false;                                                    // 2235
    };                                                                 //
                                                                       //
    Element.prototype.merge = function (element) {                     // 2238
      if (this.constructor.mergers[element.constructor.name]) {        // 2239
        return this.constructor.mergers[element.constructor.name](element, this);
      } else if (element.constructor.mergers[this.constructor.name]) {
        return element.constructor.mergers[this.constructor.name](element, this);
      }                                                                //
    };                                                                 //
                                                                       //
    Element.prototype.mount = function () {                            // 2246
      var sibling;                                                     // 2247
      if (!this._domElement) {                                         // 2248
        this._domElement = document.createElement(this.tagName());     // 2249
      }                                                                //
      sibling = this.nextSibling();                                    // 2251
      if (sibling) {                                                   // 2252
        this.parent().domElement().insertBefore(this._domElement, sibling.domElement());
      } else {                                                         //
        this.parent().domElement().appendChild(this._domElement);      // 2255
      }                                                                //
      this._addDOMEventListeners();                                    // 2257
      this._addCSSClass('ce-element');                                 // 2258
      this._addCSSClass("ce-element--type-" + this.cssTypeName());     // 2259
      if (this.isFocused()) {                                          // 2260
        this._addCSSClass('ce-element--focused');                      // 2261
      }                                                                //
      return ContentEdit.Root.get().trigger('mount', this);            // 2263
    };                                                                 //
                                                                       //
    Element.prototype.removeAttr = function (name) {                   // 2266
      name = name.toLowerCase();                                       // 2267
      if (!this._attributes[name]) {                                   // 2268
        return;                                                        // 2269
      }                                                                //
      delete this._attributes[name];                                   // 2271
      if (this.isMounted() && name.toLowerCase() !== 'class') {        // 2272
        this._domElement.removeAttribute(name);                        // 2273
      }                                                                //
      return this.taint();                                             // 2275
    };                                                                 //
                                                                       //
    Element.prototype.removeCSSClass = function (className) {          // 2278
      var c, classNameIndex, classNames;                               // 2279
      if (!this.hasCSSClass(className)) {                              // 2280
        return;                                                        // 2281
      }                                                                //
      classNames = (function () {                                      // 2283
        var _i, _len, _ref, _results;                                  // 2284
        _ref = this.attr('class').split(' ');                          // 2285
        _results = [];                                                 // 2286
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 2287
          c = _ref[_i];                                                // 2288
          _results.push(c);                                            // 2289
        }                                                              //
        return _results;                                               // 2291
      }).call(this);                                                   //
      classNameIndex = classNames.indexOf(className);                  // 2293
      if (classNameIndex > -1) {                                       // 2294
        classNames.splice(classNameIndex, 1);                          // 2295
      }                                                                //
      if (classNames.length) {                                         // 2297
        this.attr('class', classNames.join(' '));                      // 2298
      } else {                                                         //
        this.removeAttr('class');                                      // 2300
      }                                                                //
      this._removeCSSClass(className);                                 // 2302
      return this.taint();                                             // 2303
    };                                                                 //
                                                                       //
    Element.prototype.tagName = function (name) {                      // 2306
      if (name === void 0) {                                           // 2307
        return this._tagName;                                          // 2308
      }                                                                //
      this._tagName = name.toLowerCase();                              // 2310
      if (this.isMounted()) {                                          // 2311
        this.unmount();                                                // 2312
        this.mount();                                                  // 2313
      }                                                                //
      return this.taint();                                             // 2315
    };                                                                 //
                                                                       //
    Element.prototype.unmount = function () {                          // 2318
      this._removeDOMEventListeners();                                 // 2319
      if (this._domElement.parentNode) {                               // 2320
        this._domElement.parentNode.removeChild(this._domElement);     // 2321
      }                                                                //
      this._domElement = null;                                         // 2323
      return ContentEdit.Root.get().trigger('unmount', this);          // 2324
    };                                                                 //
                                                                       //
    Element.prototype._addDOMEventListeners = function () {            // 2327
      this._domElement.addEventListener('focus', (function (_this) {   // 2328
        return function (ev) {                                         // 2329
          return ev.preventDefault();                                  // 2330
        };                                                             //
      })(this));                                                       //
      this._domElement.addEventListener('dragstart', (function (_this) {
        return function (ev) {                                         // 2334
          return ev.preventDefault();                                  // 2335
        };                                                             //
      })(this));                                                       //
      this._domElement.addEventListener('keydown', (function (_this) {
        return function (ev) {                                         // 2339
          return _this._onKeyDown(ev);                                 // 2340
        };                                                             //
      })(this));                                                       //
      this._domElement.addEventListener('keyup', (function (_this) {   // 2343
        return function (ev) {                                         // 2344
          return _this._onKeyUp(ev);                                   // 2345
        };                                                             //
      })(this));                                                       //
      this._domElement.addEventListener('mousedown', (function (_this) {
        return function (ev) {                                         // 2349
          if (ev.button === 0) {                                       // 2350
            return _this._onMouseDown(ev);                             // 2351
          }                                                            //
        };                                                             //
      })(this));                                                       //
      this._domElement.addEventListener('mousemove', (function (_this) {
        return function (ev) {                                         // 2356
          return _this._onMouseMove(ev);                               // 2357
        };                                                             //
      })(this));                                                       //
      this._domElement.addEventListener('mouseover', (function (_this) {
        return function (ev) {                                         // 2361
          return _this._onMouseOver(ev);                               // 2362
        };                                                             //
      })(this));                                                       //
      this._domElement.addEventListener('mouseout', (function (_this) {
        return function (ev) {                                         // 2366
          return _this._onMouseOut(ev);                                // 2367
        };                                                             //
      })(this));                                                       //
      this._domElement.addEventListener('mouseup', (function (_this) {
        return function (ev) {                                         // 2371
          if (ev.button === 0) {                                       // 2372
            return _this._onMouseUp(ev);                               // 2373
          }                                                            //
        };                                                             //
      })(this));                                                       //
      return this._domElement.addEventListener('paste', (function (_this) {
        return function (ev) {                                         // 2378
          return _this._onPaste(ev);                                   // 2379
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    Element.prototype._onKeyDown = function (ev) {};                   // 2384
                                                                       //
    Element.prototype._onKeyUp = function (ev) {};                     // 2386
                                                                       //
    Element.prototype._onMouseDown = function (ev) {                   // 2388
      if (this.focus) {                                                // 2389
        return this.focus(true);                                       // 2390
      }                                                                //
    };                                                                 //
                                                                       //
    Element.prototype._onMouseMove = function (ev) {};                 // 2394
                                                                       //
    Element.prototype._onMouseOver = function (ev) {                   // 2396
      var dragging, root;                                              // 2397
      this._addCSSClass('ce-element--over');                           // 2398
      root = ContentEdit.Root.get();                                   // 2399
      dragging = root.dragging();                                      // 2400
      if (!dragging) {                                                 // 2401
        return;                                                        // 2402
      }                                                                //
      if (dragging === this) {                                         // 2404
        return;                                                        // 2405
      }                                                                //
      if (root._dropTarget) {                                          // 2407
        return;                                                        // 2408
      }                                                                //
      if (this.constructor.droppers[dragging.constructor.name] || dragging.constructor.droppers[this.constructor.name]) {
        this._addCSSClass('ce-element--drop');                         // 2411
        return root._dropTarget = this;                                // 2412
      }                                                                //
    };                                                                 //
                                                                       //
    Element.prototype._onMouseOut = function (ev) {                    // 2416
      var dragging, root;                                              // 2417
      this._removeCSSClass('ce-element--over');                        // 2418
      root = ContentEdit.Root.get();                                   // 2419
      dragging = root.dragging();                                      // 2420
      if (dragging) {                                                  // 2421
        this._removeCSSClass('ce-element--drop');                      // 2422
        this._removeCSSClass('ce-element--drop-above');                // 2423
        this._removeCSSClass('ce-element--drop-below');                // 2424
        this._removeCSSClass('ce-element--drop-center');               // 2425
        this._removeCSSClass('ce-element--drop-left');                 // 2426
        this._removeCSSClass('ce-element--drop-right');                // 2427
        return root._dropTarget = null;                                // 2428
      }                                                                //
    };                                                                 //
                                                                       //
    Element.prototype._onMouseUp = function (ev) {};                   // 2432
                                                                       //
    Element.prototype._onPaste = function (ev) {                       // 2434
      ev.preventDefault();                                             // 2435
      ev.stopPropagation();                                            // 2436
      return ContentEdit.Root.get().trigger('paste', this, ev);        // 2437
    };                                                                 //
                                                                       //
    Element.prototype._removeDOMEventListeners = function () {};       // 2440
                                                                       //
    Element.prototype._addCSSClass = function (className) {            // 2442
      if (!this.isMounted()) {                                         // 2443
        return;                                                        // 2444
      }                                                                //
      return ContentEdit.addCSSClass(this._domElement, className);     // 2446
    };                                                                 //
                                                                       //
    Element.prototype._attributesToString = function () {              // 2449
      if (!(Object.getOwnPropertyNames(this._attributes).length > 0)) {
        return '';                                                     // 2451
      }                                                                //
      return ' ' + ContentEdit.attributesToString(this._attributes);   // 2453
    };                                                                 //
                                                                       //
    Element.prototype._removeCSSClass = function (className) {         // 2456
      if (!this.isMounted()) {                                         // 2457
        return;                                                        // 2458
      }                                                                //
      return ContentEdit.removeCSSClass(this._domElement, className);  // 2460
    };                                                                 //
                                                                       //
    Element.droppers = {};                                             // 2463
                                                                       //
    Element.mergers = {};                                              // 2465
                                                                       //
    Element.placements = ['above', 'below'];                           // 2467
                                                                       //
    Element.getDOMElementAttributes = function (domElement) {          // 2469
      var attribute, attributes, _i, _len, _ref;                       // 2470
      if (!domElement.hasAttributes()) {                               // 2471
        return {};                                                     // 2472
      }                                                                //
      attributes = {};                                                 // 2474
      _ref = domElement.attributes;                                    // 2475
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 2476
        attribute = _ref[_i];                                          // 2477
        attributes[attribute.name.toLowerCase()] = attribute.value;    // 2478
      }                                                                //
      return attributes;                                               // 2480
    };                                                                 //
                                                                       //
    Element._dropVert = function (element, target, placement) {        // 2483
      var insertIndex;                                                 // 2484
      element.parent().detach(element);                                // 2485
      insertIndex = target.parent().children.indexOf(target);          // 2486
      if (placement[0] === 'below') {                                  // 2487
        insertIndex += 1;                                              // 2488
      }                                                                //
      return target.parent().attach(element, insertIndex);             // 2490
    };                                                                 //
                                                                       //
    Element._dropBoth = function (element, target, placement) {        // 2493
      var aClassNames, className, insertIndex, _i, _len, _ref;         // 2494
      element.parent().detach(element);                                // 2495
      insertIndex = target.parent().children.indexOf(target);          // 2496
      if (placement[0] === 'below' && placement[1] === 'center') {     // 2497
        insertIndex += 1;                                              // 2498
      }                                                                //
      if (element.a) {                                                 // 2500
        element._removeCSSClass('align-left');                         // 2501
        element._removeCSSClass('align-right');                        // 2502
        if (element.a['class']) {                                      // 2503
          aClassNames = [];                                            // 2504
          _ref = element.a['class'].split(' ');                        // 2505
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {          // 2506
            className = _ref[_i];                                      // 2507
            if (className === 'align-left' || className === 'align-right') {
              continue;                                                // 2509
            }                                                          //
            aClassNames.push(className);                               // 2511
          }                                                            //
          if (aClassNames.length) {                                    // 2513
            element.a['class'] = aClassNames.join(' ');                // 2514
          } else {                                                     //
            delete element.a['class'];                                 // 2516
          }                                                            //
        }                                                              //
      } else {                                                         //
        element.removeCSSClass('align-left');                          // 2520
        element.removeCSSClass('align-right');                         // 2521
      }                                                                //
      if (placement[1] === 'left') {                                   // 2523
        if (element.a) {                                               // 2524
          if (element.a['class']) {                                    // 2525
            element.a['class'] += ' align-left';                       // 2526
          } else {                                                     //
            element.a['class'] = 'align-left';                         // 2528
          }                                                            //
          element._addCSSClass('align-left');                          // 2530
        } else {                                                       //
          element.addCSSClass('align-left');                           // 2532
        }                                                              //
      }                                                                //
      if (placement[1] === 'right') {                                  // 2535
        if (element.a) {                                               // 2536
          if (element.a['class']) {                                    // 2537
            element.a['class'] += ' align-right';                      // 2538
          } else {                                                     //
            element.a['class'] = 'align-right';                        // 2540
          }                                                            //
          element._addCSSClass('align-right');                         // 2542
        } else {                                                       //
          element.addCSSClass('align-right');                          // 2544
        }                                                              //
      }                                                                //
      return target.parent().attach(element, insertIndex);             // 2547
    };                                                                 //
                                                                       //
    return Element;                                                    // 2550
  })(ContentEdit.Node);                                                //
                                                                       //
  ContentEdit.ElementCollection = (function (_super) {                 // 2554
    __extends(ElementCollection, _super);                              // 2555
                                                                       //
    ElementCollection.extend(ContentEdit.NodeCollection);              // 2557
                                                                       //
    function ElementCollection(tagName, attributes) {                  // 2559
      ElementCollection.__super__.constructor.call(this, tagName, attributes);
      ContentEdit.NodeCollection.prototype.constructor.call(this);     // 2561
    }                                                                  //
                                                                       //
    ElementCollection.prototype.cssTypeName = function () {            // 2564
      return 'element-collection';                                     // 2565
    };                                                                 //
                                                                       //
    ElementCollection.prototype.isMounted = function () {              // 2568
      return this._domElement !== null;                                // 2569
    };                                                                 //
                                                                       //
    ElementCollection.prototype.createDraggingDOMElement = function () {
      var helper, text;                                                // 2573
      if (!this.isMounted()) {                                         // 2574
        return;                                                        // 2575
      }                                                                //
      helper = ElementCollection.__super__.createDraggingDOMElement.call(this);
      text = this._domElement.textContent;                             // 2578
      if (text.length > ContentEdit.HELPER_CHAR_LIMIT) {               // 2579
        text = text.substr(0, ContentEdit.HELPER_CHAR_LIMIT);          // 2580
      }                                                                //
      helper.innerHTML = text;                                         // 2582
      return helper;                                                   // 2583
    };                                                                 //
                                                                       //
    ElementCollection.prototype.detach = function (element) {          // 2586
      ContentEdit.NodeCollection.prototype.detach.call(this, element);
      if (this.children.length === 0 && this.parent()) {               // 2588
        return this.parent().detach(this);                             // 2589
      }                                                                //
    };                                                                 //
                                                                       //
    ElementCollection.prototype.html = function (indent) {             // 2593
      var c, children;                                                 // 2594
      if (indent == null) {                                            // 2595
        indent = '';                                                   // 2596
      }                                                                //
      children = (function () {                                        // 2598
        var _i, _len, _ref, _results;                                  // 2599
        _ref = this.children;                                          // 2600
        _results = [];                                                 // 2601
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 2602
          c = _ref[_i];                                                // 2603
          _results.push(c.html(indent + ContentEdit.INDENT));          // 2604
        }                                                              //
        return _results;                                               // 2606
      }).call(this);                                                   //
      return "" + indent + "<" + this.tagName() + this._attributesToString() + ">\n" + ("" + children.join('\n') + "\n") + ("" + indent + "</" + this.tagName() + ">");
    };                                                                 //
                                                                       //
    ElementCollection.prototype.mount = function () {                  // 2611
      var child, name, value, _i, _len, _ref, _ref1, _results;         // 2612
      this._domElement = document.createElement(this._tagName);        // 2613
      _ref = this._attributes;                                         // 2614
      for (name in babelHelpers.sanitizeForInObject(_ref)) {           // 2615
        value = _ref[name];                                            // 2616
        this._domElement.setAttribute(name, value);                    // 2617
      }                                                                //
      ElementCollection.__super__.mount.call(this);                    // 2619
      _ref1 = this.children;                                           // 2620
      _results = [];                                                   // 2621
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {             // 2622
        child = _ref1[_i];                                             // 2623
        _results.push(child.mount());                                  // 2624
      }                                                                //
      return _results;                                                 // 2626
    };                                                                 //
                                                                       //
    ElementCollection.prototype.unmount = function () {                // 2629
      var child, _i, _len, _ref;                                       // 2630
      _ref = this.children;                                            // 2631
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 2632
        child = _ref[_i];                                              // 2633
        child.unmount();                                               // 2634
      }                                                                //
      return ElementCollection.__super__.unmount.call(this);           // 2636
    };                                                                 //
                                                                       //
    ElementCollection.prototype.blur = void 0;                         // 2639
                                                                       //
    ElementCollection.prototype.focus = void 0;                        // 2641
                                                                       //
    return ElementCollection;                                          // 2643
  })(ContentEdit.Element);                                             //
                                                                       //
  ContentEdit.ResizableElement = (function (_super) {                  // 2647
    __extends(ResizableElement, _super);                               // 2648
                                                                       //
    function ResizableElement(tagName, attributes) {                   // 2650
      ResizableElement.__super__.constructor.call(this, tagName, attributes);
      this._domSizeInfoElement = null;                                 // 2652
      this._aspectRatio = 1;                                           // 2653
    }                                                                  //
                                                                       //
    ResizableElement.prototype.aspectRatio = function () {             // 2656
      return this._aspectRatio;                                        // 2657
    };                                                                 //
                                                                       //
    ResizableElement.prototype.maxSize = function () {                 // 2660
      var maxWidth;                                                    // 2661
      maxWidth = parseInt(this.attr('data-ce-max-width') || 0);        // 2662
      if (!maxWidth) {                                                 // 2663
        maxWidth = ContentEdit.DEFAULT_MAX_ELEMENT_WIDTH;              // 2664
      }                                                                //
      maxWidth = Math.max(maxWidth, this.size()[0]);                   // 2666
      return [maxWidth, maxWidth * this.aspectRatio()];                // 2667
    };                                                                 //
                                                                       //
    ResizableElement.prototype.minSize = function () {                 // 2670
      var minWidth;                                                    // 2671
      minWidth = parseInt(this.attr('data-ce-min-width') || 0);        // 2672
      if (!minWidth) {                                                 // 2673
        minWidth = ContentEdit.DEFAULT_MIN_ELEMENT_WIDTH;              // 2674
      }                                                                //
      minWidth = Math.min(minWidth, this.size()[0]);                   // 2676
      return [minWidth, minWidth * this.aspectRatio()];                // 2677
    };                                                                 //
                                                                       //
    ResizableElement.prototype.mount = function () {                   // 2680
      ResizableElement.__super__.mount.call(this);                     // 2681
      return this._domElement.setAttribute('data-ce-size', this._getSizeInfo());
    };                                                                 //
                                                                       //
    ResizableElement.prototype.resize = function (corner, x, y) {      // 2685
      if (!this.isMounted()) {                                         // 2686
        return;                                                        // 2687
      }                                                                //
      return ContentEdit.Root.get().startResizing(this, corner, x, y, true);
    };                                                                 //
                                                                       //
    ResizableElement.prototype.size = function (newSize) {             // 2692
      var height, maxSize, minSize, width;                             // 2693
      if (!newSize) {                                                  // 2694
        width = parseInt(this.attr('width') || 1);                     // 2695
        height = parseInt(this.attr('height') || 1);                   // 2696
        return [width, height];                                        // 2697
      }                                                                //
      newSize[0] = parseInt(newSize[0]);                               // 2699
      newSize[1] = parseInt(newSize[1]);                               // 2700
      minSize = this.minSize();                                        // 2701
      newSize[0] = Math.max(newSize[0], minSize[0]);                   // 2702
      newSize[1] = Math.max(newSize[1], minSize[1]);                   // 2703
      maxSize = this.maxSize();                                        // 2704
      newSize[0] = Math.min(newSize[0], maxSize[0]);                   // 2705
      newSize[1] = Math.min(newSize[1], maxSize[1]);                   // 2706
      this.attr('width', parseInt(newSize[0]));                        // 2707
      this.attr('height', parseInt(newSize[1]));                       // 2708
      if (this.isMounted()) {                                          // 2709
        this._domElement.style.width = "" + newSize[0] + "px";         // 2710
        this._domElement.style.height = "" + newSize[1] + "px";        // 2711
        return this._domElement.setAttribute('data-ce-size', this._getSizeInfo());
      }                                                                //
    };                                                                 //
                                                                       //
    ResizableElement.prototype._onMouseDown = function (ev) {          // 2716
      var corner;                                                      // 2717
      ResizableElement.__super__._onMouseDown.call(this);              // 2718
      corner = this._getResizeCorner(ev.clientX, ev.clientY);          // 2719
      if (corner) {                                                    // 2720
        return this.resize(corner, ev.clientX, ev.clientY);            // 2721
      } else {                                                         //
        clearTimeout(this._dragTimeout);                               // 2723
        return this._dragTimeout = setTimeout((function (_this) {      // 2724
          return function () {                                         // 2725
            return _this.drag(ev.pageX, ev.pageY);                     // 2726
          };                                                           //
        })(this), 150);                                                //
      }                                                                //
    };                                                                 //
                                                                       //
    ResizableElement.prototype._onMouseMove = function (ev) {          // 2732
      var corner;                                                      // 2733
      ResizableElement.__super__._onMouseMove.call(this);              // 2734
      this._removeCSSClass('ce-element--resize-top-left');             // 2735
      this._removeCSSClass('ce-element--resize-top-right');            // 2736
      this._removeCSSClass('ce-element--resize-bottom-left');          // 2737
      this._removeCSSClass('ce-element--resize-bottom-right');         // 2738
      corner = this._getResizeCorner(ev.clientX, ev.clientY);          // 2739
      if (corner) {                                                    // 2740
        return this._addCSSClass("ce-element--resize-" + corner[0] + "-" + corner[1]);
      }                                                                //
    };                                                                 //
                                                                       //
    ResizableElement.prototype._onMouseOut = function (ev) {           // 2745
      ResizableElement.__super__._onMouseOut.call(this);               // 2746
      this._removeCSSClass('ce-element--resize-top-left');             // 2747
      this._removeCSSClass('ce-element--resize-top-right');            // 2748
      this._removeCSSClass('ce-element--resize-bottom-left');          // 2749
      return this._removeCSSClass('ce-element--resize-bottom-right');  // 2750
    };                                                                 //
                                                                       //
    ResizableElement.prototype._onMouseUp = function (ev) {            // 2753
      ResizableElement.__super__._onMouseUp.call(this);                // 2754
      if (this._dragTimeout) {                                         // 2755
        return clearTimeout(this._dragTimeout);                        // 2756
      }                                                                //
    };                                                                 //
                                                                       //
    ResizableElement.prototype._getResizeCorner = function (x, y) {    // 2760
      var corner, cornerSize, rect, size, _ref;                        // 2761
      rect = this._domElement.getBoundingClientRect();                 // 2762
      _ref = [x - rect.left, y - rect.top], x = _ref[0], y = _ref[1];  // 2763
      size = this.size();                                              // 2764
      cornerSize = ContentEdit.RESIZE_CORNER_SIZE;                     // 2765
      cornerSize = Math.min(cornerSize, Math.max(parseInt(size[0] / 4), 1));
      cornerSize = Math.min(cornerSize, Math.max(parseInt(size[1] / 4), 1));
      corner = null;                                                   // 2768
      if (x < cornerSize) {                                            // 2769
        if (y < cornerSize) {                                          // 2770
          corner = ['top', 'left'];                                    // 2771
        } else if (y > rect.height - cornerSize) {                     //
          corner = ['bottom', 'left'];                                 // 2773
        }                                                              //
      } else if (x > rect.width - cornerSize) {                        //
        if (y < cornerSize) {                                          // 2776
          corner = ['top', 'right'];                                   // 2777
        } else if (y > rect.height - cornerSize) {                     //
          corner = ['bottom', 'right'];                                // 2779
        }                                                              //
      }                                                                //
      return corner;                                                   // 2782
    };                                                                 //
                                                                       //
    ResizableElement.prototype._getSizeInfo = function () {            // 2785
      var size;                                                        // 2786
      size = this.size();                                              // 2787
      return "w " + size[0] + " × h " + size[1];                       // 2788
    };                                                                 //
                                                                       //
    return ResizableElement;                                           // 2791
  })(ContentEdit.Element);                                             //
                                                                       //
  ContentEdit.Region = (function (_super) {                            // 2795
    __extends(Region, _super);                                         // 2796
                                                                       //
    function Region(domElement) {                                      // 2798
      var c, childNode, childNodes, cls, element, tagNames, _i, _len;  // 2799
      Region.__super__.constructor.call(this);                         // 2800
      this._domElement = domElement;                                   // 2801
      tagNames = ContentEdit.TagNames.get();                           // 2802
      childNodes = (function () {                                      // 2803
        var _i, _len, _ref, _results;                                  // 2804
        _ref = this._domElement.childNodes;                            // 2805
        _results = [];                                                 // 2806
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 2807
          c = _ref[_i];                                                // 2808
          _results.push(c);                                            // 2809
        }                                                              //
        return _results;                                               // 2811
      }).call(this);                                                   //
      for (_i = 0, _len = childNodes.length; _i < _len; _i++) {        // 2813
        childNode = childNodes[_i];                                    // 2814
        if (childNode.nodeType !== 1) {                                // 2815
          continue;                                                    // 2816
        }                                                              //
        if (childNode.getAttribute("data-ce-tag")) {                   // 2818
          cls = tagNames.match(childNode.getAttribute("data-ce-tag"));
        } else {                                                       //
          cls = tagNames.match(childNode.tagName);                     // 2821
        }                                                              //
        element = cls.fromDOMElement(childNode);                       // 2823
        this._domElement.removeChild(childNode);                       // 2824
        if (element) {                                                 // 2825
          this.attach(element);                                        // 2826
        }                                                              //
      }                                                                //
    }                                                                  //
                                                                       //
    Region.prototype.domElement = function () {                        // 2831
      return this._domElement;                                         // 2832
    };                                                                 //
                                                                       //
    Region.prototype.isMounted = function () {                         // 2835
      return true;                                                     // 2836
    };                                                                 //
                                                                       //
    Region.prototype.html = function (indent) {                        // 2839
      var c;                                                           // 2840
      if (indent == null) {                                            // 2841
        indent = '';                                                   // 2842
      }                                                                //
      return (function () {                                            // 2844
        var _i, _len, _ref, _results;                                  // 2845
        _ref = this.children;                                          // 2846
        _results = [];                                                 // 2847
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 2848
          c = _ref[_i];                                                // 2849
          _results.push(c.html(indent));                               // 2850
        }                                                              //
        return _results;                                               // 2852
      }).call(this).join('\n').trim();                                 //
    };                                                                 //
                                                                       //
    return Region;                                                     // 2856
  })(ContentEdit.NodeCollection);                                      //
                                                                       //
  _Root = (function (_super) {                                         // 2860
    __extends(_Root, _super);                                          // 2861
                                                                       //
    function _Root() {                                                 // 2863
      this._onStopResizing = __bind(this._onStopResizing, this);       // 2864
      this._onResize = __bind(this._onResize, this);                   // 2865
      this._onStopDragging = __bind(this._onStopDragging, this);       // 2866
      this._onDrag = __bind(this._onDrag, this);                       // 2867
      _Root.__super__.constructor.call(this);                          // 2868
      this._focused = null;                                            // 2869
      this._dragging = null;                                           // 2870
      this._dropTarget = null;                                         // 2871
      this._draggingDOMElement = null;                                 // 2872
      this._resizing = null;                                           // 2873
      this._resizingInit = null;                                       // 2874
    }                                                                  //
                                                                       //
    _Root.prototype.dragging = function () {                           // 2877
      return this._dragging;                                           // 2878
    };                                                                 //
                                                                       //
    _Root.prototype.dropTarget = function () {                         // 2881
      return this._dropTarget;                                         // 2882
    };                                                                 //
                                                                       //
    _Root.prototype.focused = function () {                            // 2885
      return this._focused;                                            // 2886
    };                                                                 //
                                                                       //
    _Root.prototype.resizing = function () {                           // 2889
      return this._resizing;                                           // 2890
    };                                                                 //
                                                                       //
    _Root.prototype.cancelDragging = function () {                     // 2893
      if (!this._dragging) {                                           // 2894
        return;                                                        // 2895
      }                                                                //
      document.body.removeChild(this._draggingDOMElement);             // 2897
      document.removeEventListener('mousemove', this._onDrag);         // 2898
      document.removeEventListener('mouseup', this._onStopDragging);   // 2899
      this._dragging._removeCSSClass('ce-element--dragging');          // 2900
      this._dragging = null;                                           // 2901
      this._dropTarget = null;                                         // 2902
      return ContentEdit.removeCSSClass(document.body, 'ce--dragging');
    };                                                                 //
                                                                       //
    _Root.prototype.startDragging = function (element, x, y) {         // 2906
      if (this._dragging) {                                            // 2907
        return;                                                        // 2908
      }                                                                //
      this._dragging = element;                                        // 2910
      this._dragging._addCSSClass('ce-element--dragging');             // 2911
      this._draggingDOMElement = this._dragging.createDraggingDOMElement();
      document.body.appendChild(this._draggingDOMElement);             // 2913
      this._draggingDOMElement.style.left = "" + x + "px";             // 2914
      this._draggingDOMElement.style.top = "" + y + "px";              // 2915
      document.addEventListener('mousemove', this._onDrag);            // 2916
      document.addEventListener('mouseup', this._onStopDragging);      // 2917
      return ContentEdit.addCSSClass(document.body, 'ce--dragging');   // 2918
    };                                                                 //
                                                                       //
    _Root.prototype._getDropPlacement = function (x, y) {              // 2921
      var horz, rect, vert, _ref;                                      // 2922
      if (!this._dropTarget) {                                         // 2923
        return null;                                                   // 2924
      }                                                                //
      rect = this._dropTarget.domElement().getBoundingClientRect();    // 2926
      _ref = [x - rect.left, y - rect.top], x = _ref[0], y = _ref[1];  // 2927
      horz = 'center';                                                 // 2928
      if (x < ContentEdit.DROP_EDGE_SIZE) {                            // 2929
        horz = 'left';                                                 // 2930
      } else if (x > rect.width - ContentEdit.DROP_EDGE_SIZE) {        //
        horz = 'right';                                                // 2932
      }                                                                //
      vert = 'above';                                                  // 2934
      if (y > rect.height / 2) {                                       // 2935
        vert = 'below';                                                // 2936
      }                                                                //
      return [vert, horz];                                             // 2938
    };                                                                 //
                                                                       //
    _Root.prototype._onDrag = function (ev) {                          // 2941
      var placement, _ref, _ref1;                                      // 2942
      ContentSelect.Range.unselectAll();                               // 2943
      this._draggingDOMElement.style.left = "" + ev.pageX + "px";      // 2944
      this._draggingDOMElement.style.top = "" + ev.pageY + "px";       // 2945
      if (this._dropTarget) {                                          // 2946
        placement = this._getDropPlacement(ev.clientX, ev.clientY);    // 2947
        this._dropTarget._removeCSSClass('ce-element--drop-above');    // 2948
        this._dropTarget._removeCSSClass('ce-element--drop-below');    // 2949
        this._dropTarget._removeCSSClass('ce-element--drop-center');   // 2950
        this._dropTarget._removeCSSClass('ce-element--drop-left');     // 2951
        this._dropTarget._removeCSSClass('ce-element--drop-right');    // 2952
        if ((_ref = placement[0], __indexOf.call(this._dragging.constructor.placements, _ref) >= 0)) {
          this._dropTarget._addCSSClass("ce-element--drop-" + placement[0]);
        }                                                              //
        if ((_ref1 = placement[1], __indexOf.call(this._dragging.constructor.placements, _ref1) >= 0)) {
          return this._dropTarget._addCSSClass("ce-element--drop-" + placement[1]);
        }                                                              //
      }                                                                //
    };                                                                 //
                                                                       //
    _Root.prototype._onStopDragging = function (ev) {                  // 2962
      var placement;                                                   // 2963
      placement = this._getDropPlacement(ev.clientX, ev.clientY);      // 2964
      this._dragging.drop(this._dropTarget, placement);                // 2965
      return this.cancelDragging();                                    // 2966
    };                                                                 //
                                                                       //
    _Root.prototype.startResizing = function (element, corner, x, y, fixed) {
      var measureDom, parentDom;                                       // 2970
      if (this._resizing) {                                            // 2971
        return;                                                        // 2972
      }                                                                //
      this._resizing = element;                                        // 2974
      this._resizingInit = {                                           // 2975
        corner: corner,                                                // 2976
        fixed: fixed,                                                  // 2977
        origin: [x, y],                                                // 2978
        size: element.size()                                           // 2979
      };                                                               //
      this._resizing._addCSSClass('ce-element--resizing');             // 2981
      parentDom = this._resizing.parent().domElement();                // 2982
      measureDom = document.createElement('div');                      // 2983
      measureDom.setAttribute('class', 'ce-measure');                  // 2984
      parentDom.appendChild(measureDom);                               // 2985
      this._resizingParentWidth = measureDom.getBoundingClientRect().width;
      parentDom.removeChild(measureDom);                               // 2987
      document.addEventListener('mousemove', this._onResize);          // 2988
      document.addEventListener('mouseup', this._onStopResizing);      // 2989
      return ContentEdit.addCSSClass(document.body, 'ce--resizing');   // 2990
    };                                                                 //
                                                                       //
    _Root.prototype._onResize = function (ev) {                        // 2993
      var height, width, x, y;                                         // 2994
      ContentSelect.Range.unselectAll();                               // 2995
      x = this._resizingInit.origin[0] - ev.clientX;                   // 2996
      if (this._resizingInit.corner[1] === 'right') {                  // 2997
        x = -x;                                                        // 2998
      }                                                                //
      width = this._resizingInit.size[0] + x;                          // 3000
      width = Math.min(width, this._resizingParentWidth);              // 3001
      if (this._resizingInit.fixed) {                                  // 3002
        height = width * this._resizing.aspectRatio();                 // 3003
      } else {                                                         //
        y = this._resizingInit.origin[1] - ev.clientY;                 // 3005
        if (this._resizingInit.corner[0] === 'bottom') {               // 3006
          y = -y;                                                      // 3007
        }                                                              //
        height = this._resizingInit.size[1] + y;                       // 3009
      }                                                                //
      return this._resizing.size([width, height]);                     // 3011
    };                                                                 //
                                                                       //
    _Root.prototype._onStopResizing = function (ev) {                  // 3014
      document.removeEventListener('mousemove', this._onResize);       // 3015
      document.removeEventListener('mouseup', this._onStopResizing);   // 3016
      this._resizing._removeCSSClass('ce-element--resizing');          // 3017
      this._resizing = null;                                           // 3018
      this._resizingInit = null;                                       // 3019
      this._resizingParentWidth = null;                                // 3020
      return ContentEdit.removeCSSClass(document.body, 'ce--resizing');
    };                                                                 //
                                                                       //
    return _Root;                                                      // 3024
  })(ContentEdit.Node);                                                //
                                                                       //
  ContentEdit.Root = (function () {                                    // 3028
    var instance;                                                      // 3029
                                                                       //
    function Root() {}                                                 // 3031
                                                                       //
    instance = null;                                                   // 3033
                                                                       //
    Root.get = function () {                                           // 3035
      return instance != null ? instance : instance = new _Root();     // 3036
    };                                                                 //
                                                                       //
    return Root;                                                       // 3039
  })();                                                                //
                                                                       //
  ContentEdit.Static = (function (_super) {                            // 3043
    __extends(Static, _super);                                         // 3044
                                                                       //
    function Static(tagName, attributes, content) {                    // 3046
      Static.__super__.constructor.call(this, tagName, attributes);    // 3047
      this._content = content;                                         // 3048
    }                                                                  //
                                                                       //
    Static.prototype.cssTypeName = function () {                       // 3051
      return 'static';                                                 // 3052
    };                                                                 //
                                                                       //
    Static.prototype.typeName = function () {                          // 3055
      return 'Static';                                                 // 3056
    };                                                                 //
                                                                       //
    Static.prototype.createDraggingDOMElement = function () {          // 3059
      var helper, text;                                                // 3060
      if (!this.isMounted()) {                                         // 3061
        return;                                                        // 3062
      }                                                                //
      helper = Static.__super__.createDraggingDOMElement.call(this);   // 3064
      text = this._domElement.textContent;                             // 3065
      if (text.length > ContentEdit.HELPER_CHAR_LIMIT) {               // 3066
        text = text.substr(0, ContentEdit.HELPER_CHAR_LIMIT);          // 3067
      }                                                                //
      helper.innerHTML = text;                                         // 3069
      return helper;                                                   // 3070
    };                                                                 //
                                                                       //
    Static.prototype.html = function (indent) {                        // 3073
      if (indent == null) {                                            // 3074
        indent = '';                                                   // 3075
      }                                                                //
      return "" + indent + "<" + this._tagName + this._attributesToString() + ">" + ("" + this._content) + ("" + indent + "</" + this._tagName + ">");
    };                                                                 //
                                                                       //
    Static.prototype.mount = function () {                             // 3080
      var name, value, _ref;                                           // 3081
      this._domElement = document.createElement(this._tagName);        // 3082
      _ref = this._attributes;                                         // 3083
      for (name in babelHelpers.sanitizeForInObject(_ref)) {           // 3084
        value = _ref[name];                                            // 3085
        this._domElement.setAttribute(name, value);                    // 3086
      }                                                                //
      this._domElement.innerHTML = this._content;                      // 3088
      return Static.__super__.mount.call(this);                        // 3089
    };                                                                 //
                                                                       //
    Static.prototype.blur = void 0;                                    // 3092
                                                                       //
    Static.prototype.focus = void 0;                                   // 3094
                                                                       //
    Static.prototype._onMouseDown = function (ev) {                    // 3096
      Static.__super__._onMouseDown.call(this);                        // 3097
      if (this.attr('data-ce-moveable') !== void 0) {                  // 3098
        clearTimeout(this._dragTimeout);                               // 3099
        return this._dragTimeout = setTimeout((function (_this) {      // 3100
          return function () {                                         // 3101
            return _this.drag(ev.pageX, ev.pageY);                     // 3102
          };                                                           //
        })(this), 150);                                                //
      }                                                                //
    };                                                                 //
                                                                       //
    Static.prototype._onMouseOver = function (ev) {                    // 3108
      Static.__super__._onMouseOver.call(this, ev);                    // 3109
      return this._removeCSSClass('ce-element--over');                 // 3110
    };                                                                 //
                                                                       //
    Static.prototype._onMouseUp = function (ev) {                      // 3113
      Static.__super__._onMouseUp.call(this);                          // 3114
      if (this._dragTimeout) {                                         // 3115
        return clearTimeout(this._dragTimeout);                        // 3116
      }                                                                //
    };                                                                 //
                                                                       //
    Static.droppers = {                                                // 3120
      'Static': ContentEdit.Element._dropVert                          // 3121
    };                                                                 //
                                                                       //
    Static.fromDOMElement = function (domElement) {                    // 3124
      return new this(domElement.tagName, this.getDOMElementAttributes(domElement), domElement.innerHTML);
    };                                                                 //
                                                                       //
    return Static;                                                     // 3128
  })(ContentEdit.Element);                                             //
                                                                       //
  ContentEdit.TagNames.get().register(ContentEdit.Static, 'static');   // 3132
                                                                       //
  ContentEdit.Text = (function (_super) {                              // 3134
    __extends(Text, _super);                                           // 3135
                                                                       //
    function Text(tagName, attributes, content) {                      // 3137
      Text.__super__.constructor.call(this, tagName, attributes);      // 3138
      if (content instanceof HTMLString.String) {                      // 3139
        this.content = content;                                        // 3140
      } else {                                                         //
        this.content = new HTMLString.String(content).trim();          // 3142
      }                                                                //
    }                                                                  //
                                                                       //
    Text.prototype.cssTypeName = function () {                         // 3146
      return 'text';                                                   // 3147
    };                                                                 //
                                                                       //
    Text.prototype.typeName = function () {                            // 3150
      return 'Text';                                                   // 3151
    };                                                                 //
                                                                       //
    Text.prototype.blur = function () {                                // 3154
      var error;                                                       // 3155
      if (this.isMounted()) {                                          // 3156
        this._syncContent();                                           // 3157
      }                                                                //
      if (this.content.isWhitespace()) {                               // 3159
        if (this.parent()) {                                           // 3160
          this.parent().detach(this);                                  // 3161
        }                                                              //
      } else if (this.isMounted()) {                                   //
        try {                                                          // 3164
          this._domElement.blur();                                     // 3165
        } catch (_error) {                                             //
          error = _error;                                              // 3167
        }                                                              //
        this._domElement.removeAttribute('contenteditable');           // 3169
      }                                                                //
      return Text.__super__.blur.call(this);                           // 3171
    };                                                                 //
                                                                       //
    Text.prototype.createDraggingDOMElement = function () {            // 3174
      var helper, text;                                                // 3175
      if (!this.isMounted()) {                                         // 3176
        return;                                                        // 3177
      }                                                                //
      helper = Text.__super__.createDraggingDOMElement.call(this);     // 3179
      text = this._domElement.textContent;                             // 3180
      if (text.length > ContentEdit.HELPER_CHAR_LIMIT) {               // 3181
        text = text.substr(0, ContentEdit.HELPER_CHAR_LIMIT);          // 3182
      }                                                                //
      helper.innerHTML = text;                                         // 3184
      return helper;                                                   // 3185
    };                                                                 //
                                                                       //
    Text.prototype.drag = function (x, y) {                            // 3188
      this.storeState();                                               // 3189
      this._domElement.removeAttribute('contenteditable');             // 3190
      return Text.__super__.drag.call(this, x, y);                     // 3191
    };                                                                 //
                                                                       //
    Text.prototype.drop = function (element, placement) {              // 3194
      Text.__super__.drop.call(this, element, placement);              // 3195
      return this.restoreState();                                      // 3196
    };                                                                 //
                                                                       //
    Text.prototype.focus = function (supressDOMFocus) {                // 3199
      if (this.isMounted()) {                                          // 3200
        this._domElement.setAttribute('contenteditable', '');          // 3201
      }                                                                //
      return Text.__super__.focus.call(this, supressDOMFocus);         // 3203
    };                                                                 //
                                                                       //
    Text.prototype.html = function (indent) {                          // 3206
      var content;                                                     // 3207
      if (indent == null) {                                            // 3208
        indent = '';                                                   // 3209
      }                                                                //
      if (!this._lastCached || this._lastCached < this._modified) {    // 3211
        content = this.content.copy();                                 // 3212
        content.optimize();                                            // 3213
        this._lastCached = Date.now();                                 // 3214
        this._cached = content.html();                                 // 3215
      }                                                                //
      return "" + indent + "<" + this._tagName + this._attributesToString() + ">\n" + ("" + indent + ContentEdit.INDENT + this._cached + "\n") + ("" + indent + "</" + this._tagName + ">");
    };                                                                 //
                                                                       //
    Text.prototype.mount = function () {                               // 3220
      var name, value, _ref;                                           // 3221
      this._domElement = document.createElement(this._tagName);        // 3222
      _ref = this._attributes;                                         // 3223
      for (name in babelHelpers.sanitizeForInObject(_ref)) {           // 3224
        value = _ref[name];                                            // 3225
        this._domElement.setAttribute(name, value);                    // 3226
      }                                                                //
      this.updateInnerHTML();                                          // 3228
      return Text.__super__.mount.call(this);                          // 3229
    };                                                                 //
                                                                       //
    Text.prototype.restoreState = function () {                        // 3232
      if (!this._savedSelection) {                                     // 3233
        return;                                                        // 3234
      }                                                                //
      if (!(this.isMounted() && this.isFocused())) {                   // 3236
        this._savedSelection = void 0;                                 // 3237
        return;                                                        // 3238
      }                                                                //
      this._domElement.setAttribute('contenteditable', '');            // 3240
      this._addCSSClass('ce-element--focused');                        // 3241
      this._savedSelection.select(this._domElement);                   // 3242
      return this._savedSelection = void 0;                            // 3243
    };                                                                 //
                                                                       //
    Text.prototype.selection = function (selection) {                  // 3246
      if (selection === void 0) {                                      // 3247
        if (this.isMounted()) {                                        // 3248
          return ContentSelect.Range.query(this._domElement);          // 3249
        } else {                                                       //
          return new ContentSelect.Range(0, 0);                        // 3251
        }                                                              //
      }                                                                //
      return selection.select(this._domElement);                       // 3254
    };                                                                 //
                                                                       //
    Text.prototype.storeState = function () {                          // 3257
      if (!(this.isMounted() && this.isFocused())) {                   // 3258
        return;                                                        // 3259
      }                                                                //
      return this._savedSelection = ContentSelect.Range.query(this._domElement);
    };                                                                 //
                                                                       //
    Text.prototype.updateInnerHTML = function () {                     // 3264
      this._domElement.innerHTML = this.content.html();                // 3265
      ContentSelect.Range.prepareElement(this._domElement);            // 3266
      return this._flagIfEmpty();                                      // 3267
    };                                                                 //
                                                                       //
    Text.prototype._onKeyDown = function (ev) {                        // 3270
      switch (ev.keyCode) {                                            // 3271
        case 40:                                                       // 3272
          return this._keyDown(ev);                                    // 3273
        case 37:                                                       // 3273
          return this._keyLeft(ev);                                    // 3275
        case 39:                                                       // 3275
          return this._keyRight(ev);                                   // 3277
        case 38:                                                       // 3278
          return this._keyUp(ev);                                      // 3279
        case 9:                                                        // 3280
          return this._keyTab(ev);                                     // 3281
        case 8:                                                        // 3281
          return this._keyBack(ev);                                    // 3283
        case 46:                                                       // 3283
          return this._keyDelete(ev);                                  // 3285
        case 13:                                                       // 3285
          return this._keyReturn(ev);                                  // 3287
      }                                                                // 3287
    };                                                                 //
                                                                       //
    Text.prototype._onKeyUp = function (ev) {                          // 3291
      return this._syncContent();                                      // 3292
    };                                                                 //
                                                                       //
    Text.prototype._onMouseDown = function (ev) {                      // 3295
      Text.__super__._onMouseDown.call(this);                          // 3296
      clearTimeout(this._dragTimeout);                                 // 3297
      return this._dragTimeout = setTimeout((function (_this) {        // 3298
        return function () {                                           // 3299
          return _this.drag(ev.pageX, ev.pageY);                       // 3300
        };                                                             //
      })(this), ContentEdit.DRAG_HOLD_DURATION);                       //
    };                                                                 //
                                                                       //
    Text.prototype._onMouseMove = function (ev) {                      // 3305
      if (this._dragTimeout) {                                         // 3306
        clearTimeout(this._dragTimeout);                               // 3307
      }                                                                //
      return Text.__super__._onMouseMove.call(this);                   // 3309
    };                                                                 //
                                                                       //
    Text.prototype._onMouseOut = function (ev) {                       // 3312
      if (this._dragTimeout) {                                         // 3313
        clearTimeout(this._dragTimeout);                               // 3314
      }                                                                //
      return Text.__super__._onMouseOut.call(this);                    // 3316
    };                                                                 //
                                                                       //
    Text.prototype._onMouseUp = function (ev) {                        // 3319
      if (this._dragTimeout) {                                         // 3320
        clearTimeout(this._dragTimeout);                               // 3321
      }                                                                //
      return Text.__super__._onMouseUp.call(this);                     // 3323
    };                                                                 //
                                                                       //
    Text.prototype._keyBack = function (ev) {                          // 3326
      var previous, selection;                                         // 3327
      selection = ContentSelect.Range.query(this._domElement);         // 3328
      if (!(selection.get()[0] === 0 && selection.isCollapsed())) {    // 3329
        return;                                                        // 3330
      }                                                                //
      ev.preventDefault();                                             // 3332
      previous = this.previousContent();                               // 3333
      this._syncContent();                                             // 3334
      if (previous) {                                                  // 3335
        return previous.merge(this);                                   // 3336
      }                                                                //
    };                                                                 //
                                                                       //
    Text.prototype._keyDelete = function (ev) {                        // 3340
      var next, selection;                                             // 3341
      selection = ContentSelect.Range.query(this._domElement);         // 3342
      if (!(this._atEnd(selection) && selection.isCollapsed())) {      // 3343
        return;                                                        // 3344
      }                                                                //
      ev.preventDefault();                                             // 3346
      next = this.nextContent();                                       // 3347
      if (next) {                                                      // 3348
        return this.merge(next);                                       // 3349
      }                                                                //
    };                                                                 //
                                                                       //
    Text.prototype._keyDown = function (ev) {                          // 3353
      return this._keyRight(ev);                                       // 3354
    };                                                                 //
                                                                       //
    Text.prototype._keyLeft = function (ev) {                          // 3357
      var previous, selection;                                         // 3358
      selection = ContentSelect.Range.query(this._domElement);         // 3359
      if (!(selection.get()[0] === 0 && selection.isCollapsed())) {    // 3360
        return;                                                        // 3361
      }                                                                //
      ev.preventDefault();                                             // 3363
      previous = this.previousContent();                               // 3364
      if (previous) {                                                  // 3365
        previous.focus();                                              // 3366
        selection = new ContentSelect.Range(previous.content.length(), previous.content.length());
        return selection.select(previous.domElement());                // 3368
      } else {                                                         //
        return ContentEdit.Root.get().trigger('previous-region', this.closest(function (node) {
          return node.constructor.name === 'Region';                   // 3371
        }));                                                           //
      }                                                                //
    };                                                                 //
                                                                       //
    Text.prototype._keyReturn = function (ev) {                        // 3376
      var element, selection, tail, tip;                               // 3377
      ev.preventDefault();                                             // 3378
      if (this.content.isWhitespace()) {                               // 3379
        return;                                                        // 3380
      }                                                                //
      ContentSelect.Range.query(this._domElement);                     // 3382
      selection = ContentSelect.Range.query(this._domElement);         // 3383
      tip = this.content.substring(0, selection.get()[0]);             // 3384
      tail = this.content.substring(selection.get()[1]);               // 3385
      this.content = tip.trim();                                       // 3386
      this.updateInnerHTML();                                          // 3387
      element = new this.constructor('p', {}, tail.trim());            // 3388
      this.parent().attach(element, this.parent().children.indexOf(this) + 1);
      if (tip.length()) {                                              // 3390
        element.focus();                                               // 3391
        selection = new ContentSelect.Range(0, 0);                     // 3392
        selection.select(element.domElement());                        // 3393
      } else {                                                         //
        selection = new ContentSelect.Range(0, tip.length());          // 3395
        selection.select(this._domElement);                            // 3396
      }                                                                //
      return this.taint();                                             // 3398
    };                                                                 //
                                                                       //
    Text.prototype._keyRight = function (ev) {                         // 3401
      var next, selection;                                             // 3402
      selection = ContentSelect.Range.query(this._domElement);         // 3403
      if (!(this._atEnd(selection) && selection.isCollapsed())) {      // 3404
        return;                                                        // 3405
      }                                                                //
      ev.preventDefault();                                             // 3407
      next = this.nextContent();                                       // 3408
      if (next) {                                                      // 3409
        next.focus();                                                  // 3410
        selection = new ContentSelect.Range(0, 0);                     // 3411
        return selection.select(next.domElement());                    // 3412
      } else {                                                         //
        return ContentEdit.Root.get().trigger('next-region', this.closest(function (node) {
          return node.constructor.name === 'Region';                   // 3415
        }));                                                           //
      }                                                                //
    };                                                                 //
                                                                       //
    Text.prototype._keyTab = function (ev) {                           // 3420
      return ev.preventDefault();                                      // 3421
    };                                                                 //
                                                                       //
    Text.prototype._keyUp = function (ev) {                            // 3424
      return this._keyLeft(ev);                                        // 3425
    };                                                                 //
                                                                       //
    Text.prototype._atEnd = function (selection) {                     // 3428
      var atEnd;                                                       // 3429
      atEnd = selection.get()[0] === this.content.length();            // 3430
      if (selection.get()[0] === this.content.length() - 1 && this.content.characters[this.content.characters.length - 1].isTag('br')) {
        atEnd = true;                                                  // 3432
      }                                                                //
      return atEnd;                                                    // 3434
    };                                                                 //
                                                                       //
    Text.prototype._flagIfEmpty = function () {                        // 3437
      if (this.content.length() === 0) {                               // 3438
        return this._addCSSClass('ce-element--empty');                 // 3439
      } else {                                                         //
        return this._removeCSSClass('ce-element--empty');              // 3441
      }                                                                //
    };                                                                 //
                                                                       //
    Text.prototype._syncContent = function (ev) {                      // 3445
      var newSnaphot, snapshot;                                        // 3446
      snapshot = this.content.html();                                  // 3447
      this.content = new HTMLString.String(this._domElement.innerHTML, this.content.preserveWhitespace());
      newSnaphot = this.content.html();                                // 3449
      if (snapshot !== newSnaphot) {                                   // 3450
        this.taint();                                                  // 3451
      }                                                                //
      return this._flagIfEmpty();                                      // 3453
    };                                                                 //
                                                                       //
    Text.droppers = {                                                  // 3456
      'Static': ContentEdit.Element._dropVert,                         // 3457
      'Text': ContentEdit.Element._dropVert                            // 3458
    };                                                                 //
                                                                       //
    Text.mergers = {                                                   // 3461
      'Text': function (element, target) {                             // 3462
        var offset;                                                    // 3463
        offset = target.content.length();                              // 3464
        if (element.content.length()) {                                // 3465
          target.content = target.content.concat(element.content);     // 3466
        }                                                              //
        if (target.isMounted()) {                                      // 3468
          target.updateInnerHTML();                                    // 3469
        }                                                              //
        target.focus();                                                // 3471
        new ContentSelect.Range(offset, offset).select(target._domElement);
        if (element.parent()) {                                        // 3473
          element.parent().detach(element);                            // 3474
        }                                                              //
        return target.taint();                                         // 3476
      }                                                                //
    };                                                                 //
                                                                       //
    Text.fromDOMElement = function (domElement) {                      // 3480
      return new this(domElement.tagName, this.getDOMElementAttributes(domElement), domElement.innerHTML.replace(/^\s+|\s+$/g, ''));
    };                                                                 //
                                                                       //
    return Text;                                                       // 3484
  })(ContentEdit.Element);                                             //
                                                                       //
  ContentEdit.TagNames.get().register(ContentEdit.Text, 'address', 'blockquote', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p');
                                                                       //
  ContentEdit.PreText = (function (_super) {                           // 3490
    __extends(PreText, _super);                                        // 3491
                                                                       //
    function PreText(tagName, attributes, content) {                   // 3493
      if (content instanceof HTMLString.String) {                      // 3494
        this.content = content;                                        // 3495
      } else {                                                         //
        this.content = new HTMLString.String(content, true);           // 3497
      }                                                                //
      ContentEdit.Element.call(this, tagName, attributes);             // 3499
    }                                                                  //
                                                                       //
    PreText.prototype.cssTypeName = function () {                      // 3502
      return 'pre-text';                                               // 3503
    };                                                                 //
                                                                       //
    PreText.prototype.typeName = function () {                         // 3506
      return 'Preformatted';                                           // 3507
    };                                                                 //
                                                                       //
    PreText.prototype.html = function (indent) {                       // 3510
      var content;                                                     // 3511
      if (indent == null) {                                            // 3512
        indent = '';                                                   // 3513
      }                                                                //
      if (!this._lastCached || this._lastCached < this._modified) {    // 3515
        content = this.content.copy();                                 // 3516
        content.optimize();                                            // 3517
        this._lastCached = Date.now();                                 // 3518
        this._cached = content.html();                                 // 3519
      }                                                                //
      return "" + indent + "<" + this._tagName + this._attributesToString() + ">" + ("" + this._cached + "</" + this._tagName + ">");
    };                                                                 //
                                                                       //
    PreText.prototype.updateInnerHTML = function () {                  // 3524
      var html;                                                        // 3525
      html = this.content.html();                                      // 3526
      html += '\n';                                                    // 3527
      this._domElement.innerHTML = html;                               // 3528
      ContentSelect.Range.prepareElement(this._domElement);            // 3529
      return this._flagIfEmpty();                                      // 3530
    };                                                                 //
                                                                       //
    PreText.prototype._onKeyUp = function (ev) {                       // 3533
      var html, newSnaphot, snapshot;                                  // 3534
      snapshot = this.content.html();                                  // 3535
      html = this._domElement.innerHTML.replace(/[\n]$/, '');          // 3536
      this.content = new HTMLString.String(html, this.content.preserveWhitespace());
      newSnaphot = this.content.html();                                // 3538
      if (snapshot !== newSnaphot) {                                   // 3539
        this.taint();                                                  // 3540
      }                                                                //
      return this._flagIfEmpty();                                      // 3542
    };                                                                 //
                                                                       //
    PreText.prototype._keyReturn = function (ev) {                     // 3545
      var cursor, selection, tail, tip;                                // 3546
      ev.preventDefault();                                             // 3547
      selection = ContentSelect.Range.query(this._domElement);         // 3548
      cursor = selection.get()[0] + 1;                                 // 3549
      if (selection.get()[0] === 0 && selection.isCollapsed()) {       // 3550
        this.content = new HTMLString.String('\n', true).concat(this.content);
      } else if (this._atEnd(selection) && selection.isCollapsed()) {  //
        this.content = this.content.concat(new HTMLString.String('\n', true));
      } else if (selection.get()[0] === 0 && selection.get()[1] === this.content.length()) {
        this.content = new HTMLString.String('\n', true);              // 3555
        cursor = 0;                                                    // 3556
      } else {                                                         //
        tip = this.content.substring(0, selection.get()[0]);           // 3558
        tail = this.content.substring(selection.get()[1]);             // 3559
        this.content = tip.concat(new HTMLString.String('\n', true), tail);
      }                                                                //
      this.updateInnerHTML();                                          // 3562
      selection.set(cursor, cursor);                                   // 3563
      selection.select(this._domElement);                              // 3564
      return this.taint();                                             // 3565
    };                                                                 //
                                                                       //
    PreText.droppers = {                                               // 3568
      'PreText': ContentEdit.Element._dropVert,                        // 3569
      'Static': ContentEdit.Element._dropVert,                         // 3570
      'Text': ContentEdit.Element._dropVert                            // 3571
    };                                                                 //
                                                                       //
    PreText.mergers = {};                                              // 3574
                                                                       //
    PreText.fromDOMElement = function (domElement) {                   // 3576
      return new this(domElement.tagName, this.getDOMElementAttributes(domElement), domElement.innerHTML);
    };                                                                 //
                                                                       //
    return PreText;                                                    // 3580
  })(ContentEdit.Text);                                                //
                                                                       //
  ContentEdit.TagNames.get().register(ContentEdit.PreText, 'pre');     // 3584
                                                                       //
  ContentEdit.Image = (function (_super) {                             // 3586
    __extends(Image, _super);                                          // 3587
                                                                       //
    function Image(attributes, a) {                                    // 3589
      var size;                                                        // 3590
      Image.__super__.constructor.call(this, 'img', attributes);       // 3591
      this.a = a ? a : null;                                           // 3592
      size = this.size();                                              // 3593
      this._aspectRatio = size[1] / size[0];                           // 3594
    }                                                                  //
                                                                       //
    Image.prototype.cssTypeName = function () {                        // 3597
      return 'image';                                                  // 3598
    };                                                                 //
                                                                       //
    Image.prototype.typeName = function () {                           // 3601
      return 'Image';                                                  // 3602
    };                                                                 //
                                                                       //
    Image.prototype.createDraggingDOMElement = function () {           // 3605
      var helper;                                                      // 3606
      if (!this.isMounted()) {                                         // 3607
        return;                                                        // 3608
      }                                                                //
      helper = Image.__super__.createDraggingDOMElement.call(this);    // 3610
      helper.style.backgroundImage = "url(" + this._attributes['src'] + ")";
      return helper;                                                   // 3612
    };                                                                 //
                                                                       //
    Image.prototype.html = function (indent) {                         // 3615
      var attributes, img;                                             // 3616
      if (indent == null) {                                            // 3617
        indent = '';                                                   // 3618
      }                                                                //
      img = "" + indent + "<img" + this._attributesToString() + ">";   // 3620
      if (this.a) {                                                    // 3621
        attributes = ContentEdit.attributesToString(this.a);           // 3622
        attributes = "" + attributes + " data-ce-tag=\"img\"";         // 3623
        return "" + indent + "<a " + attributes + ">\n" + ("" + ContentEdit.INDENT + img + "\n") + ("" + indent + "</a>");
      } else {                                                         //
        return img;                                                    // 3626
      }                                                                //
    };                                                                 //
                                                                       //
    Image.prototype.mount = function () {                              // 3630
      var style;                                                       // 3631
      this._domElement = document.createElement('div');                // 3632
      if (this.a && this.a['class']) {                                 // 3633
        this._domElement.setAttribute('class', this.a['class']);       // 3634
      } else if (this._attributes['class']) {                          //
        this._domElement.setAttribute('class', this._attributes['class']);
      }                                                                //
      style = this._attributes['style'] ? this._attributes['style'] : '';
      style += "background-image:url(" + this._attributes['src'] + ");";
      if (this._attributes['width']) {                                 // 3640
        style += "width:" + this._attributes['width'] + "px;";         // 3641
      }                                                                //
      if (this._attributes['height']) {                                // 3643
        style += "height:" + this._attributes['height'] + "px;";       // 3644
      }                                                                //
      this._domElement.setAttribute('style', style);                   // 3646
      return Image.__super__.mount.call(this);                         // 3647
    };                                                                 //
                                                                       //
    Image.droppers = {                                                 // 3650
      'Image': ContentEdit.Element._dropBoth,                          // 3651
      'PreText': ContentEdit.Element._dropBoth,                        // 3652
      'Static': ContentEdit.Element._dropBoth,                         // 3653
      'Text': ContentEdit.Element._dropBoth                            // 3654
    };                                                                 //
                                                                       //
    Image.placements = ['above', 'below', 'left', 'right', 'center'];  // 3657
                                                                       //
    Image.fromDOMElement = function (domElement) {                     // 3659
      var a, attributes, c, childNode, childNodes, _i, _len;           // 3660
      a = null;                                                        // 3661
      if (domElement.tagName.toLowerCase() === 'a') {                  // 3662
        a = this.getDOMElementAttributes(domElement);                  // 3663
        childNodes = (function () {                                    // 3664
          var _i, _len, _ref, _results;                                // 3665
          _ref = domElement.childNodes;                                // 3666
          _results = [];                                               // 3667
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {          // 3668
            c = _ref[_i];                                              // 3669
            _results.push(c);                                          // 3670
          }                                                            //
          return _results;                                             // 3672
        })();                                                          //
        for (_i = 0, _len = childNodes.length; _i < _len; _i++) {      // 3674
          childNode = childNodes[_i];                                  // 3675
          if (childNode.nodeType === 1 && childNode.tagName.toLowerCase() === 'img') {
            domElement = childNode;                                    // 3677
            break;                                                     // 3678
          }                                                            //
        }                                                              //
        if (domElement.tagName.toLowerCase() === 'a') {                // 3681
          domElement = document.createElement('img');                  // 3682
        }                                                              //
      }                                                                //
      attributes = this.getDOMElementAttributes(domElement);           // 3685
      if (attributes['width'] === void 0) {                            // 3686
        if (attributes['height'] === void 0) {                         // 3687
          attributes['width'] = domElement.naturalWidth;               // 3688
        } else {                                                       //
          attributes['width'] = domElement.clientWidth;                // 3690
        }                                                              //
      }                                                                //
      if (attributes['height'] === void 0) {                           // 3693
        if (attributes['width'] === void 0) {                          // 3694
          attributes['height'] = domElement.naturalHeight;             // 3695
        } else {                                                       //
          attributes['height'] = domElement.clientHeight;              // 3697
        }                                                              //
      }                                                                //
      return new this(attributes, a);                                  // 3700
    };                                                                 //
                                                                       //
    return Image;                                                      // 3703
  })(ContentEdit.ResizableElement);                                    //
                                                                       //
  ContentEdit.TagNames.get().register(ContentEdit.Image, 'img');       // 3707
                                                                       //
  ContentEdit.Video = (function (_super) {                             // 3709
    __extends(Video, _super);                                          // 3710
                                                                       //
    function Video(tagName, attributes, sources) {                     // 3712
      var size;                                                        // 3713
      if (sources == null) {                                           // 3714
        sources = [];                                                  // 3715
      }                                                                //
      Video.__super__.constructor.call(this, tagName, attributes);     // 3717
      this.sources = sources;                                          // 3718
      size = this.size();                                              // 3719
      this._aspectRatio = size[1] / size[0];                           // 3720
    }                                                                  //
                                                                       //
    Video.prototype.cssTypeName = function () {                        // 3723
      return 'video';                                                  // 3724
    };                                                                 //
                                                                       //
    Video.prototype.typeName = function () {                           // 3727
      return 'Video';                                                  // 3728
    };                                                                 //
                                                                       //
    Video.prototype._title = function () {                             // 3731
      var src;                                                         // 3732
      src = '';                                                        // 3733
      if (this.attr('src')) {                                          // 3734
        src = this.attr('src');                                        // 3735
      } else {                                                         //
        if (this.sources.length) {                                     // 3737
          src = this.sources[0]['src'];                                // 3738
        }                                                              //
      }                                                                //
      if (!src) {                                                      // 3741
        src = 'No video source set';                                   // 3742
      }                                                                //
      if (src.length > ContentEdit.HELPER_CHAR_LIMIT) {                // 3744
        src = text.substr(0, ContentEdit.HELPER_CHAR_LIMIT);           // 3745
      }                                                                //
      return src;                                                      // 3747
    };                                                                 //
                                                                       //
    Video.prototype.createDraggingDOMElement = function () {           // 3750
      var helper;                                                      // 3751
      if (!this.isMounted()) {                                         // 3752
        return;                                                        // 3753
      }                                                                //
      helper = Video.__super__.createDraggingDOMElement.call(this);    // 3755
      helper.innerHTML = this._title();                                // 3756
      return helper;                                                   // 3757
    };                                                                 //
                                                                       //
    Video.prototype.html = function (indent) {                         // 3760
      var attributes, source, sourceStrings, _i, _len, _ref;           // 3761
      if (indent == null) {                                            // 3762
        indent = '';                                                   // 3763
      }                                                                //
      if (this.tagName() === 'video') {                                // 3765
        sourceStrings = [];                                            // 3766
        _ref = this.sources;                                           // 3767
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 3768
          source = _ref[_i];                                           // 3769
          attributes = ContentEdit.attributesToString(source);         // 3770
          sourceStrings.push("" + indent + ContentEdit.INDENT + "<source " + attributes + ">");
        }                                                              //
        return "" + indent + "<video" + this._attributesToString() + ">\n" + sourceStrings.join('\n') + ("\n" + indent + "</video>");
      } else {                                                         //
        return "" + indent + "<" + this._tagName + this._attributesToString() + ">" + ("</" + this._tagName + ">");
      }                                                                //
    };                                                                 //
                                                                       //
    Video.prototype.mount = function () {                              // 3779
      var style;                                                       // 3780
      this._domElement = document.createElement('div');                // 3781
      if (this.a && this.a['class']) {                                 // 3782
        this._domElement.setAttribute('class', this.a['class']);       // 3783
      } else if (this._attributes['class']) {                          //
        this._domElement.setAttribute('class', this._attributes['class']);
      }                                                                //
      style = this._attributes['style'] ? this._attributes['style'] : '';
      if (this._attributes['width']) {                                 // 3788
        style += "width:" + this._attributes['width'] + "px;";         // 3789
      }                                                                //
      if (this._attributes['height']) {                                // 3791
        style += "height:" + this._attributes['height'] + "px;";       // 3792
      }                                                                //
      this._domElement.setAttribute('style', style);                   // 3794
      this._domElement.setAttribute('data-ce-title', this._title());   // 3795
      return Video.__super__.mount.call(this);                         // 3796
    };                                                                 //
                                                                       //
    Video.droppers = {                                                 // 3799
      'Image': ContentEdit.Element._dropBoth,                          // 3800
      'PreText': ContentEdit.Element._dropBoth,                        // 3801
      'Static': ContentEdit.Element._dropBoth,                         // 3802
      'Text': ContentEdit.Element._dropBoth,                           // 3803
      'Video': ContentEdit.Element._dropBoth                           // 3804
    };                                                                 //
                                                                       //
    Video.placements = ['above', 'below', 'left', 'right', 'center'];  // 3807
                                                                       //
    Video.fromDOMElement = function (domElement) {                     // 3809
      var c, childNode, childNodes, sources, _i, _len;                 // 3810
      childNodes = (function () {                                      // 3811
        var _i, _len, _ref, _results;                                  // 3812
        _ref = domElement.childNodes;                                  // 3813
        _results = [];                                                 // 3814
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 3815
          c = _ref[_i];                                                // 3816
          _results.push(c);                                            // 3817
        }                                                              //
        return _results;                                               // 3819
      })();                                                            //
      sources = [];                                                    // 3821
      for (_i = 0, _len = childNodes.length; _i < _len; _i++) {        // 3822
        childNode = childNodes[_i];                                    // 3823
        if (childNode.nodeType === 1 && childNode.tagName.toLowerCase() === 'source') {
          sources.push(this.getDOMElementAttributes(childNode));       // 3825
        }                                                              //
      }                                                                //
      return new this(domElement.tagName, this.getDOMElementAttributes(domElement), sources);
    };                                                                 //
                                                                       //
    return Video;                                                      // 3831
  })(ContentEdit.ResizableElement);                                    //
                                                                       //
  ContentEdit.TagNames.get().register(ContentEdit.Video, 'iframe', 'video');
                                                                       //
  ContentEdit.List = (function (_super) {                              // 3837
    __extends(List, _super);                                           // 3838
                                                                       //
    function List(tagName, attributes) {                               // 3840
      List.__super__.constructor.call(this, tagName, attributes);      // 3841
    }                                                                  //
                                                                       //
    List.prototype.cssTypeName = function () {                         // 3844
      return 'list';                                                   // 3845
    };                                                                 //
                                                                       //
    List.prototype.typeName = function () {                            // 3848
      return 'List';                                                   // 3849
    };                                                                 //
                                                                       //
    List.prototype._onMouseOver = function (ev) {                      // 3852
      if (this.parent().constructor.name === 'ListItem') {             // 3853
        return;                                                        // 3854
      }                                                                //
      List.__super__._onMouseOver.call(this, ev);                      // 3856
      return this._removeCSSClass('ce-element--over');                 // 3857
    };                                                                 //
                                                                       //
    List.droppers = {                                                  // 3860
      'Image': ContentEdit.Element._dropBoth,                          // 3861
      'List': ContentEdit.Element._dropVert,                           // 3862
      'PreText': ContentEdit.Element._dropVert,                        // 3863
      'Static': ContentEdit.Element._dropVert,                         // 3864
      'Text': ContentEdit.Element._dropVert,                           // 3865
      'Video': ContentEdit.Element._dropBoth                           // 3866
    };                                                                 //
                                                                       //
    List.fromDOMElement = function (domElement) {                      // 3869
      var c, childNode, childNodes, list, _i, _len;                    // 3870
      list = new this(domElement.tagName, this.getDOMElementAttributes(domElement));
      childNodes = (function () {                                      // 3872
        var _i, _len, _ref, _results;                                  // 3873
        _ref = domElement.childNodes;                                  // 3874
        _results = [];                                                 // 3875
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 3876
          c = _ref[_i];                                                // 3877
          _results.push(c);                                            // 3878
        }                                                              //
        return _results;                                               // 3880
      })();                                                            //
      for (_i = 0, _len = childNodes.length; _i < _len; _i++) {        // 3882
        childNode = childNodes[_i];                                    // 3883
        if (childNode.nodeType !== 1) {                                // 3884
          continue;                                                    // 3885
        }                                                              //
        if (childNode.tagName.toLowerCase() !== 'li') {                // 3887
          continue;                                                    // 3888
        }                                                              //
        list.attach(ContentEdit.ListItem.fromDOMElement(childNode));   // 3890
      }                                                                //
      if (list.children.length === 0) {                                // 3892
        return null;                                                   // 3893
      }                                                                //
      return list;                                                     // 3895
    };                                                                 //
                                                                       //
    return List;                                                       // 3898
  })(ContentEdit.ElementCollection);                                   //
                                                                       //
  ContentEdit.TagNames.get().register(ContentEdit.List, 'ol', 'ul');   // 3902
                                                                       //
  ContentEdit.ListItem = (function (_super) {                          // 3904
    __extends(ListItem, _super);                                       // 3905
                                                                       //
    function ListItem(attributes) {                                    // 3907
      ListItem.__super__.constructor.call(this, 'li', attributes);     // 3908
    }                                                                  //
                                                                       //
    ListItem.prototype.cssTypeName = function () {                     // 3911
      return 'list-item';                                              // 3912
    };                                                                 //
                                                                       //
    ListItem.prototype.list = function () {                            // 3915
      if (this.children.length === 2) {                                // 3916
        return this.children[1];                                       // 3917
      }                                                                //
      return null;                                                     // 3919
    };                                                                 //
                                                                       //
    ListItem.prototype.listItemText = function () {                    // 3922
      if (this.children.length > 0) {                                  // 3923
        return this.children[0];                                       // 3924
      }                                                                //
      return null;                                                     // 3926
    };                                                                 //
                                                                       //
    ListItem.prototype.html = function (indent) {                      // 3929
      var lines;                                                       // 3930
      if (indent == null) {                                            // 3931
        indent = '';                                                   // 3932
      }                                                                //
      lines = ["" + indent + "<li" + this._attributesToString() + ">"];
      if (this.listItemText()) {                                       // 3935
        lines.push(this.listItemText().html(indent + ContentEdit.INDENT));
      }                                                                //
      if (this.list()) {                                               // 3938
        lines.push(this.list().html(indent + ContentEdit.INDENT));     // 3939
      }                                                                //
      lines.push("" + indent + "</li>");                               // 3941
      return lines.join('\n');                                         // 3942
    };                                                                 //
                                                                       //
    ListItem.prototype.indent = function () {                          // 3945
      var sibling;                                                     // 3946
      if (this.parent().children.indexOf(this) === 0) {                // 3947
        return;                                                        // 3948
      }                                                                //
      sibling = this.previousSibling();                                // 3950
      if (!sibling.list()) {                                           // 3951
        sibling.attach(new ContentEdit.List(sibling.parent().tagName()));
      }                                                                //
      this.listItemText().storeState();                                // 3954
      this.parent().detach(this);                                      // 3955
      sibling.list().attach(this);                                     // 3956
      return this.listItemText().restoreState();                       // 3957
    };                                                                 //
                                                                       //
    ListItem.prototype.remove = function () {                          // 3960
      var child, i, index, _i, _len, _ref;                             // 3961
      if (!this.parent()) {                                            // 3962
        return;                                                        // 3963
      }                                                                //
      index = this.parent().children.indexOf(this);                    // 3965
      if (this.list()) {                                               // 3966
        _ref = this.list().children.slice();                           // 3967
        for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {    // 3968
          child = _ref[i];                                             // 3969
          child.parent().detach(child);                                // 3970
          this.parent().attach(child, i + index);                      // 3971
        }                                                              //
      }                                                                //
      return this.parent().detach(this);                               // 3974
    };                                                                 //
                                                                       //
    ListItem.prototype.unindent = function () {                        // 3977
      var child, grandParent, i, itemIndex, list, parent, parentIndex, selection, sibling, siblings, text, _i, _j, _k, _l, _len, _len1, _len2, _len3, _ref, _ref1;
      parent = this.parent();                                          // 3979
      grandParent = parent.parent();                                   // 3980
      siblings = parent.children.slice(parent.children.indexOf(this) + 1, parent.children.length);
      if (grandParent.constructor.name === 'ListItem') {               // 3982
        this.listItemText().storeState();                              // 3983
        parent.detach(this);                                           // 3984
        grandParent.parent().attach(this, grandParent.parent().children.indexOf(grandParent) + 1);
        if (siblings.length && !this.list()) {                         // 3986
          this.attach(new ContentEdit.List(parent.tagName()));         // 3987
        }                                                              //
        for (_i = 0, _len = siblings.length; _i < _len; _i++) {        // 3989
          sibling = siblings[_i];                                      // 3990
          sibling.parent().detach(sibling);                            // 3991
          this.list().attach(sibling);                                 // 3992
        }                                                              //
        return this.listItemText().restoreState();                     // 3994
      } else {                                                         //
        text = new ContentEdit.Text('p', this.attr('class') ? {        // 3996
          'class': this.attr('class')                                  // 3997
        } : {}, this.listItemText().content);                          //
        selection = null;                                              // 3999
        if (this.listItemText().isFocused()) {                         // 4000
          selection = ContentSelect.Range.query(this.listItemText().domElement());
        }                                                              //
        parentIndex = grandParent.children.indexOf(parent);            // 4003
        itemIndex = parent.children.indexOf(this);                     // 4004
        if (itemIndex === 0) {                                         // 4005
          list = null;                                                 // 4006
          if (parent.children.length === 1) {                          // 4007
            if (this.list()) {                                         // 4008
              list = new ContentEdit.List(parent.tagName());           // 4009
            }                                                          //
            grandParent.detach(parent);                                // 4011
          } else {                                                     //
            parent.detach(this);                                       // 4013
          }                                                            //
          grandParent.attach(text, parentIndex);                       // 4015
          if (list) {                                                  // 4016
            grandParent.attach(list, parentIndex + 1);                 // 4017
          }                                                            //
          if (this.list()) {                                           // 4019
            _ref = this.list().children.slice();                       // 4020
            for (i = _j = 0, _len1 = _ref.length; _j < _len1; i = ++_j) {
              child = _ref[i];                                         // 4022
              child.parent().detach(child);                            // 4023
              if (list) {                                              // 4024
                list.attach(child);                                    // 4025
              } else {                                                 //
                parent.attach(child, i);                               // 4027
              }                                                        //
            }                                                          //
          }                                                            //
        } else if (itemIndex === parent.children.length - 1) {         //
          parent.detach(this);                                         // 4032
          grandParent.attach(text, parentIndex + 1);                   // 4033
          if (this.list()) {                                           // 4034
            grandParent.attach(this.list(), parentIndex + 2);          // 4035
          }                                                            //
        } else {                                                       //
          parent.detach(this);                                         // 4038
          grandParent.attach(text, parentIndex + 1);                   // 4039
          list = new ContentEdit.List(parent.tagName());               // 4040
          grandParent.attach(list, parentIndex + 2);                   // 4041
          if (this.list()) {                                           // 4042
            _ref1 = this.list().children.slice();                      // 4043
            for (_k = 0, _len2 = _ref1.length; _k < _len2; _k++) {     // 4044
              child = _ref1[_k];                                       // 4045
              child.parent().detach(child);                            // 4046
              list.attach(child);                                      // 4047
            }                                                          //
          }                                                            //
          for (_l = 0, _len3 = siblings.length; _l < _len3; _l++) {    // 4050
            sibling = siblings[_l];                                    // 4051
            sibling.parent().detach(sibling);                          // 4052
            list.attach(sibling);                                      // 4053
          }                                                            //
        }                                                              //
        if (selection) {                                               // 4056
          text.focus();                                                // 4057
          return selection.select(text.domElement());                  // 4058
        }                                                              //
      }                                                                //
    };                                                                 //
                                                                       //
    ListItem.prototype._onMouseOver = function (ev) {                  // 4063
      ListItem.__super__._onMouseOver.call(this, ev);                  // 4064
      return this._removeCSSClass('ce-element--over');                 // 4065
    };                                                                 //
                                                                       //
    ListItem.prototype._addDOMEventListeners = function () {};         // 4068
                                                                       //
    ListItem.prototype._removeDOMEventListners = function () {};       // 4070
                                                                       //
    ListItem.fromDOMElement = function (domElement) {                  // 4072
      var childNode, content, listDOMElement, listElement, listItem, listItemText, _i, _len, _ref, _ref1;
      listItem = new this(this.getDOMElementAttributes(domElement));   // 4074
      content = '';                                                    // 4075
      listDOMElement = null;                                           // 4076
      _ref = domElement.childNodes;                                    // 4077
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 4078
        childNode = _ref[_i];                                          // 4079
        if (childNode.nodeType === 1) {                                // 4080
          if ((_ref1 = childNode.tagName.toLowerCase()) === 'ul' || _ref1 === 'li') {
            if (!listDOMElement) {                                     // 4082
              listDOMElement = childNode;                              // 4083
            }                                                          //
          } else {                                                     //
            content += childNode.outerHTML;                            // 4086
          }                                                            //
        } else {                                                       //
          content += HTMLString.String.encode(childNode.textContent);  // 4089
        }                                                              //
      }                                                                //
      content = content.replace(/^\s+|\s+$/g, '');                     // 4092
      listItemText = new ContentEdit.ListItemText(content);            // 4093
      listItem.attach(listItemText);                                   // 4094
      if (listDOMElement) {                                            // 4095
        listElement = ContentEdit.List.fromDOMElement(listDOMElement);
        listItem.attach(listElement);                                  // 4097
      }                                                                //
      return listItem;                                                 // 4099
    };                                                                 //
                                                                       //
    return ListItem;                                                   // 4102
  })(ContentEdit.ElementCollection);                                   //
                                                                       //
  ContentEdit.ListItemText = (function (_super) {                      // 4106
    __extends(ListItemText, _super);                                   // 4107
                                                                       //
    function ListItemText(content) {                                   // 4109
      ListItemText.__super__.constructor.call(this, 'div', {}, content);
    }                                                                  //
                                                                       //
    ListItemText.prototype.cssTypeName = function () {                 // 4113
      return 'list-item-text';                                         // 4114
    };                                                                 //
                                                                       //
    ListItemText.prototype.typeName = function () {                    // 4117
      return 'List item';                                              // 4118
    };                                                                 //
                                                                       //
    ListItemText.prototype.blur = function () {                        // 4121
      if (this.content.isWhitespace()) {                               // 4122
        this.parent().remove();                                        // 4123
      } else if (this.isMounted()) {                                   //
        this._domElement.blur();                                       // 4125
        this._domElement.removeAttribute('contenteditable');           // 4126
      }                                                                //
      return ContentEdit.Element.prototype.blur.call(this);            // 4128
    };                                                                 //
                                                                       //
    ListItemText.prototype.html = function (indent) {                  // 4131
      var content;                                                     // 4132
      if (indent == null) {                                            // 4133
        indent = '';                                                   // 4134
      }                                                                //
      if (!this._lastCached || this._lastCached < this._modified) {    // 4136
        content = this.content.copy();                                 // 4137
        content.optimize();                                            // 4138
        this._lastCached = Date.now();                                 // 4139
        this._cached = content.html();                                 // 4140
      }                                                                //
      return "" + indent + this._cached;                               // 4142
    };                                                                 //
                                                                       //
    ListItemText.prototype._onMouseDown = function (ev) {              // 4145
      var initDrag;                                                    // 4146
      ContentEdit.Element.prototype._onMouseDown.call(this, ev);       // 4147
      initDrag = (function (_this) {                                   // 4148
        return function () {                                           // 4149
          var listRoot;                                                // 4150
          if (ContentEdit.Root.get().dragging() === _this) {           // 4151
            ContentEdit.Root.get().cancelDragging();                   // 4152
            listRoot = _this.closest(function (node) {                 // 4153
              return node.parent().constructor.name === 'Region';      // 4154
            });                                                        //
            return listRoot.drag(ev.pageX, ev.pageY);                  // 4156
          } else {                                                     //
            _this.drag(ev.pageX, ev.pageY);                            // 4158
            return _this._dragTimeout = setTimeout(initDrag, ContentEdit.DRAG_HOLD_DURATION * 2);
          }                                                            //
        };                                                             //
      })(this);                                                        //
      clearTimeout(this._dragTimeout);                                 // 4163
      return this._dragTimeout = setTimeout(initDrag, ContentEdit.DRAG_HOLD_DURATION);
    };                                                                 //
                                                                       //
    ListItemText.prototype._onMouseMove = function (ev) {              // 4167
      if (this._dragTimeout) {                                         // 4168
        clearTimeout(this._dragTimeout);                               // 4169
      }                                                                //
      return ContentEdit.Element.prototype._onMouseMove.call(this, ev);
    };                                                                 //
                                                                       //
    ListItemText.prototype._onMouseUp = function (ev) {                // 4174
      if (this._dragTimeout) {                                         // 4175
        clearTimeout(this._dragTimeout);                               // 4176
      }                                                                //
      return ContentEdit.Element.prototype._onMouseUp.call(this, ev);  // 4178
    };                                                                 //
                                                                       //
    ListItemText.prototype._keyTab = function (ev) {                   // 4181
      ev.preventDefault();                                             // 4182
      if (ev.shiftKey) {                                               // 4183
        return this.parent().unindent();                               // 4184
      } else {                                                         //
        return this.parent().indent();                                 // 4186
      }                                                                //
    };                                                                 //
                                                                       //
    ListItemText.prototype._keyReturn = function (ev) {                // 4190
      var grandParent, list, listItem, selection, tail, tip;           // 4191
      ev.preventDefault();                                             // 4192
      if (this.content.isWhitespace()) {                               // 4193
        this.parent().unindent();                                      // 4194
        return;                                                        // 4195
      }                                                                //
      ContentSelect.Range.query(this._domElement);                     // 4197
      selection = ContentSelect.Range.query(this._domElement);         // 4198
      tip = this.content.substring(0, selection.get()[0]);             // 4199
      tail = this.content.substring(selection.get()[1]);               // 4200
      if (tip.length() + tail.length() === 0) {                        // 4201
        this.parent().unindent();                                      // 4202
        return;                                                        // 4203
      }                                                                //
      this.content = tip.trim();                                       // 4205
      this.updateInnerHTML();                                          // 4206
      grandParent = this.parent().parent();                            // 4207
      listItem = new ContentEdit.ListItem(this.attr('class') ? {       // 4208
        'class': this.attr('class')                                    // 4209
      } : {});                                                         //
      grandParent.attach(listItem, grandParent.children.indexOf(this.parent()) + 1);
      listItem.attach(new ContentEdit.ListItemText(tail.trim()));      // 4212
      list = this.parent().list();                                     // 4213
      if (list) {                                                      // 4214
        this.parent().detach(list);                                    // 4215
        listItem.attach(list);                                         // 4216
      }                                                                //
      if (tip.length()) {                                              // 4218
        listItem.listItemText().focus();                               // 4219
        selection = new ContentSelect.Range(0, 0);                     // 4220
        return selection.select(listItem.listItemText().domElement());
      } else {                                                         //
        selection = new ContentSelect.Range(0, tip.length());          // 4223
        return selection.select(this._domElement);                     // 4224
      }                                                                //
    };                                                                 //
                                                                       //
    ListItemText.droppers = {                                          // 4228
      'ListItemText': function (element, target, placement) {          // 4229
        var elementParent, insertIndex, listItem, targetParent;        // 4230
        elementParent = element.parent();                              // 4231
        targetParent = target.parent();                                // 4232
        elementParent.remove();                                        // 4233
        elementParent.detach(element);                                 // 4234
        listItem = new ContentEdit.ListItem(elementParent._attributes);
        listItem.attach(element);                                      // 4236
        if (targetParent.list() && placement[0] === 'below') {         // 4237
          targetParent.list().attach(listItem, 0);                     // 4238
          return;                                                      // 4239
        }                                                              //
        insertIndex = targetParent.parent().children.indexOf(targetParent);
        if (placement[0] === 'below') {                                // 4242
          insertIndex += 1;                                            // 4243
        }                                                              //
        return targetParent.parent().attach(listItem, insertIndex);    // 4245
      },                                                               //
      'Text': function (element, target, placement) {                  // 4247
        var cssClass, insertIndex, listItem, targetParent, text;       // 4248
        if (element.constructor.name === 'Text') {                     // 4249
          targetParent = target.parent();                              // 4250
          element.parent().detach(element);                            // 4251
          cssClass = element.attr('class');                            // 4252
          listItem = new ContentEdit.ListItem(cssClass ? {             // 4253
            'class': cssClass                                          // 4254
          } : {});                                                     //
          listItem.attach(new ContentEdit.ListItemText(element.content));
          if (targetParent.list() && placement[0] === 'below') {       // 4257
            targetParent.list().attach(listItem, 0);                   // 4258
            return;                                                    // 4259
          }                                                            //
          insertIndex = targetParent.parent().children.indexOf(targetParent);
          if (placement[0] === 'below') {                              // 4262
            insertIndex += 1;                                          // 4263
          }                                                            //
          targetParent.parent().attach(listItem, insertIndex);         // 4265
          listItem.listItemText().focus();                             // 4266
          if (element._savedSelection) {                               // 4267
            return element._savedSelection.select(listItem.listItemText().domElement());
          }                                                            //
        } else {                                                       //
          cssClass = element.attr('class');                            // 4271
          text = new ContentEdit.Text('p', cssClass ? {                // 4272
            'class': cssClass                                          // 4273
          } : {}, element.content);                                    //
          element.parent().remove();                                   // 4275
          insertIndex = target.parent().children.indexOf(target);      // 4276
          if (placement[0] === 'below') {                              // 4277
            insertIndex += 1;                                          // 4278
          }                                                            //
          target.parent().attach(text, insertIndex);                   // 4280
          text.focus();                                                // 4281
          if (element._savedSelection) {                               // 4282
            return element._savedSelection.select(text.domElement());  // 4283
          }                                                            //
        }                                                              //
      }                                                                //
    };                                                                 //
                                                                       //
    ListItemText.mergers = {                                           // 4289
      'ListItemText': function (element, target) {                     // 4290
        var offset;                                                    // 4291
        offset = target.content.length();                              // 4292
        if (element.content.length()) {                                // 4293
          target.content = target.content.concat(element.content);     // 4294
        }                                                              //
        if (target.isMounted()) {                                      // 4296
          target._domElement.innerHTML = target.content.html();        // 4297
        }                                                              //
        target.focus();                                                // 4299
        new ContentSelect.Range(offset, offset).select(target._domElement);
        if (element.constructor.name === 'Text') {                     // 4301
          if (element.parent()) {                                      // 4302
            element.parent().detach(element);                          // 4303
          }                                                            //
        } else {                                                       //
          element.parent().remove();                                   // 4306
        }                                                              //
        return target.taint();                                         // 4308
      }                                                                //
    };                                                                 //
                                                                       //
    return ListItemText;                                               // 4312
  })(ContentEdit.Text);                                                //
                                                                       //
  _mergers = ContentEdit.ListItemText.mergers;                         // 4316
                                                                       //
  _mergers['Text'] = _mergers['ListItemText'];                         // 4318
                                                                       //
  ContentEdit.Table = (function (_super) {                             // 4320
    __extends(Table, _super);                                          // 4321
                                                                       //
    function Table(attributes) {                                       // 4323
      Table.__super__.constructor.call(this, 'table', attributes);     // 4324
    }                                                                  //
                                                                       //
    Table.prototype.cssTypeName = function () {                        // 4327
      return 'table';                                                  // 4328
    };                                                                 //
                                                                       //
    Table.prototype.typeName = function () {                           // 4331
      return 'Table';                                                  // 4332
    };                                                                 //
                                                                       //
    Table.prototype.firstSection = function () {                       // 4335
      var section;                                                     // 4336
      if (section = this.thead()) {                                    // 4337
        return section;                                                // 4338
      } else if (section = this.tbody()) {                             //
        return section;                                                // 4340
      } else if (section = this.tfoot()) {                             //
        return section;                                                // 4342
      }                                                                //
      return null;                                                     // 4344
    };                                                                 //
                                                                       //
    Table.prototype.lastSection = function () {                        // 4347
      var section;                                                     // 4348
      if (section = this.tfoot()) {                                    // 4349
        return section;                                                // 4350
      } else if (section = this.tbody()) {                             //
        return section;                                                // 4352
      } else if (section = this.thead()) {                             //
        return section;                                                // 4354
      }                                                                //
      return null;                                                     // 4356
    };                                                                 //
                                                                       //
    Table.prototype.tbody = function () {                              // 4359
      return this._getChild('tbody');                                  // 4360
    };                                                                 //
                                                                       //
    Table.prototype.tfoot = function () {                              // 4363
      return this._getChild('tfoot');                                  // 4364
    };                                                                 //
                                                                       //
    Table.prototype.thead = function () {                              // 4367
      return this._getChild('thead');                                  // 4368
    };                                                                 //
                                                                       //
    Table.prototype._onMouseOver = function (ev) {                     // 4371
      Table.__super__._onMouseOver.call(this, ev);                     // 4372
      return this._removeCSSClass('ce-element--over');                 // 4373
    };                                                                 //
                                                                       //
    Table.prototype._getChild = function (tagName) {                   // 4376
      var child, _i, _len, _ref;                                       // 4377
      _ref = this.children;                                            // 4378
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 4379
        child = _ref[_i];                                              // 4380
        if (child.tagName() === tagName) {                             // 4381
          return child;                                                // 4382
        }                                                              //
      }                                                                //
      return null;                                                     // 4385
    };                                                                 //
                                                                       //
    Table.droppers = {                                                 // 4388
      'Image': ContentEdit.Element._dropBoth,                          // 4389
      'List': ContentEdit.Element._dropVert,                           // 4390
      'PreText': ContentEdit.Element._dropVert,                        // 4391
      'Static': ContentEdit.Element._dropVert,                         // 4392
      'Table': ContentEdit.Element._dropVert,                          // 4393
      'Text': ContentEdit.Element._dropVert,                           // 4394
      'Video': ContentEdit.Element._dropBoth                           // 4395
    };                                                                 //
                                                                       //
    Table.fromDOMElement = function (domElement) {                     // 4398
      var c, childNode, childNodes, orphanRows, row, section, table, tagName, _i, _j, _len, _len1;
      table = new this(this.getDOMElementAttributes(domElement));      // 4400
      childNodes = (function () {                                      // 4401
        var _i, _len, _ref, _results;                                  // 4402
        _ref = domElement.childNodes;                                  // 4403
        _results = [];                                                 // 4404
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 4405
          c = _ref[_i];                                                // 4406
          _results.push(c);                                            // 4407
        }                                                              //
        return _results;                                               // 4409
      })();                                                            //
      orphanRows = [];                                                 // 4411
      for (_i = 0, _len = childNodes.length; _i < _len; _i++) {        // 4412
        childNode = childNodes[_i];                                    // 4413
        if (childNode.nodeType !== 1) {                                // 4414
          continue;                                                    // 4415
        }                                                              //
        tagName = childNode.tagName.toLowerCase();                     // 4417
        if (table._getChild(tagName)) {                                // 4418
          continue;                                                    // 4419
        }                                                              //
        switch (tagName) {                                             // 4421
          case 'tbody':                                                // 4422
          case 'tfoot':                                                // 4423
          case 'thead':                                                // 4424
            section = ContentEdit.TableSection.fromDOMElement(childNode);
            table.attach(section);                                     // 4426
            break;                                                     // 4427
          case 'tr':                                                   // 4428
            orphanRows.push(ContentEdit.TableRow.fromDOMElement(childNode));
        }                                                              // 4429
      }                                                                //
      if (orphanRows.length > 0) {                                     // 4432
        if (!table._getChild('tbody')) {                               // 4433
          table.attach(new ContentEdit.TableSection('tbody'));         // 4434
        }                                                              //
        for (_j = 0, _len1 = orphanRows.length; _j < _len1; _j++) {    // 4436
          row = orphanRows[_j];                                        // 4437
          table.tbody().attach(row);                                   // 4438
        }                                                              //
      }                                                                //
      if (table.children.length === 0) {                               // 4441
        return null;                                                   // 4442
      }                                                                //
      return table;                                                    // 4444
    };                                                                 //
                                                                       //
    return Table;                                                      // 4447
  })(ContentEdit.ElementCollection);                                   //
                                                                       //
  ContentEdit.TagNames.get().register(ContentEdit.Table, 'table');     // 4451
                                                                       //
  ContentEdit.TableSection = (function (_super) {                      // 4453
    __extends(TableSection, _super);                                   // 4454
                                                                       //
    function TableSection(tagName, attributes) {                       // 4456
      TableSection.__super__.constructor.call(this, tagName, attributes);
    }                                                                  //
                                                                       //
    TableSection.prototype.cssTypeName = function () {                 // 4460
      return 'table-section';                                          // 4461
    };                                                                 //
                                                                       //
    TableSection.prototype._onMouseOver = function (ev) {              // 4464
      TableSection.__super__._onMouseOver.call(this, ev);              // 4465
      return this._removeCSSClass('ce-element--over');                 // 4466
    };                                                                 //
                                                                       //
    TableSection.fromDOMElement = function (domElement) {              // 4469
      var c, childNode, childNodes, section, _i, _len;                 // 4470
      section = new this(domElement.tagName, this.getDOMElementAttributes(domElement));
      childNodes = (function () {                                      // 4472
        var _i, _len, _ref, _results;                                  // 4473
        _ref = domElement.childNodes;                                  // 4474
        _results = [];                                                 // 4475
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 4476
          c = _ref[_i];                                                // 4477
          _results.push(c);                                            // 4478
        }                                                              //
        return _results;                                               // 4480
      })();                                                            //
      for (_i = 0, _len = childNodes.length; _i < _len; _i++) {        // 4482
        childNode = childNodes[_i];                                    // 4483
        if (childNode.nodeType !== 1) {                                // 4484
          continue;                                                    // 4485
        }                                                              //
        if (childNode.tagName.toLowerCase() !== 'tr') {                // 4487
          continue;                                                    // 4488
        }                                                              //
        section.attach(ContentEdit.TableRow.fromDOMElement(childNode));
      }                                                                //
      return section;                                                  // 4492
    };                                                                 //
                                                                       //
    return TableSection;                                               // 4495
  })(ContentEdit.ElementCollection);                                   //
                                                                       //
  ContentEdit.TableRow = (function (_super) {                          // 4499
    __extends(TableRow, _super);                                       // 4500
                                                                       //
    function TableRow(attributes) {                                    // 4502
      TableRow.__super__.constructor.call(this, 'tr', attributes);     // 4503
    }                                                                  //
                                                                       //
    TableRow.prototype.cssTypeName = function () {                     // 4506
      return 'table-row';                                              // 4507
    };                                                                 //
                                                                       //
    TableRow.prototype.typeName = function () {                        // 4510
      return 'Table row';                                              // 4511
    };                                                                 //
                                                                       //
    TableRow.prototype._onMouseOver = function (ev) {                  // 4514
      TableRow.__super__._onMouseOver.call(this, ev);                  // 4515
      return this._removeCSSClass('ce-element--over');                 // 4516
    };                                                                 //
                                                                       //
    TableRow.droppers = {                                              // 4519
      'TableRow': ContentEdit.Element._dropVert                        // 4520
    };                                                                 //
                                                                       //
    TableRow.fromDOMElement = function (domElement) {                  // 4523
      var c, childNode, childNodes, row, tagName, _i, _len;            // 4524
      row = new this(this.getDOMElementAttributes(domElement));        // 4525
      childNodes = (function () {                                      // 4526
        var _i, _len, _ref, _results;                                  // 4527
        _ref = domElement.childNodes;                                  // 4528
        _results = [];                                                 // 4529
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 4530
          c = _ref[_i];                                                // 4531
          _results.push(c);                                            // 4532
        }                                                              //
        return _results;                                               // 4534
      })();                                                            //
      for (_i = 0, _len = childNodes.length; _i < _len; _i++) {        // 4536
        childNode = childNodes[_i];                                    // 4537
        if (childNode.nodeType !== 1) {                                // 4538
          continue;                                                    // 4539
        }                                                              //
        tagName = childNode.tagName.toLowerCase();                     // 4541
        if (!(tagName === 'td' || tagName === 'th')) {                 // 4542
          continue;                                                    // 4543
        }                                                              //
        row.attach(ContentEdit.TableCell.fromDOMElement(childNode));   // 4545
      }                                                                //
      return row;                                                      // 4547
    };                                                                 //
                                                                       //
    return TableRow;                                                   // 4550
  })(ContentEdit.ElementCollection);                                   //
                                                                       //
  ContentEdit.TableCell = (function (_super) {                         // 4554
    __extends(TableCell, _super);                                      // 4555
                                                                       //
    function TableCell(tagName, attributes) {                          // 4557
      TableCell.__super__.constructor.call(this, tagName, attributes);
    }                                                                  //
                                                                       //
    TableCell.prototype.cssTypeName = function () {                    // 4561
      return 'table-cell';                                             // 4562
    };                                                                 //
                                                                       //
    TableCell.prototype.tableCellText = function () {                  // 4565
      if (this.children.length > 0) {                                  // 4566
        return this.children[0];                                       // 4567
      }                                                                //
      return null;                                                     // 4569
    };                                                                 //
                                                                       //
    TableCell.prototype.html = function (indent) {                     // 4572
      var lines;                                                       // 4573
      if (indent == null) {                                            // 4574
        indent = '';                                                   // 4575
      }                                                                //
      lines = ["" + indent + "<" + this.tagName() + this._attributesToString() + ">"];
      if (this.tableCellText()) {                                      // 4578
        lines.push(this.tableCellText().html(indent + ContentEdit.INDENT));
      }                                                                //
      lines.push("" + indent + "</" + this.tagName() + ">");           // 4581
      return lines.join('\n');                                         // 4582
    };                                                                 //
                                                                       //
    TableCell.prototype._onMouseOver = function (ev) {                 // 4585
      TableCell.__super__._onMouseOver.call(this, ev);                 // 4586
      return this._removeCSSClass('ce-element--over');                 // 4587
    };                                                                 //
                                                                       //
    TableCell.prototype._addDOMEventListeners = function () {};        // 4590
                                                                       //
    TableCell.prototype._removeDOMEventListners = function () {};      // 4592
                                                                       //
    TableCell.fromDOMElement = function (domElement) {                 // 4594
      var tableCell, tableCellText;                                    // 4595
      tableCell = new this(domElement.tagName, this.getDOMElementAttributes(domElement));
      tableCellText = new ContentEdit.TableCellText(domElement.innerHTML.replace(/^\s+|\s+$/g, ''));
      tableCell.attach(tableCellText);                                 // 4598
      return tableCell;                                                // 4599
    };                                                                 //
                                                                       //
    return TableCell;                                                  // 4602
  })(ContentEdit.ElementCollection);                                   //
                                                                       //
  ContentEdit.TableCellText = (function (_super) {                     // 4606
    __extends(TableCellText, _super);                                  // 4607
                                                                       //
    function TableCellText(content) {                                  // 4609
      TableCellText.__super__.constructor.call(this, 'div', {}, content);
    }                                                                  //
                                                                       //
    TableCellText.prototype.cssTypeName = function () {                // 4613
      return 'table-cell-text';                                        // 4614
    };                                                                 //
                                                                       //
    TableCellText.prototype._isInFirstRow = function () {              // 4617
      var cell, row, section, table;                                   // 4618
      cell = this.parent();                                            // 4619
      row = cell.parent();                                             // 4620
      section = row.parent();                                          // 4621
      table = section.parent();                                        // 4622
      if (section !== table.firstSection()) {                          // 4623
        return false;                                                  // 4624
      }                                                                //
      return row === section.children[0];                              // 4626
    };                                                                 //
                                                                       //
    TableCellText.prototype._isInLastRow = function () {               // 4629
      var cell, row, section, table;                                   // 4630
      cell = this.parent();                                            // 4631
      row = cell.parent();                                             // 4632
      section = row.parent();                                          // 4633
      table = section.parent();                                        // 4634
      if (section !== table.lastSection()) {                           // 4635
        return false;                                                  // 4636
      }                                                                //
      return row === section.children[section.children.length - 1];    // 4638
    };                                                                 //
                                                                       //
    TableCellText.prototype._isLastInSection = function () {           // 4641
      var cell, row, section;                                          // 4642
      cell = this.parent();                                            // 4643
      row = cell.parent();                                             // 4644
      section = row.parent();                                          // 4645
      if (row !== section.children[section.children.length - 1]) {     // 4646
        return false;                                                  // 4647
      }                                                                //
      return cell === row.children[row.children.length - 1];           // 4649
    };                                                                 //
                                                                       //
    TableCellText.prototype.blur = function () {                       // 4652
      if (this.isMounted()) {                                          // 4653
        this._domElement.blur();                                       // 4654
        this._domElement.removeAttribute('contenteditable');           // 4655
      }                                                                //
      return ContentEdit.Element.prototype.blur.call(this);            // 4657
    };                                                                 //
                                                                       //
    TableCellText.prototype.html = function (indent) {                 // 4660
      var content;                                                     // 4661
      if (indent == null) {                                            // 4662
        indent = '';                                                   // 4663
      }                                                                //
      if (!this._lastCached || this._lastCached < this._modified) {    // 4665
        content = this.content.copy();                                 // 4666
        content.optimize();                                            // 4667
        this._lastCached = Date.now();                                 // 4668
        this._cached = content.html();                                 // 4669
      }                                                                //
      return "" + indent + this._cached;                               // 4671
    };                                                                 //
                                                                       //
    TableCellText.prototype._onMouseDown = function (ev) {             // 4674
      var initDrag;                                                    // 4675
      ContentEdit.Element.prototype._onMouseDown.call(this, ev);       // 4676
      initDrag = (function (_this) {                                   // 4677
        return function () {                                           // 4678
          var cell, table;                                             // 4679
          cell = _this.parent();                                       // 4680
          if (ContentEdit.Root.get().dragging() === cell.parent()) {   // 4681
            ContentEdit.Root.get().cancelDragging();                   // 4682
            table = cell.parent().parent().parent();                   // 4683
            return table.drag(ev.pageX, ev.pageY);                     // 4684
          } else {                                                     //
            cell.parent().drag(ev.pageX, ev.pageY);                    // 4686
            return _this._dragTimeout = setTimeout(initDrag, ContentEdit.DRAG_HOLD_DURATION * 2);
          }                                                            //
        };                                                             //
      })(this);                                                        //
      clearTimeout(this._dragTimeout);                                 // 4691
      return this._dragTimeout = setTimeout(initDrag, ContentEdit.DRAG_HOLD_DURATION);
    };                                                                 //
                                                                       //
    TableCellText.prototype._keyReturn = function (ev) {               // 4695
      ev.preventDefault();                                             // 4696
      return this._keyTab({                                            // 4697
        'shiftKey': false,                                             // 4698
        'preventDefault': function () {}                               // 4699
      });                                                              //
    };                                                                 //
                                                                       //
    TableCellText.prototype._keyDown = function (ev) {                 // 4703
      var cell, cellIndex, lastCell, nextRow, row;                     // 4704
      ev.preventDefault();                                             // 4705
      cell = this.parent();                                            // 4706
      if (this._isInLastRow()) {                                       // 4707
        row = cell.parent();                                           // 4708
        lastCell = row.children[row.children.length - 1].tableCellText();
        return lastCell.nextContent().focus();                         // 4710
      } else {                                                         //
        nextRow = cell.parent().nextWithTest(function (node) {         // 4712
          return node.constructor.name === 'TableRow';                 // 4713
        });                                                            //
        cellIndex = cell.parent().children.indexOf(cell);              // 4715
        cellIndex = Math.min(cellIndex, nextRow.children.length);      // 4716
        return nextRow.children[cellIndex].tableCellText().focus();    // 4717
      }                                                                //
    };                                                                 //
                                                                       //
    TableCellText.prototype._keyTab = function (ev) {                  // 4721
      var cell, child, grandParent, newCell, newCellText, row, section, _i, _len, _ref;
      ev.preventDefault();                                             // 4723
      cell = this.parent();                                            // 4724
      if (ev.shiftKey) {                                               // 4725
        if (this._isInFirstRow() && cell.parent().children[0] === cell) {
          return;                                                      // 4727
        }                                                              //
        return this.previousContent().focus();                         // 4729
      } else {                                                         //
        grandParent = cell.parent().parent();                          // 4731
        if (grandParent.tagName() === 'tbody' && this._isLastInSection()) {
          row = new ContentEdit.TableRow();                            // 4733
          _ref = cell.parent().children;                               // 4734
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {          // 4735
            child = _ref[_i];                                          // 4736
            newCell = new ContentEdit.TableCell(child.tagName(), child._attributes);
            newCellText = new ContentEdit.TableCellText('');           // 4738
            newCell.attach(newCellText);                               // 4739
            row.attach(newCell);                                       // 4740
          }                                                            //
          section = this.closest(function (node) {                     // 4742
            return node.constructor.name === 'TableSection';           // 4743
          });                                                          //
          section.attach(row);                                         // 4745
          return row.children[0].tableCellText().focus();              // 4746
        } else {                                                       //
          return this.nextContent().focus();                           // 4748
        }                                                              //
      }                                                                //
    };                                                                 //
                                                                       //
    TableCellText.prototype._keyUp = function (ev) {                   // 4753
      var cell, cellIndex, previousRow, row;                           // 4754
      ev.preventDefault();                                             // 4755
      cell = this.parent();                                            // 4756
      if (this._isInFirstRow()) {                                      // 4757
        row = cell.parent();                                           // 4758
        return row.children[0].previousContent().focus();              // 4759
      } else {                                                         //
        previousRow = cell.parent().previousWithTest(function (node) {
          return node.constructor.name === 'TableRow';                 // 4762
        });                                                            //
        cellIndex = cell.parent().children.indexOf(cell);              // 4764
        cellIndex = Math.min(cellIndex, previousRow.children.length);  // 4765
        return previousRow.children[cellIndex].tableCellText().focus();
      }                                                                //
    };                                                                 //
                                                                       //
    TableCellText.droppers = {};                                       // 4770
                                                                       //
    TableCellText.mergers = {};                                        // 4772
                                                                       //
    return TableCellText;                                              // 4774
  })(ContentEdit.Text);                                                //
}).call(this);                                                         //
                                                                       //
(function () {                                                         // 4780
  var AttributeUI,                                                     // 4781
      CropMarksUI,                                                     //
      StyleUI,                                                         //
      _EditorApp,                                                      //
      __slice = [].slice,                                              //
      __hasProp = ({}).hasOwnProperty,                                 //
      __extends = function (child, parent) {                           //
    for (var key in babelHelpers.sanitizeForInObject(parent)) {        // 4784
      if (__hasProp.call(parent, key)) child[key] = parent[key];       // 4784
    }function ctor() {                                                 //
      this.constructor = child;                                        // 4784
    }ctor.prototype = parent.prototype;child.prototype = new ctor();child.__super__ = parent.prototype;return child;
  },                                                                   //
      __bind = function (fn, me) {                                     //
    return function () {                                               // 4785
      return fn.apply(me, arguments);                                  // 4785
    };                                                                 //
  };                                                                   //
                                                                       //
  window.ContentTools = {                                              // 4787
    Tools: {},                                                         // 4788
    DEFAULT_TOOLS: [['bold', 'italic', 'link', 'align-left', 'align-center', 'align-right'], ['heading', 'subheading', 'paragraph', 'unordered-list', 'ordered-list', 'table', 'indent', 'unindent', 'line-break'], ['image', 'video', 'preformatted'], ['undo', 'redo', 'remove']],
    DEFAULT_VIDEO_HEIGHT: 300,                                         // 4790
    DEFAULT_VIDEO_WIDTH: 400,                                          // 4791
    HIGHLIGHT_HOLD_DURATION: 2000,                                     // 4792
    INSPECTOR_IGNORED_ELEMENTS: ['ListItemText', 'Region', 'TableCellText'],
    IMAGE_UPLOADER: null,                                              // 4794
    MIN_CROP: 10,                                                      // 4795
    RESTRICTED_ATTRIBUTES: {                                           // 4796
      'img': ['height', 'src', 'width', 'data-ce-max-width', 'data-ce-min-width'],
      'iframe': ['height', 'width']                                    // 4798
    },                                                                 //
    getEmbedVideoURL: function (url) {                                 // 4800
      var domains, id, kv, m, netloc, params, paramsStr, parser, path, _i, _len, _ref;
      domains = {                                                      // 4802
        'www.youtube.com': 'youtube',                                  // 4803
        'youtu.be': 'youtube',                                         // 4804
        'vimeo.com': 'vimeo',                                          // 4805
        'player.vimeo.com': 'vimeo'                                    // 4806
      };                                                               //
      parser = document.createElement('a');                            // 4808
      parser.href = url;                                               // 4809
      netloc = parser.hostname.toLowerCase();                          // 4810
      path = parser.pathname;                                          // 4811
      if (path !== null && path.substr(0, 1) !== "/") {                // 4812
        path = "/" + path;                                             // 4813
      }                                                                //
      params = {};                                                     // 4815
      paramsStr = parser.search.slice(1);                              // 4816
      _ref = paramsStr.split('&');                                     // 4817
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 4818
        kv = _ref[_i];                                                 // 4819
        kv = kv.split("=");                                            // 4820
        params[kv[0]] = kv[1];                                         // 4821
      }                                                                //
      switch (domains[netloc]) {                                       // 4823
        case 'youtube':                                                // 4824
          if (path.toLowerCase() === '/watch') {                       // 4825
            if (!params['v']) {                                        // 4826
              return null;                                             // 4827
            }                                                          //
            id = params['v'];                                          // 4829
          } else {                                                     //
            m = path.match(/\/([A-Za-z0-9_-]+)$/i);                    // 4831
            if (!m) {                                                  // 4832
              return null;                                             // 4833
            }                                                          //
            id = m[1];                                                 // 4835
          }                                                            //
          return "https://www.youtube.com/embed/" + id;                // 4837
        case 'vimeo':                                                  // 4838
          m = path.match(/\/(\w+\/\w+\/){0,1}(\d+)/i);                 // 4839
          if (!m) {                                                    // 4840
            return null;                                               // 4841
          }                                                            //
          return "https://player.vimeo.com/video/" + m[2];             // 4843
      }                                                                // 4843
      return null;                                                     // 4845
    }                                                                  //
  };                                                                   //
                                                                       //
  ContentTools.ComponentUI = (function () {                            // 4849
    function ComponentUI() {                                           // 4850
      this._bindings = {};                                             // 4851
      this._parent = null;                                             // 4852
      this._children = [];                                             // 4853
      this._domElement = null;                                         // 4854
    }                                                                  //
                                                                       //
    ComponentUI.prototype.children = function () {                     // 4857
      return this._children.slice();                                   // 4858
    };                                                                 //
                                                                       //
    ComponentUI.prototype.domElement = function () {                   // 4861
      return this._domElement;                                         // 4862
    };                                                                 //
                                                                       //
    ComponentUI.prototype.isMounted = function () {                    // 4865
      return this._domElement !== null;                                // 4866
    };                                                                 //
                                                                       //
    ComponentUI.prototype.parent = function () {                       // 4869
      return this._parent;                                             // 4870
    };                                                                 //
                                                                       //
    ComponentUI.prototype.attach = function (component, index) {       // 4873
      if (component.parent()) {                                        // 4874
        component.parent().detach(component);                          // 4875
      }                                                                //
      component._parent = this;                                        // 4877
      if (index !== void 0) {                                          // 4878
        return this._children.splice(index, 0, component);             // 4879
      } else {                                                         //
        return this._children.push(component);                         // 4881
      }                                                                //
    };                                                                 //
                                                                       //
    ComponentUI.prototype.addCSSClass = function (className) {         // 4885
      if (!this.isMounted()) {                                         // 4886
        return;                                                        // 4887
      }                                                                //
      return ContentEdit.addCSSClass(this._domElement, className);     // 4889
    };                                                                 //
                                                                       //
    ComponentUI.prototype.detatch = function (component) {             // 4892
      var componentIndex;                                              // 4893
      componentIndex = this._children.indexOf(component);              // 4894
      if (componentIndex === -1) {                                     // 4895
        return;                                                        // 4896
      }                                                                //
      return this._children.splice(componentIndex, 1);                 // 4898
    };                                                                 //
                                                                       //
    ComponentUI.prototype.mount = function () {};                      // 4901
                                                                       //
    ComponentUI.prototype.removeCSSClass = function (className) {      // 4903
      if (!this.isMounted()) {                                         // 4904
        return;                                                        // 4905
      }                                                                //
      return ContentEdit.removeCSSClass(this._domElement, className);  // 4907
    };                                                                 //
                                                                       //
    ComponentUI.prototype.unmount = function () {                      // 4910
      if (!this.isMounted()) {                                         // 4911
        return;                                                        // 4912
      }                                                                //
      this._removeDOMEventListeners();                                 // 4914
      this._domElement.parentNode.removeChild(this._domElement);       // 4915
      return this._domElement = null;                                  // 4916
    };                                                                 //
                                                                       //
    ComponentUI.prototype.bind = function (eventName, callback) {      // 4919
      if (this._bindings[eventName] === void 0) {                      // 4920
        this._bindings[eventName] = [];                                // 4921
      }                                                                //
      this._bindings[eventName].push(callback);                        // 4923
      return callback;                                                 // 4924
    };                                                                 //
                                                                       //
    ComponentUI.prototype.trigger = function () {                      // 4927
      var args, callback, eventName, _i, _len, _ref, _results;         // 4928
      eventName = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      if (!this._bindings[eventName]) {                                // 4930
        return;                                                        // 4931
      }                                                                //
      _ref = this._bindings[eventName];                                // 4933
      _results = [];                                                   // 4934
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 4935
        callback = _ref[_i];                                           // 4936
        if (!callback) {                                               // 4937
          continue;                                                    // 4938
        }                                                              //
        _results.push(callback.call.apply(callback, [this].concat(__slice.call(args))));
      }                                                                //
      return _results;                                                 // 4942
    };                                                                 //
                                                                       //
    ComponentUI.prototype.unbind = function (eventName, callback) {    // 4945
      var i, suspect, _i, _len, _ref, _results;                        // 4946
      if (!eventName) {                                                // 4947
        this._bindings = {};                                           // 4948
        return;                                                        // 4949
      }                                                                //
      if (!callback) {                                                 // 4951
        this._bindings[eventName] = void 0;                            // 4952
        return;                                                        // 4953
      }                                                                //
      if (!this._bindings[eventName]) {                                // 4955
        return;                                                        // 4956
      }                                                                //
      _ref = this._bindings[eventName];                                // 4958
      _results = [];                                                   // 4959
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {      // 4960
        suspect = _ref[i];                                             // 4961
        if (suspect === callback) {                                    // 4962
          _results.push(this._bindings[eventName].splice(i, 1));       // 4963
        } else {                                                       //
          _results.push(void 0);                                       // 4965
        }                                                              //
      }                                                                //
      return _results;                                                 // 4968
    };                                                                 //
                                                                       //
    ComponentUI.prototype._addDOMEventListeners = function () {};      // 4971
                                                                       //
    ComponentUI.prototype._removeDOMEventListeners = function () {};   // 4973
                                                                       //
    ComponentUI.createDiv = function (classNames, attributes, content) {
      var domElement, name, value;                                     // 4976
      domElement = document.createElement('div');                      // 4977
      if (classNames && classNames.length > 0) {                       // 4978
        domElement.setAttribute('class', classNames.join(' '));        // 4979
      }                                                                //
      if (attributes) {                                                // 4981
        for (name in babelHelpers.sanitizeForInObject(attributes)) {   // 4982
          value = attributes[name];                                    // 4983
          domElement.setAttribute(name, value);                        // 4984
        }                                                              //
      }                                                                //
      if (content) {                                                   // 4987
        domElement.innerHTML = content;                                // 4988
      }                                                                //
      return domElement;                                               // 4990
    };                                                                 //
                                                                       //
    return ComponentUI;                                                // 4993
  })();                                                                //
                                                                       //
  ContentTools.WidgetUI = (function (_super) {                         // 4997
    __extends(WidgetUI, _super);                                       // 4998
                                                                       //
    function WidgetUI() {                                              // 5000
      return WidgetUI.__super__.constructor.apply(this, arguments);    // 5001
    }                                                                  //
                                                                       //
    WidgetUI.prototype.attach = function (component, index) {          // 5004
      WidgetUI.__super__.attach.call(this, component, index);          // 5005
      if (!this.isMounted()) {                                         // 5006
        return component.mount();                                      // 5007
      }                                                                //
    };                                                                 //
                                                                       //
    WidgetUI.prototype.detatch = function (component) {                // 5011
      WidgetUI.__super__.detatch.call(this, component);                // 5012
      if (this.isMounted()) {                                          // 5013
        return component.unmount();                                    // 5014
      }                                                                //
    };                                                                 //
                                                                       //
    WidgetUI.prototype.show = function () {                            // 5018
      var fadeIn;                                                      // 5019
      if (!this.isMounted()) {                                         // 5020
        this.mount();                                                  // 5021
      }                                                                //
      fadeIn = (function (_this) {                                     // 5023
        return function () {                                           // 5024
          return _this.addCSSClass('ct-widget--active');               // 5025
        };                                                             //
      })(this);                                                        //
      return setTimeout(fadeIn, 100);                                  // 5028
    };                                                                 //
                                                                       //
    WidgetUI.prototype.hide = function () {                            // 5031
      var monitorForHidden;                                            // 5032
      this.removeCSSClass('ct-widget--active');                        // 5033
      monitorForHidden = (function (_this) {                           // 5034
        return function () {                                           // 5035
          if (!window.getComputedStyle) {                              // 5036
            _this.unmount();                                           // 5037
            return;                                                    // 5038
          }                                                            //
          if (parseFloat(window.getComputedStyle(_this._domElement).opacity) < 0.01) {
            return _this.unmount();                                    // 5041
          } else {                                                     //
            return setTimeout(monitorForHidden, 250);                  // 5043
          }                                                            //
        };                                                             //
      })(this);                                                        //
      if (this.isMounted()) {                                          // 5047
        return setTimeout(monitorForHidden, 250);                      // 5048
      }                                                                //
    };                                                                 //
                                                                       //
    return WidgetUI;                                                   // 5052
  })(ContentTools.ComponentUI);                                        //
                                                                       //
  ContentTools.AnchoredComponentUI = (function (_super) {              // 5056
    __extends(AnchoredComponentUI, _super);                            // 5057
                                                                       //
    function AnchoredComponentUI() {                                   // 5059
      return AnchoredComponentUI.__super__.constructor.apply(this, arguments);
    }                                                                  //
                                                                       //
    AnchoredComponentUI.prototype.mount = function (domParent, before) {
      if (before == null) {                                            // 5064
        before = null;                                                 // 5065
      }                                                                //
      domParent.insertBefore(this._domElement, before);                // 5067
      return this._addDOMEventListeners();                             // 5068
    };                                                                 //
                                                                       //
    return AnchoredComponentUI;                                        // 5071
  })(ContentTools.ComponentUI);                                        //
                                                                       //
  ContentTools.FlashUI = (function (_super) {                          // 5075
    __extends(FlashUI, _super);                                        // 5076
                                                                       //
    function FlashUI(modifier) {                                       // 5078
      FlashUI.__super__.constructor.call(this);                        // 5079
      this.mount(modifier);                                            // 5080
    }                                                                  //
                                                                       //
    FlashUI.prototype.mount = function (modifier) {                    // 5083
      var monitorForHidden;                                            // 5084
      this._domElement = this.constructor.createDiv(['ct-flash', 'ct-flash--active', "ct-flash--" + modifier, 'ct-widget', 'ct-widget--active']);
      FlashUI.__super__.mount.call(this, ContentTools.EditorApp.get().domElement());
      monitorForHidden = (function (_this) {                           // 5087
        return function () {                                           // 5088
          if (!window.getComputedStyle) {                              // 5089
            _this.unmount();                                           // 5090
            return;                                                    // 5091
          }                                                            //
          if (parseFloat(window.getComputedStyle(_this._domElement).opacity) < 0.01) {
            return _this.unmount();                                    // 5094
          } else {                                                     //
            return setTimeout(monitorForHidden, 250);                  // 5096
          }                                                            //
        };                                                             //
      })(this);                                                        //
      return setTimeout(monitorForHidden, 250);                        // 5100
    };                                                                 //
                                                                       //
    return FlashUI;                                                    // 5103
  })(ContentTools.AnchoredComponentUI);                                //
                                                                       //
  ContentTools.IgnitionUI = (function (_super) {                       // 5107
    __extends(IgnitionUI, _super);                                     // 5108
                                                                       //
    function IgnitionUI() {                                            // 5110
      IgnitionUI.__super__.constructor.call(this);                     // 5111
      this._busy = false;                                              // 5112
    }                                                                  //
                                                                       //
    IgnitionUI.prototype.busy = function (busy) {                      // 5115
      if (busy === void 0) {                                           // 5116
        return this._busy;                                             // 5117
      }                                                                //
      if (this._busy === busy) {                                       // 5119
        return;                                                        // 5120
      }                                                                //
      this._busy = busy;                                               // 5122
      if (busy) {                                                      // 5123
        return this.addCSSClass('ct-ignition--busy');                  // 5124
      } else {                                                         //
        return this.removeCSSClass('ct-ignition--busy');               // 5126
      }                                                                //
    };                                                                 //
                                                                       //
    IgnitionUI.prototype.changeState = function (state) {              // 5130
      if (state === 'editing') {                                       // 5131
        this.addCSSClass('ct-ignition--editing');                      // 5132
        return this.removeCSSClass('ct-ignition--ready');              // 5133
      } else if (state === 'ready') {                                  //
        this.removeCSSClass('ct-ignition--editing');                   // 5135
        return this.addCSSClass('ct-ignition--ready');                 // 5136
      }                                                                //
    };                                                                 //
                                                                       //
    IgnitionUI.prototype.mount = function () {                         // 5140
      IgnitionUI.__super__.mount.call(this);                           // 5141
      this._domElement = this.constructor.createDiv(['ct-widget', 'ct-ignition', 'ct-ignition--ready']);
      this.parent().domElement().appendChild(this._domElement);        // 5143
      this._domEdit = this.constructor.createDiv(['ct-ignition__button', 'ct-ignition__button--edit']);
      this._domElement.appendChild(this._domEdit);                     // 5145
      this._domConfirm = this.constructor.createDiv(['ct-ignition__button', 'ct-ignition__button--confirm']);
      this._domElement.appendChild(this._domConfirm);                  // 5147
      this._domCancel = this.constructor.createDiv(['ct-ignition__button', 'ct-ignition__button--cancel']);
      this._domElement.appendChild(this._domCancel);                   // 5149
      this._domBusy = this.constructor.createDiv(['ct-ignition__button', 'ct-ignition__button--busy']);
      this._domElement.appendChild(this._domBusy);                     // 5151
      return this._addDOMEventListeners();                             // 5152
    };                                                                 //
                                                                       //
    IgnitionUI.prototype.unmount = function () {                       // 5155
      IgnitionUI.__super__.unmount.call(this);                         // 5156
      this._domEdit = null;                                            // 5157
      this._domConfirm = null;                                         // 5158
      return this._domCancel = null;                                   // 5159
    };                                                                 //
                                                                       //
    IgnitionUI.prototype._addDOMEventListeners = function () {         // 5162
      this._domEdit.addEventListener('click', (function (_this) {      // 5163
        return function (ev) {                                         // 5164
          ev.preventDefault();                                         // 5165
          _this.addCSSClass('ct-ignition--editing');                   // 5166
          _this.removeCSSClass('ct-ignition--ready');                  // 5167
          return _this.trigger('start');                               // 5168
        };                                                             //
      })(this));                                                       //
      this._domConfirm.addEventListener('click', (function (_this) {   // 5171
        return function (ev) {                                         // 5172
          ev.preventDefault();                                         // 5173
          _this.removeCSSClass('ct-ignition--editing');                // 5174
          _this.addCSSClass('ct-ignition--ready');                     // 5175
          return _this.trigger('stop', true);                          // 5176
        };                                                             //
      })(this));                                                       //
      return this._domCancel.addEventListener('click', (function (_this) {
        return function (ev) {                                         // 5180
          ev.preventDefault();                                         // 5181
          _this.removeCSSClass('ct-ignition--editing');                // 5182
          _this.addCSSClass('ct-ignition--ready');                     // 5183
          return _this.trigger('stop', false);                         // 5184
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    return IgnitionUI;                                                 // 5189
  })(ContentTools.WidgetUI);                                           //
                                                                       //
  ContentTools.InspectorUI = (function (_super) {                      // 5193
    __extends(InspectorUI, _super);                                    // 5194
                                                                       //
    function InspectorUI() {                                           // 5196
      InspectorUI.__super__.constructor.call(this);                    // 5197
      this._tagUIs = [];                                               // 5198
    }                                                                  //
                                                                       //
    InspectorUI.prototype.mount = function () {                        // 5201
      this._domElement = this.constructor.createDiv(['ct-widget', 'ct-inspector']);
      this.parent().domElement().appendChild(this._domElement);        // 5203
      this._domTags = this.constructor.createDiv(['ct-inspector__tags', 'ct-tags']);
      this._domElement.appendChild(this._domTags);                     // 5205
      this._addDOMEventListeners();                                    // 5206
      this._handleFocusChange = (function (_this) {                    // 5207
        return function () {                                           // 5208
          return _this.updateTags();                                   // 5209
        };                                                             //
      })(this);                                                        //
      ContentEdit.Root.get().bind('blur', this._handleFocusChange);    // 5212
      ContentEdit.Root.get().bind('focus', this._handleFocusChange);   // 5213
      return ContentEdit.Root.get().bind('mount', this._handleFocusChange);
    };                                                                 //
                                                                       //
    InspectorUI.prototype.unmount = function () {                      // 5217
      InspectorUI.__super__.unmount.call(this);                        // 5218
      this._domTags = null;                                            // 5219
      ContentEdit.Root.get().unbind('blur', this._handleFocusChange);  // 5220
      ContentEdit.Root.get().unbind('focus', this._handleFocusChange);
      return ContentEdit.Root.get().unbind('mount', this._handleFocusChange);
    };                                                                 //
                                                                       //
    InspectorUI.prototype.updateTags = function () {                   // 5225
      var element, elements, tag, _i, _j, _len, _len1, _ref, _results;
      element = ContentEdit.Root.get().focused();                      // 5227
      _ref = this._tagUIs;                                             // 5228
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 5229
        tag = _ref[_i];                                                // 5230
        tag.unmount();                                                 // 5231
      }                                                                //
      this._tagUIs = [];                                               // 5233
      if (!element) {                                                  // 5234
        return;                                                        // 5235
      }                                                                //
      elements = element.parents();                                    // 5237
      elements.reverse();                                              // 5238
      elements.push(element);                                          // 5239
      _results = [];                                                   // 5240
      for (_j = 0, _len1 = elements.length; _j < _len1; _j++) {        // 5241
        element = elements[_j];                                        // 5242
        if (ContentTools.INSPECTOR_IGNORED_ELEMENTS.indexOf(element.constructor.name) !== -1) {
          continue;                                                    // 5244
        }                                                              //
        tag = new ContentTools.TagUI(element);                         // 5246
        this._tagUIs.push(tag);                                        // 5247
        _results.push(tag.mount(this._domTags));                       // 5248
      }                                                                //
      return _results;                                                 // 5250
    };                                                                 //
                                                                       //
    return InspectorUI;                                                // 5253
  })(ContentTools.WidgetUI);                                           //
                                                                       //
  ContentTools.TagUI = (function (_super) {                            // 5257
    __extends(TagUI, _super);                                          // 5258
                                                                       //
    function TagUI(element) {                                          // 5260
      this.element = element;                                          // 5261
      this._onMouseDown = __bind(this._onMouseDown, this);             // 5262
      TagUI.__super__.constructor.call(this);                          // 5263
    }                                                                  //
                                                                       //
    TagUI.prototype.mount = function (domParent, before) {             // 5266
      if (before == null) {                                            // 5267
        before = null;                                                 // 5268
      }                                                                //
      this._domElement = this.constructor.createDiv(['ct-tag']);       // 5270
      this._domElement.textContent = this.element.tagName();           // 5271
      return TagUI.__super__.mount.call(this, domParent, before);      // 5272
    };                                                                 //
                                                                       //
    TagUI.prototype._addDOMEventListeners = function () {              // 5275
      return this._domElement.addEventListener('mousedown', this._onMouseDown);
    };                                                                 //
                                                                       //
    TagUI.prototype._onMouseDown = function (ev) {                     // 5279
      var app, dialog, modal;                                          // 5280
      ev.preventDefault();                                             // 5281
      if (this.element.storeState) {                                   // 5282
        this.element.storeState();                                     // 5283
      }                                                                //
      app = ContentTools.EditorApp.get();                              // 5285
      modal = new ContentTools.ModalUI();                              // 5286
      dialog = new ContentTools.PropertiesDialog(this.element);        // 5287
      dialog.bind('cancel', (function (_this) {                        // 5288
        return function () {                                           // 5289
          dialog.unbind('cancel');                                     // 5290
          modal.hide();                                                // 5291
          dialog.hide();                                               // 5292
          if (_this.element.restoreState) {                            // 5293
            return _this.element.restoreState();                       // 5294
          }                                                            //
        };                                                             //
      })(this));                                                       //
      dialog.bind('save', (function (_this) {                          // 5298
        return function (attributes, styles, innerHTML) {              // 5299
          var applied, className, classNames, cssClass, element, name, value, _i, _j, _len, _len1, _ref, _ref1;
          dialog.unbind('save');                                       // 5301
          for (name in babelHelpers.sanitizeForInObject(attributes)) {
            value = attributes[name];                                  // 5303
            if (name === 'class') {                                    // 5304
              if (value === null) {                                    // 5305
                value = '';                                            // 5306
              }                                                        //
              classNames = {};                                         // 5308
              _ref = value.split(' ');                                 // 5309
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {      // 5310
                className = _ref[_i];                                  // 5311
                className = className.trim();                          // 5312
                if (!className) {                                      // 5313
                  continue;                                            // 5314
                }                                                      //
                classNames[className] = true;                          // 5316
                if (!_this.element.hasCSSClass(className)) {           // 5317
                  _this.element.addCSSClass(className);                // 5318
                }                                                      //
              }                                                        //
              _ref1 = _this.element.attr('class').split(' ');          // 5321
              for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {   // 5322
                className = _ref1[_j];                                 // 5323
                className = className.trim();                          // 5324
                if (classNames[className] === void 0) {                // 5325
                  _this.element.removeCSSClass(className);             // 5326
                }                                                      //
              }                                                        //
            } else {                                                   //
              if (value === null) {                                    // 5330
                _this.element.removeAttr(name);                        // 5331
              } else {                                                 //
                _this.element.attr(name, value);                       // 5333
              }                                                        //
            }                                                          //
          }                                                            //
          for (cssClass in babelHelpers.sanitizeForInObject(styles)) {
            applied = styles[cssClass];                                // 5338
            if (applied) {                                             // 5339
              _this.element.addCSSClass(cssClass);                     // 5340
            } else {                                                   //
              _this.element.removeCSSClass(cssClass);                  // 5342
            }                                                          //
          }                                                            //
          if (innerHTML !== null) {                                    // 5345
            if (innerHTML !== dialog.getElementInnerHTML()) {          // 5346
              element = _this.element;                                 // 5347
              if (!element.content) {                                  // 5348
                element = element.children[0];                         // 5349
              }                                                        //
              element.content = new HTMLString.String(innerHTML, element.content.preserveWhitespace());
              element.updateInnerHTML();                               // 5352
              element.taint();                                         // 5353
              element.selection(new ContentSelect.Range(0, 0));        // 5354
              element.storeState();                                    // 5355
            }                                                          //
          }                                                            //
          modal.hide();                                                // 5358
          dialog.hide();                                               // 5359
          if (_this.element.restoreState) {                            // 5360
            return _this.element.restoreState();                       // 5361
          }                                                            //
        };                                                             //
      })(this));                                                       //
      app.attach(modal);                                               // 5365
      app.attach(dialog);                                              // 5366
      modal.show();                                                    // 5367
      return dialog.show();                                            // 5368
    };                                                                 //
                                                                       //
    return TagUI;                                                      // 5371
  })(ContentTools.AnchoredComponentUI);                                //
                                                                       //
  ContentTools.ModalUI = (function (_super) {                          // 5375
    __extends(ModalUI, _super);                                        // 5376
                                                                       //
    function ModalUI(transparent, allowScrolling) {                    // 5378
      ModalUI.__super__.constructor.call(this);                        // 5379
      this._transparent = transparent;                                 // 5380
      this._allowScrolling = allowScrolling;                           // 5381
    }                                                                  //
                                                                       //
    ModalUI.prototype.mount = function () {                            // 5384
      this._domElement = this.constructor.createDiv(['ct-widget', 'ct-modal']);
      this.parent().domElement().appendChild(this._domElement);        // 5386
      if (this._transparent) {                                         // 5387
        this.addCSSClass('ct-modal--transparent');                     // 5388
      }                                                                //
      if (!this._allowScrolling) {                                     // 5390
        ContentEdit.addCSSClass(document.body, 'ct--no-scroll');       // 5391
      }                                                                //
      return this._addDOMEventListeners();                             // 5393
    };                                                                 //
                                                                       //
    ModalUI.prototype.unmount = function () {                          // 5396
      if (!this._allowScrolling) {                                     // 5397
        ContentEdit.removeCSSClass(document.body, 'ct--no-scroll');    // 5398
      }                                                                //
      return ModalUI.__super__.unmount.call(this);                     // 5400
    };                                                                 //
                                                                       //
    ModalUI.prototype._addDOMEventListeners = function () {            // 5403
      return this._domElement.addEventListener('click', (function (_this) {
        return function (ev) {                                         // 5405
          return _this.trigger('click');                               // 5406
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    return ModalUI;                                                    // 5411
  })(ContentTools.WidgetUI);                                           //
                                                                       //
  ContentTools.ToolboxUI = (function (_super) {                        // 5415
    __extends(ToolboxUI, _super);                                      // 5416
                                                                       //
    function ToolboxUI(tools) {                                        // 5418
      this._onStopDragging = __bind(this._onStopDragging, this);       // 5419
      this._onStartDragging = __bind(this._onStartDragging, this);     // 5420
      this._onDrag = __bind(this._onDrag, this);                       // 5421
      ToolboxUI.__super__.constructor.call(this);                      // 5422
      this._tools = tools;                                             // 5423
      this._dragging = false;                                          // 5424
      this._draggingOffset = null;                                     // 5425
      this._domGrip = null;                                            // 5426
      this._toolUIs = {};                                              // 5427
    }                                                                  //
                                                                       //
    ToolboxUI.prototype.isDragging = function () {                     // 5430
      return this._dragging;                                           // 5431
    };                                                                 //
                                                                       //
    ToolboxUI.prototype.hide = function () {                           // 5434
      this._removeDOMEventListeners();                                 // 5435
      return ToolboxUI.__super__.hide.call(this);                      // 5436
    };                                                                 //
                                                                       //
    ToolboxUI.prototype.tools = function (tools) {                     // 5439
      if (tools === void 0) {                                          // 5440
        return this._tools;                                            // 5441
      }                                                                //
      this._tools = tools;                                             // 5443
      this.unmount();                                                  // 5444
      return this.mount();                                             // 5445
    };                                                                 //
                                                                       //
    ToolboxUI.prototype.mount = function () {                          // 5448
      var coord, domToolGroup, i, position, restore, tool, toolGroup, toolName, _i, _j, _len, _len1, _ref;
      this._domElement = this.constructor.createDiv(['ct-widget', 'ct-toolbox']);
      this.parent().domElement().appendChild(this._domElement);        // 5451
      this._domGrip = this.constructor.createDiv(['ct-toolbox__grip', 'ct-grip']);
      this._domElement.appendChild(this._domGrip);                     // 5453
      this._domGrip.appendChild(this.constructor.createDiv(['ct-grip__bump']));
      this._domGrip.appendChild(this.constructor.createDiv(['ct-grip__bump']));
      this._domGrip.appendChild(this.constructor.createDiv(['ct-grip__bump']));
      _ref = this._tools;                                              // 5457
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {      // 5458
        toolGroup = _ref[i];                                           // 5459
        domToolGroup = this.constructor.createDiv(['ct-tool-group']);  // 5460
        this._domElement.appendChild(domToolGroup);                    // 5461
        for (_j = 0, _len1 = toolGroup.length; _j < _len1; _j++) {     // 5462
          toolName = toolGroup[_j];                                    // 5463
          tool = ContentTools.ToolShelf.fetch(toolName);               // 5464
          this._toolUIs[toolName] = new ContentTools.ToolUI(tool);     // 5465
          this._toolUIs[toolName].mount(domToolGroup);                 // 5466
          this._toolUIs[toolName].disabled(true);                      // 5467
          this._toolUIs[toolName].bind('apply', (function (_this) {    // 5468
            return function () {                                       // 5469
              return _this.updateTools();                              // 5470
            };                                                         //
          })(this));                                                   //
        }                                                              //
      }                                                                //
      restore = window.localStorage.getItem('ct-toolbox-position');    // 5475
      if (restore && /^\d+,\d+$/.test(restore)) {                      // 5476
        position = (function () {                                      // 5477
          var _k, _len2, _ref1, _results;                              // 5478
          _ref1 = restore.split(',');                                  // 5479
          _results = [];                                               // 5480
          for (_k = 0, _len2 = _ref1.length; _k < _len2; _k++) {       // 5481
            coord = _ref1[_k];                                         // 5482
            _results.push(parseInt(coord));                            // 5483
          }                                                            //
          return _results;                                             // 5485
        })();                                                          //
        this._domElement.style.left = "" + position[0] + "px";         // 5487
        this._domElement.style.top = "" + position[1] + "px";          // 5488
        this._contain();                                               // 5489
      }                                                                //
      return this._addDOMEventListeners();                             // 5491
    };                                                                 //
                                                                       //
    ToolboxUI.prototype.updateTools = function () {                    // 5494
      var element, name, selection, toolUI, _ref, _results;            // 5495
      element = ContentEdit.Root.get().focused();                      // 5496
      selection = null;                                                // 5497
      if (element && element.selection) {                              // 5498
        selection = element.selection();                               // 5499
      }                                                                //
      _ref = this._toolUIs;                                            // 5501
      _results = [];                                                   // 5502
      for (name in babelHelpers.sanitizeForInObject(_ref)) {           // 5503
        toolUI = _ref[name];                                           // 5504
        _results.push(toolUI.update(element, selection));              // 5505
      }                                                                //
      return _results;                                                 // 5507
    };                                                                 //
                                                                       //
    ToolboxUI.prototype.unmount = function () {                        // 5510
      ToolboxUI.__super__.unmount.call(this);                          // 5511
      return this._domGrip = null;                                     // 5512
    };                                                                 //
                                                                       //
    ToolboxUI.prototype._addDOMEventListeners = function () {          // 5515
      this._domGrip.addEventListener('mousedown', this._onStartDragging);
      this._handleResize = (function (_this) {                         // 5517
        return function (ev) {                                         // 5518
          var containResize;                                           // 5519
          if (_this._resizeTimeout) {                                  // 5520
            clearTimeout(_this._resizeTimeout);                        // 5521
          }                                                            //
          containResize = function () {                                // 5523
            return _this._contain();                                   // 5524
          };                                                           //
          return _this._resizeTimeout = setTimeout(containResize, 250);
        };                                                             //
      })(this);                                                        //
      window.addEventListener('resize', this._handleResize);           // 5529
      this._updateTools = (function (_this) {                          // 5530
        return function () {                                           // 5531
          var app, element, name, selection, toolUI, update, _ref, _results;
          app = ContentTools.EditorApp.get();                          // 5533
          update = false;                                              // 5534
          element = ContentEdit.Root.get().focused();                  // 5535
          selection = null;                                            // 5536
          if (element === _this._lastUpdateElement) {                  // 5537
            if (element && element.selection) {                        // 5538
              selection = element.selection();                         // 5539
              if (_this._lastUpdateSelection && selection.eq(_this._lastUpdateSelection)) {
                update = true;                                         // 5541
              }                                                        //
            }                                                          //
          } else {                                                     //
            update = true;                                             // 5545
          }                                                            //
          if (app.history) {                                           // 5547
            if (_this._lastUpdateHistoryLength !== app.history.length()) {
              update = true;                                           // 5549
            }                                                          //
            _this._lastUpdateHistoryLength = app.history.length();     // 5551
          }                                                            //
          _this._lastUpdateElement = element;                          // 5553
          _this._lastUpdateSelection = selection;                      // 5554
          _ref = _this._toolUIs;                                       // 5555
          _results = [];                                               // 5556
          for (name in babelHelpers.sanitizeForInObject(_ref)) {       // 5557
            toolUI = _ref[name];                                       // 5558
            _results.push(toolUI.update(element, selection));          // 5559
          }                                                            //
          return _results;                                             // 5561
        };                                                             //
      })(this);                                                        //
      this._updateToolsTimeout = setInterval(this._updateTools, 100);  // 5564
      this._handleKeyDown = (function (_this) {                        // 5565
        return function (ev) {                                         // 5566
          var element, os, redo, undo, version;                        // 5567
          if (ev.keyCode === 46) {                                     // 5568
            element = ContentEdit.Root.get().focused();                // 5569
            if (element && !element.content) {                         // 5570
              ContentTools.Tools.Remove.apply(element, null, function () {});
            }                                                          //
          }                                                            //
          version = navigator.appVersion;                              // 5574
          os = 'linux';                                                // 5575
          if (version.indexOf('Mac') !== -1) {                         // 5576
            os = 'mac';                                                // 5577
          } else if (version.indexOf('Win') !== -1) {                  //
            os = 'windows';                                            // 5579
          }                                                            //
          redo = false;                                                // 5581
          undo = false;                                                // 5582
          switch (os) {                                                // 5583
            case 'linux':                                              // 5584
              if (ev.keyCode === 90 && ev.ctrlKey) {                   // 5585
                redo = ev.shiftKey;                                    // 5586
                undo = !redo;                                          // 5587
              }                                                        //
              break;                                                   // 5589
            case 'mac':                                                // 5589
              if (ev.keyCode === 90 && ev.metaKey) {                   // 5591
                redo = ev.shiftKey;                                    // 5592
                undo = !redo;                                          // 5593
              }                                                        //
              break;                                                   // 5595
            case 'windows':                                            // 5595
              if (ev.keyCode === 89 && ev.ctrlKey) {                   // 5597
                redo = true;                                           // 5598
              }                                                        //
              if (ev.keyCode === 90 && ev.ctrlKey) {                   // 5600
                undo = true;                                           // 5601
              }                                                        //
          }                                                            // 5602
          if (undo && ContentTools.Tools.Undo.canApply(null, null)) {  // 5604
            ContentTools.Tools.Undo.apply(null, null, function () {});
          }                                                            //
          if (redo && ContentTools.Tools.Redo.canApply(null, null)) {  // 5607
            return ContentTools.Tools.Redo.apply(null, null, function () {});
          }                                                            //
        };                                                             //
      })(this);                                                        //
      return window.addEventListener('keydown', this._handleKeyDown);  // 5612
    };                                                                 //
                                                                       //
    ToolboxUI.prototype._contain = function () {                       // 5615
      var rect;                                                        // 5616
      if (!this.isMounted()) {                                         // 5617
        return;                                                        // 5618
      }                                                                //
      rect = this._domElement.getBoundingClientRect();                 // 5620
      if (rect.left + rect.width > window.innerWidth) {                // 5621
        this._domElement.style.left = "" + (window.innerWidth - rect.width) + "px";
      }                                                                //
      if (rect.top + rect.height > window.innerHeight) {               // 5624
        this._domElement.style.top = "" + (window.innerHeight - rect.height) + "px";
      }                                                                //
      if (rect.left < 0) {                                             // 5627
        this._domElement.style.left = '0px';                           // 5628
      }                                                                //
      if (rect.top < 0) {                                              // 5630
        this._domElement.style.top = '0px';                            // 5631
      }                                                                //
      rect = this._domElement.getBoundingClientRect();                 // 5633
      return window.localStorage.setItem('ct-toolbox-position', "" + rect.left + "," + rect.top);
    };                                                                 //
                                                                       //
    ToolboxUI.prototype._removeDOMEventListeners = function () {       // 5637
      if (this.isMounted()) {                                          // 5638
        this._domGrip.removeEventListener('mousedown', this._onStartDragging);
      }                                                                //
      window.removeEventListener('keydown', this._handleKeyDown);      // 5641
      window.removeEventListener('resize', this._handleResize);        // 5642
      window.removeEventListener('resize', this._handleResize);        // 5643
      return clearInterval(this._updateToolsTimeout);                  // 5644
    };                                                                 //
                                                                       //
    ToolboxUI.prototype._onDrag = function (ev) {                      // 5647
      ContentSelect.Range.unselectAll();                               // 5648
      this._domElement.style.left = "" + (ev.clientX - this._draggingOffset.x) + "px";
      return this._domElement.style.top = "" + (ev.clientY - this._draggingOffset.y) + "px";
    };                                                                 //
                                                                       //
    ToolboxUI.prototype._onStartDragging = function (ev) {             // 5653
      var rect;                                                        // 5654
      ev.preventDefault();                                             // 5655
      if (this.isDragging()) {                                         // 5656
        return;                                                        // 5657
      }                                                                //
      this._dragging = true;                                           // 5659
      this.addCSSClass('ct-toolbox--dragging');                        // 5660
      rect = this._domElement.getBoundingClientRect();                 // 5661
      this._draggingOffset = {                                         // 5662
        x: ev.clientX - rect.left,                                     // 5663
        y: ev.clientY - rect.top                                       // 5664
      };                                                               //
      document.addEventListener('mousemove', this._onDrag);            // 5666
      document.addEventListener('mouseup', this._onStopDragging);      // 5667
      return ContentEdit.addCSSClass(document.body, 'ce--dragging');   // 5668
    };                                                                 //
                                                                       //
    ToolboxUI.prototype._onStopDragging = function (ev) {              // 5671
      if (!this.isDragging()) {                                        // 5672
        return;                                                        // 5673
      }                                                                //
      this._contain();                                                 // 5675
      document.removeEventListener('mousemove', this._onDrag);         // 5676
      document.removeEventListener('mouseup', this._onStopDragging);   // 5677
      this._draggingOffset = null;                                     // 5678
      this._dragging = false;                                          // 5679
      this.removeCSSClass('ct-toolbox--dragging');                     // 5680
      return ContentEdit.removeCSSClass(document.body, 'ce--dragging');
    };                                                                 //
                                                                       //
    return ToolboxUI;                                                  // 5684
  })(ContentTools.WidgetUI);                                           //
                                                                       //
  ContentTools.ToolUI = (function (_super) {                           // 5688
    __extends(ToolUI, _super);                                         // 5689
                                                                       //
    function ToolUI(tool) {                                            // 5691
      this._onMouseUp = __bind(this._onMouseUp, this);                 // 5692
      this._onMouseLeave = __bind(this._onMouseLeave, this);           // 5693
      this._onMouseDown = __bind(this._onMouseDown, this);             // 5694
      this._addDOMEventListeners = __bind(this._addDOMEventListeners, this);
      ToolUI.__super__.constructor.call(this);                         // 5696
      this.tool = tool;                                                // 5697
      this._mouseDown = false;                                         // 5698
      this._disabled = false;                                          // 5699
    }                                                                  //
                                                                       //
    ToolUI.prototype.apply = function (element, selection) {           // 5702
      var callback;                                                    // 5703
      if (!this.tool.canApply(element, selection)) {                   // 5704
        return;                                                        // 5705
      }                                                                //
      callback = (function (_this) {                                   // 5707
        return function (applied) {                                    // 5708
          if (applied) {                                               // 5709
            return _this.trigger('apply');                             // 5710
          }                                                            //
        };                                                             //
      })(this);                                                        //
      return this.tool.apply(element, selection, callback);            // 5714
    };                                                                 //
                                                                       //
    ToolUI.prototype.disabled = function (disabledState) {             // 5717
      if (disabledState === void 0) {                                  // 5718
        return this._disabled;                                         // 5719
      }                                                                //
      if (this._disabled === disabledState) {                          // 5721
        return;                                                        // 5722
      }                                                                //
      this._disabled = disabledState;                                  // 5724
      if (disabledState) {                                             // 5725
        this._mouseDown = false;                                       // 5726
        this.addCSSClass('ct-tool--disabled');                         // 5727
        return this.removeCSSClass('ct-tool--applied');                // 5728
      } else {                                                         //
        return this.removeCSSClass('ct-tool--disabled');               // 5730
      }                                                                //
    };                                                                 //
                                                                       //
    ToolUI.prototype.mount = function (domParent, before) {            // 5734
      if (before == null) {                                            // 5735
        before = null;                                                 // 5736
      }                                                                //
      this._domElement = this.constructor.createDiv(['ct-tool', "ct-tool--" + this.tool.icon]);
      this._domElement.setAttribute('data-tooltip', ContentEdit._(this.tool.label));
      return ToolUI.__super__.mount.call(this, domParent, before);     // 5740
    };                                                                 //
                                                                       //
    ToolUI.prototype.update = function (element, selection) {          // 5743
      if (!(element && element.isMounted())) {                         // 5744
        this.disabled(true);                                           // 5745
        return;                                                        // 5746
      }                                                                //
      if (this.tool.canApply(element, selection)) {                    // 5748
        this.disabled(false);                                          // 5749
      } else {                                                         //
        this.disabled(true);                                           // 5751
        return;                                                        // 5752
      }                                                                //
      if (this.tool.isApplied(element, selection)) {                   // 5754
        return this.addCSSClass('ct-tool--applied');                   // 5755
      } else {                                                         //
        return this.removeCSSClass('ct-tool--applied');                // 5757
      }                                                                //
    };                                                                 //
                                                                       //
    ToolUI.prototype._addDOMEventListeners = function () {             // 5761
      this._domElement.addEventListener('mousedown', this._onMouseDown);
      this._domElement.addEventListener('mouseleave', this._onMouseLeave);
      return this._domElement.addEventListener('mouseup', this._onMouseUp);
    };                                                                 //
                                                                       //
    ToolUI.prototype._onMouseDown = function (ev) {                    // 5767
      ev.preventDefault();                                             // 5768
      if (this.disabled()) {                                           // 5769
        return;                                                        // 5770
      }                                                                //
      this._mouseDown = true;                                          // 5772
      return this.addCSSClass('ct-tool--down');                        // 5773
    };                                                                 //
                                                                       //
    ToolUI.prototype._onMouseLeave = function () {                     // 5776
      this._mouseDown = false;                                         // 5777
      return this.removeCSSClass('ct-tool--down');                     // 5778
    };                                                                 //
                                                                       //
    ToolUI.prototype._onMouseUp = function () {                        // 5781
      var element, selection;                                          // 5782
      if (this._mouseDown) {                                           // 5783
        element = ContentEdit.Root.get().focused();                    // 5784
        if (!(element && element.isMounted())) {                       // 5785
          return;                                                      // 5786
        }                                                              //
        selection = null;                                              // 5788
        if (element.selection) {                                       // 5789
          selection = element.selection();                             // 5790
        }                                                              //
        this.apply(element, selection);                                // 5792
      }                                                                //
      this._mouseDown = false;                                         // 5794
      return this.removeCSSClass('ct-tool--down');                     // 5795
    };                                                                 //
                                                                       //
    return ToolUI;                                                     // 5798
  })(ContentTools.AnchoredComponentUI);                                //
                                                                       //
  ContentTools.AnchoredDialogUI = (function (_super) {                 // 5802
    __extends(AnchoredDialogUI, _super);                               // 5803
                                                                       //
    function AnchoredDialogUI() {                                      // 5805
      AnchoredDialogUI.__super__.constructor.call(this);               // 5806
      this._position = [0, 0];                                         // 5807
    }                                                                  //
                                                                       //
    AnchoredDialogUI.prototype.mount = function () {                   // 5810
      this._domElement = this.constructor.createDiv(['ct-widget', 'ct-anchored-dialog']);
      this.parent().domElement().appendChild(this._domElement);        // 5812
      this._domElement.style.top = "" + this._position[1] + "px";      // 5813
      return this._domElement.style.left = "" + this._position[0] + "px";
    };                                                                 //
                                                                       //
    AnchoredDialogUI.prototype.position = function (newPosition) {     // 5817
      if (newPosition === void 0) {                                    // 5818
        return this._position.slice();                                 // 5819
      }                                                                //
      this._position = newPosition.slice();                            // 5821
      if (this.isMounted()) {                                          // 5822
        this._domElement.style.top = "" + this._position[1] + "px";    // 5823
        return this._domElement.style.left = "" + this._position[0] + "px";
      }                                                                //
    };                                                                 //
                                                                       //
    return AnchoredDialogUI;                                           // 5828
  })(ContentTools.WidgetUI);                                           //
                                                                       //
  ContentTools.DialogUI = (function (_super) {                         // 5832
    __extends(DialogUI, _super);                                       // 5833
                                                                       //
    function DialogUI(caption) {                                       // 5835
      if (caption == null) {                                           // 5836
        caption = '';                                                  // 5837
      }                                                                //
      DialogUI.__super__.constructor.call(this);                       // 5839
      this._busy = false;                                              // 5840
      this._caption = caption;                                         // 5841
    }                                                                  //
                                                                       //
    DialogUI.prototype.busy = function (busy) {                        // 5844
      if (busy === void 0) {                                           // 5845
        return this._busy;                                             // 5846
      }                                                                //
      if (this._busy === busy) {                                       // 5848
        return;                                                        // 5849
      }                                                                //
      this._busy = busy;                                               // 5851
      if (!this.isMounted()) {                                         // 5852
        return;                                                        // 5853
      }                                                                //
      if (this._busy) {                                                // 5855
        return ContentEdit.addCSSClass(this._domElement, 'ct-dialog--busy');
      } else {                                                         //
        return ContentEdit.removeCSSClass(this._domElement, 'ct-dialog--busy');
      }                                                                //
    };                                                                 //
                                                                       //
    DialogUI.prototype.caption = function (caption) {                  // 5862
      if (caption === void 0) {                                        // 5863
        return this._caption;                                          // 5864
      }                                                                //
      this._caption = caption;                                         // 5866
      return this._domCaption.textContent = ContentEdit._(caption);    // 5867
    };                                                                 //
                                                                       //
    DialogUI.prototype.mount = function () {                           // 5870
      var dialogCSSClasses, domBody, domHeader;                        // 5871
      dialogCSSClasses = ['ct-widget', 'ct-dialog'];                   // 5872
      if (this._busy) {                                                // 5873
        dialogCSSClasses.push('ct-dialog--busy');                      // 5874
      }                                                                //
      this._domElement = this.constructor.createDiv(dialogCSSClasses);
      this.parent().domElement().appendChild(this._domElement);        // 5877
      domHeader = this.constructor.createDiv(['ct-dialog__header']);   // 5878
      this._domElement.appendChild(domHeader);                         // 5879
      this._domCaption = this.constructor.createDiv(['ct-dialog__caption']);
      domHeader.appendChild(this._domCaption);                         // 5881
      this.caption(this._caption);                                     // 5882
      this._domClose = this.constructor.createDiv(['ct-dialog__close']);
      domHeader.appendChild(this._domClose);                           // 5884
      domBody = this.constructor.createDiv(['ct-dialog__body']);       // 5885
      this._domElement.appendChild(domBody);                           // 5886
      this._domView = this.constructor.createDiv(['ct-dialog__view']);
      domBody.appendChild(this._domView);                              // 5888
      this._domControls = this.constructor.createDiv(['ct-dialog__controls']);
      domBody.appendChild(this._domControls);                          // 5890
      this._domBusy = this.constructor.createDiv(['ct-dialog__busy']);
      return this._domElement.appendChild(this._domBusy);              // 5892
    };                                                                 //
                                                                       //
    DialogUI.prototype.unmount = function () {                         // 5895
      DialogUI.__super__.unmount.call(this);                           // 5896
      this._domBusy = null;                                            // 5897
      this._domCaption = null;                                         // 5898
      this._domClose = null;                                           // 5899
      this._domControls = null;                                        // 5900
      return this._domView = null;                                     // 5901
    };                                                                 //
                                                                       //
    DialogUI.prototype._addDOMEventListeners = function () {           // 5904
      this._handleEscape = (function (_this) {                         // 5905
        return function (ev) {                                         // 5906
          if (_this._busy) {                                           // 5907
            return;                                                    // 5908
          }                                                            //
          if (ev.keyCode === 27) {                                     // 5910
            return _this.trigger('cancel');                            // 5911
          }                                                            //
        };                                                             //
      })(this);                                                        //
      document.addEventListener('keyup', this._handleEscape);          // 5915
      return this._domClose.addEventListener('click', (function (_this) {
        return function (ev) {                                         // 5917
          ev.preventDefault();                                         // 5918
          if (_this._busy) {                                           // 5919
            return;                                                    // 5920
          }                                                            //
          return _this.trigger('cancel');                              // 5922
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    DialogUI.prototype._removeDOMEventListeners = function () {        // 5927
      return document.removeEventListener('keyup', this._handleEscape);
    };                                                                 //
                                                                       //
    return DialogUI;                                                   // 5931
  })(ContentTools.WidgetUI);                                           //
                                                                       //
  ContentTools.ImageDialog = (function (_super) {                      // 5935
    __extends(ImageDialog, _super);                                    // 5936
                                                                       //
    function ImageDialog() {                                           // 5938
      ImageDialog.__super__.constructor.call(this, 'Insert image');    // 5939
      this._cropMarks = null;                                          // 5940
      this._imageURL = null;                                           // 5941
      this._imageSize = null;                                          // 5942
      this._progress = 0;                                              // 5943
      this._state = 'empty';                                           // 5944
      if (ContentTools.IMAGE_UPLOADER) {                               // 5945
        ContentTools.IMAGE_UPLOADER(this);                             // 5946
      }                                                                //
    }                                                                  //
                                                                       //
    ImageDialog.prototype.cropRegion = function () {                   // 5950
      if (this._cropMarks) {                                           // 5951
        return this._cropMarks.region();                               // 5952
      }                                                                //
      return [0, 0, 1, 1];                                             // 5954
    };                                                                 //
                                                                       //
    ImageDialog.prototype.addCropMarks = function () {                 // 5957
      if (this._cropMarks) {                                           // 5958
        return;                                                        // 5959
      }                                                                //
      this._cropMarks = new CropMarksUI(this._imageSize);              // 5961
      this._cropMarks.mount(this._domView);                            // 5962
      return ContentEdit.addCSSClass(this._domCrop, 'ct-control--active');
    };                                                                 //
                                                                       //
    ImageDialog.prototype.clear = function () {                        // 5966
      if (this._domImage) {                                            // 5967
        this._domImage.parentNode.removeChild(this._domImage);         // 5968
        this._domImage = null;                                         // 5969
      }                                                                //
      this._imageURL = null;                                           // 5971
      this._imageSize = null;                                          // 5972
      return this.state('empty');                                      // 5973
    };                                                                 //
                                                                       //
    ImageDialog.prototype.mount = function () {                        // 5976
      var domActions, domProgressBar, domTools;                        // 5977
      ImageDialog.__super__.mount.call(this);                          // 5978
      ContentEdit.addCSSClass(this._domElement, 'ct-image-dialog');    // 5979
      ContentEdit.addCSSClass(this._domElement, 'ct-image-dialog--empty');
      ContentEdit.addCSSClass(this._domView, 'ct-image-dialog__view');
      domTools = this.constructor.createDiv(['ct-control-group', 'ct-control-group--left']);
      this._domControls.appendChild(domTools);                         // 5983
      this._domRotateCCW = this.constructor.createDiv(['ct-control', 'ct-control--icon', 'ct-control--rotate-ccw']);
      this._domRotateCCW.setAttribute('data-tooltip', ContentEdit._('Rotate') + ' -90°');
      domTools.appendChild(this._domRotateCCW);                        // 5986
      this._domRotateCW = this.constructor.createDiv(['ct-control', 'ct-control--icon', 'ct-control--rotate-cw']);
      this._domRotateCW.setAttribute('data-tooltip', ContentEdit._('Rotate') + ' 90°');
      domTools.appendChild(this._domRotateCW);                         // 5989
      this._domCrop = this.constructor.createDiv(['ct-control', 'ct-control--icon', 'ct-control--crop']);
      this._domCrop.setAttribute('data-tooltip', ContentEdit._('Crop marks'));
      domTools.appendChild(this._domCrop);                             // 5992
      domProgressBar = this.constructor.createDiv(['ct-progress-bar']);
      domTools.appendChild(domProgressBar);                            // 5994
      this._domProgress = this.constructor.createDiv(['ct-progress-bar__progress']);
      domProgressBar.appendChild(this._domProgress);                   // 5996
      domActions = this.constructor.createDiv(['ct-control-group', 'ct-control-group--right']);
      this._domControls.appendChild(domActions);                       // 5998
      this._domUpload = this.constructor.createDiv(['ct-control', 'ct-control--text', 'ct-control--upload']);
      this._domUpload.textContent = ContentEdit._('Upload');           // 6000
      domActions.appendChild(this._domUpload);                         // 6001
      this._domInput = document.createElement('input');                // 6002
      this._domInput.setAttribute('class', 'ct-image-dialog__file-upload');
      this._domInput.setAttribute('name', 'file');                     // 6004
      this._domInput.setAttribute('type', 'file');                     // 6005
      this._domInput.setAttribute('accept', 'image/*');                // 6006
      this._domUpload.appendChild(this._domInput);                     // 6007
      this._domInsert = this.constructor.createDiv(['ct-control', 'ct-control--text', 'ct-control--insert']);
      this._domInsert.textContent = ContentEdit._('Insert');           // 6009
      domActions.appendChild(this._domInsert);                         // 6010
      this._domCancelUpload = this.constructor.createDiv(['ct-control', 'ct-control--text', 'ct-control--cancel']);
      this._domCancelUpload.textContent = ContentEdit._('Cancel');     // 6012
      domActions.appendChild(this._domCancelUpload);                   // 6013
      this._domClear = this.constructor.createDiv(['ct-control', 'ct-control--text', 'ct-control--clear']);
      this._domClear.textContent = ContentEdit._('Clear');             // 6015
      domActions.appendChild(this._domClear);                          // 6016
      this._addDOMEventListeners();                                    // 6017
      return this.trigger('imageUploader.mount');                      // 6018
    };                                                                 //
                                                                       //
    ImageDialog.prototype.populate = function (imageURL, imageSize) {  // 6021
      this._imageURL = imageURL;                                       // 6022
      this._imageSize = imageSize;                                     // 6023
      if (!this._domImage) {                                           // 6024
        this._domImage = this.constructor.createDiv(['ct-image-dialog__image']);
        this._domView.appendChild(this._domImage);                     // 6026
      }                                                                //
      this._domImage.style['background-image'] = "url(" + imageURL + ")";
      return this.state('populated');                                  // 6029
    };                                                                 //
                                                                       //
    ImageDialog.prototype.progress = function (progress) {             // 6032
      if (progress === void 0) {                                       // 6033
        return this._progress;                                         // 6034
      }                                                                //
      this._progress = progress;                                       // 6036
      if (!this.isMounted()) {                                         // 6037
        return;                                                        // 6038
      }                                                                //
      return this._domProgress.style.width = "" + this._progress + "%";
    };                                                                 //
                                                                       //
    ImageDialog.prototype.removeCropMarks = function () {              // 6043
      if (!this._cropMarks) {                                          // 6044
        return;                                                        // 6045
      }                                                                //
      this._cropMarks.unmount();                                       // 6047
      this._cropMarks = null;                                          // 6048
      return ContentEdit.removeCSSClass(this._domCrop, 'ct-control--active');
    };                                                                 //
                                                                       //
    ImageDialog.prototype.save = function (imageURL, imageSize, imageAttrs) {
      return this.trigger('save', imageURL, imageSize, imageAttrs);    // 6053
    };                                                                 //
                                                                       //
    ImageDialog.prototype.state = function (state) {                   // 6056
      var prevState;                                                   // 6057
      if (state === void 0) {                                          // 6058
        return this._state;                                            // 6059
      }                                                                //
      if (this._state === state) {                                     // 6061
        return;                                                        // 6062
      }                                                                //
      prevState = this._state;                                         // 6064
      this._state = state;                                             // 6065
      if (!this.isMounted()) {                                         // 6066
        return;                                                        // 6067
      }                                                                //
      ContentEdit.addCSSClass(this._domElement, "ct-image-dialog--" + this._state);
      return ContentEdit.removeCSSClass(this._domElement, "ct-image-dialog--" + prevState);
    };                                                                 //
                                                                       //
    ImageDialog.prototype.unmount = function () {                      // 6073
      ImageDialog.__super__.unmount.call(this);                        // 6074
      this._domCancelUpload = null;                                    // 6075
      this._domClear = null;                                           // 6076
      this._domCrop = null;                                            // 6077
      this._domInput = null;                                           // 6078
      this._domInsert = null;                                          // 6079
      this._domProgress = null;                                        // 6080
      this._domRotateCCW = null;                                       // 6081
      this._domRotateCW = null;                                        // 6082
      this._domUpload = null;                                          // 6083
      return this.trigger('imageUploader.unmount');                    // 6084
    };                                                                 //
                                                                       //
    ImageDialog.prototype._addDOMEventListeners = function () {        // 6087
      ImageDialog.__super__._addDOMEventListeners.call(this);          // 6088
      this._domInput.addEventListener('change', (function (_this) {    // 6089
        return function (ev) {                                         // 6090
          var file;                                                    // 6091
          file = ev.target.files[0];                                   // 6092
          ev.target.value = '';                                        // 6093
          if (ev.target.value) {                                       // 6094
            ev.target.type = 'text';                                   // 6095
            ev.target.type = 'file';                                   // 6096
          }                                                            //
          return _this.trigger('imageUploader.fileReady', file);       // 6098
        };                                                             //
      })(this));                                                       //
      this._domCancelUpload.addEventListener('click', (function (_this) {
        return function (ev) {                                         // 6102
          return _this.trigger('imageUploader.cancelUpload');          // 6103
        };                                                             //
      })(this));                                                       //
      this._domClear.addEventListener('click', (function (_this) {     // 6106
        return function (ev) {                                         // 6107
          _this.removeCropMarks();                                     // 6108
          return _this.trigger('imageUploader.clear');                 // 6109
        };                                                             //
      })(this));                                                       //
      this._domRotateCCW.addEventListener('click', (function (_this) {
        return function (ev) {                                         // 6113
          _this.removeCropMarks();                                     // 6114
          return _this.trigger('imageUploader.rotateCCW');             // 6115
        };                                                             //
      })(this));                                                       //
      this._domRotateCW.addEventListener('click', (function (_this) {  // 6118
        return function (ev) {                                         // 6119
          _this.removeCropMarks();                                     // 6120
          return _this.trigger('imageUploader.rotateCW');              // 6121
        };                                                             //
      })(this));                                                       //
      this._domCrop.addEventListener('click', (function (_this) {      // 6124
        return function (ev) {                                         // 6125
          if (_this._cropMarks) {                                      // 6126
            return _this.removeCropMarks();                            // 6127
          } else {                                                     //
            return _this.addCropMarks();                               // 6129
          }                                                            //
        };                                                             //
      })(this));                                                       //
      return this._domInsert.addEventListener('click', (function (_this) {
        return function (ev) {                                         // 6134
          return _this.trigger('imageUploader.save');                  // 6135
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    return ImageDialog;                                                // 6140
  })(ContentTools.DialogUI);                                           //
                                                                       //
  CropMarksUI = (function (_super) {                                   // 6144
    __extends(CropMarksUI, _super);                                    // 6145
                                                                       //
    function CropMarksUI(imageSize) {                                  // 6147
      CropMarksUI.__super__.constructor.call(this);                    // 6148
      this._bounds = null;                                             // 6149
      this._dragging = null;                                           // 6150
      this._draggingOrigin = null;                                     // 6151
      this._imageSize = imageSize;                                     // 6152
    }                                                                  //
                                                                       //
    CropMarksUI.prototype.mount = function (domParent, before) {       // 6155
      if (before == null) {                                            // 6156
        before = null;                                                 // 6157
      }                                                                //
      this._domElement = this.constructor.createDiv(['ct-crop-marks']);
      this._domClipper = this.constructor.createDiv(['ct-crop-marks__clipper']);
      this._domElement.appendChild(this._domClipper);                  // 6161
      this._domRulers = [this.constructor.createDiv(['ct-crop-marks__ruler', 'ct-crop-marks__ruler--top-left']), this.constructor.createDiv(['ct-crop-marks__ruler', 'ct-crop-marks__ruler--bottom-right'])];
      this._domClipper.appendChild(this._domRulers[0]);                // 6163
      this._domClipper.appendChild(this._domRulers[1]);                // 6164
      this._domHandles = [this.constructor.createDiv(['ct-crop-marks__handle', 'ct-crop-marks__handle--top-left']), this.constructor.createDiv(['ct-crop-marks__handle', 'ct-crop-marks__handle--bottom-right'])];
      this._domElement.appendChild(this._domHandles[0]);               // 6166
      this._domElement.appendChild(this._domHandles[1]);               // 6167
      CropMarksUI.__super__.mount.call(this, domParent, before);       // 6168
      return this._fit(domParent);                                     // 6169
    };                                                                 //
                                                                       //
    CropMarksUI.prototype.region = function () {                       // 6172
      return [parseFloat(this._domHandles[0].style.top) / this._bounds[1], parseFloat(this._domHandles[0].style.left) / this._bounds[0], parseFloat(this._domHandles[1].style.top) / this._bounds[1], parseFloat(this._domHandles[1].style.left) / this._bounds[0]];
    };                                                                 //
                                                                       //
    CropMarksUI.prototype.unmount = function () {                      // 6176
      CropMarksUI.__super__.unmount.call(this);                        // 6177
      this._domClipper = null;                                         // 6178
      this._domHandles = null;                                         // 6179
      return this._domRulers = null;                                   // 6180
    };                                                                 //
                                                                       //
    CropMarksUI.prototype._addDOMEventListeners = function () {        // 6183
      CropMarksUI.__super__._addDOMEventListeners.call(this);          // 6184
      this._domHandles[0].addEventListener('mousedown', (function (_this) {
        return function (ev) {                                         // 6186
          if (ev.button === 0) {                                       // 6187
            return _this._startDrag(0, ev.clientY, ev.clientX);        // 6188
          }                                                            //
        };                                                             //
      })(this));                                                       //
      return this._domHandles[1].addEventListener('mousedown', (function (_this) {
        return function (ev) {                                         // 6193
          if (ev.button === 0) {                                       // 6194
            return _this._startDrag(1, ev.clientY, ev.clientX);        // 6195
          }                                                            //
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    CropMarksUI.prototype._drag = function (top, left) {               // 6201
      var height, minCrop, offsetLeft, offsetTop, width;               // 6202
      if (this._dragging === null) {                                   // 6203
        return;                                                        // 6204
      }                                                                //
      ContentSelect.Range.unselectAll();                               // 6206
      offsetTop = top - this._draggingOrigin[1];                       // 6207
      offsetLeft = left - this._draggingOrigin[0];                     // 6208
      height = this._bounds[1];                                        // 6209
      left = 0;                                                        // 6210
      top = 0;                                                         // 6211
      width = this._bounds[0];                                         // 6212
      minCrop = Math.min(Math.min(ContentTools.MIN_CROP, height), width);
      if (this._dragging === 0) {                                      // 6214
        height = parseInt(this._domHandles[1].style.top) - minCrop;    // 6215
        width = parseInt(this._domHandles[1].style.left) - minCrop;    // 6216
      } else {                                                         //
        left = parseInt(this._domHandles[0].style.left) + minCrop;     // 6218
        top = parseInt(this._domHandles[0].style.top) + minCrop;       // 6219
      }                                                                //
      offsetTop = Math.min(Math.max(top, offsetTop), height);          // 6221
      offsetLeft = Math.min(Math.max(left, offsetLeft), width);        // 6222
      this._domHandles[this._dragging].style.top = "" + offsetTop + "px";
      this._domHandles[this._dragging].style.left = "" + offsetLeft + "px";
      this._domRulers[this._dragging].style.top = "" + offsetTop + "px";
      return this._domRulers[this._dragging].style.left = "" + offsetLeft + "px";
    };                                                                 //
                                                                       //
    CropMarksUI.prototype._fit = function (domParent) {                // 6229
      var height, heightScale, left, ratio, rect, top, width, widthScale;
      rect = domParent.getBoundingClientRect();                        // 6231
      widthScale = rect.width / this._imageSize[0];                    // 6232
      heightScale = rect.height / this._imageSize[1];                  // 6233
      ratio = Math.min(widthScale, heightScale);                       // 6234
      width = ratio * this._imageSize[0];                              // 6235
      height = ratio * this._imageSize[1];                             // 6236
      left = (rect.width - width) / 2;                                 // 6237
      top = (rect.height - height) / 2;                                // 6238
      this._domElement.style.width = "" + width + "px";                // 6239
      this._domElement.style.height = "" + height + "px";              // 6240
      this._domElement.style.top = "" + top + "px";                    // 6241
      this._domElement.style.left = "" + left + "px";                  // 6242
      this._domHandles[0].style.top = '0px';                           // 6243
      this._domHandles[0].style.left = '0px';                          // 6244
      this._domHandles[1].style.top = "" + height + "px";              // 6245
      this._domHandles[1].style.left = "" + width + "px";              // 6246
      this._domRulers[0].style.top = '0px';                            // 6247
      this._domRulers[0].style.left = '0px';                           // 6248
      this._domRulers[1].style.top = "" + height + "px";               // 6249
      this._domRulers[1].style.left = "" + width + "px";               // 6250
      return this._bounds = [width, height];                           // 6251
    };                                                                 //
                                                                       //
    CropMarksUI.prototype._startDrag = function (handleIndex, top, left) {
      var domHandle;                                                   // 6255
      domHandle = this._domHandles[handleIndex];                       // 6256
      this._dragging = handleIndex;                                    // 6257
      this._draggingOrigin = [left - parseInt(domHandle.style.left), top - parseInt(domHandle.style.top)];
      this._onMouseMove = (function (_this) {                          // 6259
        return function (ev) {                                         // 6260
          return _this._drag(ev.clientY, ev.clientX);                  // 6261
        };                                                             //
      })(this);                                                        //
      document.addEventListener('mousemove', this._onMouseMove);       // 6264
      this._onMouseUp = (function (_this) {                            // 6265
        return function (ev) {                                         // 6266
          return _this._stopDrag();                                    // 6267
        };                                                             //
      })(this);                                                        //
      return document.addEventListener('mouseup', this._onMouseUp);    // 6270
    };                                                                 //
                                                                       //
    CropMarksUI.prototype._stopDrag = function () {                    // 6273
      document.removeEventListener('mousemove', this._onMouseMove);    // 6274
      document.removeEventListener('mouseup', this._onMouseUp);        // 6275
      this._dragging = null;                                           // 6276
      return this._draggingOrigin = null;                              // 6277
    };                                                                 //
                                                                       //
    return CropMarksUI;                                                // 6280
  })(ContentTools.AnchoredComponentUI);                                //
                                                                       //
  ContentTools.LinkDialog = (function (_super) {                       // 6284
    __extends(LinkDialog, _super);                                     // 6285
                                                                       //
    function LinkDialog(initialValue) {                                // 6287
      if (initialValue == null) {                                      // 6288
        initialValue = '';                                             // 6289
      }                                                                //
      LinkDialog.__super__.constructor.call(this);                     // 6291
      this._initialValue = initialValue;                               // 6292
    }                                                                  //
                                                                       //
    LinkDialog.prototype.mount = function () {                         // 6295
      LinkDialog.__super__.mount.call(this);                           // 6296
      this._domInput = document.createElement('input');                // 6297
      this._domInput.setAttribute('class', 'ct-anchored-dialog__input');
      this._domInput.setAttribute('name', 'href');                     // 6299
      this._domInput.setAttribute('placeholder', ContentEdit._('Enter a link') + '...');
      this._domInput.setAttribute('type', 'text');                     // 6301
      this._domInput.setAttribute('value', this._initialValue);        // 6302
      this._domElement.appendChild(this._domInput);                    // 6303
      this._domButton = this.constructor.createDiv(['ct-anchored-dialog__button']);
      this._domElement.appendChild(this._domButton);                   // 6305
      return this._addDOMEventListeners();                             // 6306
    };                                                                 //
                                                                       //
    LinkDialog.prototype.save = function () {                          // 6309
      if (!this.isMounted) {                                           // 6310
        return this.trigger('save', '');                               // 6311
      }                                                                //
      return this.trigger('save', this._domInput.value.trim());        // 6313
    };                                                                 //
                                                                       //
    LinkDialog.prototype.show = function () {                          // 6316
      LinkDialog.__super__.show.call(this);                            // 6317
      this._domInput.focus();                                          // 6318
      if (this._initialValue) {                                        // 6319
        return this._domInput.select();                                // 6320
      }                                                                //
    };                                                                 //
                                                                       //
    LinkDialog.prototype.unmount = function () {                       // 6324
      if (this.isMounted()) {                                          // 6325
        this._domInput.blur();                                         // 6326
      }                                                                //
      LinkDialog.__super__.unmount.call(this);                         // 6328
      this._domButton = null;                                          // 6329
      return this._domInput = null;                                    // 6330
    };                                                                 //
                                                                       //
    LinkDialog.prototype._addDOMEventListeners = function () {         // 6333
      this._domInput.addEventListener('keypress', (function (_this) {  // 6334
        return function (ev) {                                         // 6335
          if (ev.keyCode === 13) {                                     // 6336
            return _this.save();                                       // 6337
          }                                                            //
        };                                                             //
      })(this));                                                       //
      return this._domButton.addEventListener('click', (function (_this) {
        return function (ev) {                                         // 6342
          ev.preventDefault();                                         // 6343
          return _this.save();                                         // 6344
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    return LinkDialog;                                                 // 6349
  })(ContentTools.AnchoredDialogUI);                                   //
                                                                       //
  ContentTools.PropertiesDialog = (function (_super) {                 // 6353
    __extends(PropertiesDialog, _super);                               // 6354
                                                                       //
    function PropertiesDialog(element) {                               // 6356
      var _ref;                                                        // 6357
      this.element = element;                                          // 6358
      PropertiesDialog.__super__.constructor.call(this, 'Properties');
      this._attributeUIs = [];                                         // 6360
      this._focusedAttributeUI = null;                                 // 6361
      this._styleUIs = [];                                             // 6362
      this._supportsCoding = element.content;                          // 6363
      if ((_ref = element.constructor.name) === 'ListItem' || _ref === 'TableCell') {
        this._supportsCoding = true;                                   // 6365
      }                                                                //
    }                                                                  //
                                                                       //
    PropertiesDialog.prototype.caption = function (caption) {          // 6369
      if (caption === void 0) {                                        // 6370
        return this._caption;                                          // 6371
      }                                                                //
      this._caption = caption;                                         // 6373
      return this._domCaption.textContent = ContentEdit._(caption) + (": " + this.element.tagName());
    };                                                                 //
                                                                       //
    PropertiesDialog.prototype.changedAttributes = function () {       // 6377
      var attributeUI, attributes, changedAttributes, name, restricted, value, _i, _len, _ref, _ref1;
      attributes = {};                                                 // 6379
      changedAttributes = {};                                          // 6380
      _ref = this._attributeUIs;                                       // 6381
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 6382
        attributeUI = _ref[_i];                                        // 6383
        name = attributeUI.name();                                     // 6384
        value = attributeUI.value();                                   // 6385
        if (name === '') {                                             // 6386
          continue;                                                    // 6387
        }                                                              //
        attributes[name.toLowerCase()] = true;                         // 6389
        if (this.element.attr(name) !== value) {                       // 6390
          changedAttributes[name] = value;                             // 6391
        }                                                              //
      }                                                                //
      restricted = ContentTools.RESTRICTED_ATTRIBUTES[this.element.tagName()];
      _ref1 = this.element.attributes();                               // 6395
      for (name in babelHelpers.sanitizeForInObject(_ref1)) {          // 6396
        value = _ref1[name];                                           // 6397
        if (restricted && restricted.indexOf(name.toLowerCase()) !== -1) {
          continue;                                                    // 6399
        }                                                              //
        if (attributes[name] === void 0) {                             // 6401
          changedAttributes[name] = null;                              // 6402
        }                                                              //
      }                                                                //
      return changedAttributes;                                        // 6405
    };                                                                 //
                                                                       //
    PropertiesDialog.prototype.changedStyles = function () {           // 6408
      var cssClass, styleUI, styles, _i, _len, _ref;                   // 6409
      styles = {};                                                     // 6410
      _ref = this._styleUIs;                                           // 6411
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 6412
        styleUI = _ref[_i];                                            // 6413
        cssClass = styleUI.style.cssClass();                           // 6414
        if (this.element.hasCSSClass(cssClass) !== styleUI.applied()) {
          styles[cssClass] = styleUI.applied();                        // 6416
        }                                                              //
      }                                                                //
      return styles;                                                   // 6419
    };                                                                 //
                                                                       //
    PropertiesDialog.prototype.getElementInnerHTML = function () {     // 6422
      if (!this._supportsCoding) {                                     // 6423
        return null;                                                   // 6424
      }                                                                //
      if (this.element.content) {                                      // 6426
        return this.element.content.html();                            // 6427
      }                                                                //
      return this.element.children[0].content.html();                  // 6429
    };                                                                 //
                                                                       //
    PropertiesDialog.prototype.mount = function () {                   // 6432
      var attributeNames, attributes, domActions, domTabs, lastTab, name, restricted, style, styleUI, value, _i, _j, _len, _len1, _ref;
      PropertiesDialog.__super__.mount.call(this);                     // 6434
      ContentEdit.addCSSClass(this._domElement, 'ct-properties-dialog');
      ContentEdit.addCSSClass(this._domView, 'ct-properties-dialog__view');
      this._domStyles = this.constructor.createDiv(['ct-properties-dialog__styles']);
      this._domStyles.setAttribute('data-ct-empty', ContentEdit._('No styles available for this tag'));
      this._domView.appendChild(this._domStyles);                      // 6439
      _ref = ContentTools.StylePalette.styles(this.element.tagName());
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 6441
        style = _ref[_i];                                              // 6442
        styleUI = new StyleUI(style, this.element.hasCSSClass(style.cssClass()));
        this._styleUIs.push(styleUI);                                  // 6444
        styleUI.mount(this._domStyles);                                // 6445
      }                                                                //
      this._domAttributes = this.constructor.createDiv(['ct-properties-dialog__attributes']);
      this._domView.appendChild(this._domAttributes);                  // 6448
      restricted = ContentTools.RESTRICTED_ATTRIBUTES[this.element.tagName()];
      attributes = this.element.attributes();                          // 6450
      attributeNames = [];                                             // 6451
      for (name in babelHelpers.sanitizeForInObject(attributes)) {     // 6452
        value = attributes[name];                                      // 6453
        if (restricted && restricted.indexOf(name.toLowerCase()) !== -1) {
          continue;                                                    // 6455
        }                                                              //
        attributeNames.push(name);                                     // 6457
      }                                                                //
      attributeNames.sort();                                           // 6459
      for (_j = 0, _len1 = attributeNames.length; _j < _len1; _j++) {  // 6460
        name = attributeNames[_j];                                     // 6461
        value = attributes[name];                                      // 6462
        this._addAttributeUI(name, value);                             // 6463
      }                                                                //
      this._addAttributeUI('', '');                                    // 6465
      this._domCode = this.constructor.createDiv(['ct-properties-dialog__code']);
      this._domView.appendChild(this._domCode);                        // 6467
      this._domInnerHTML = document.createElement('textarea');         // 6468
      this._domInnerHTML.setAttribute('class', 'ct-properties-dialog__inner-html');
      this._domInnerHTML.setAttribute('name', 'code');                 // 6470
      this._domInnerHTML.value = this.getElementInnerHTML();           // 6471
      this._domCode.appendChild(this._domInnerHTML);                   // 6472
      domTabs = this.constructor.createDiv(['ct-control-group', 'ct-control-group--left']);
      this._domControls.appendChild(domTabs);                          // 6474
      this._domStylesTab = this.constructor.createDiv(['ct-control', 'ct-control--icon', 'ct-control--styles']);
      this._domStylesTab.setAttribute('data-tooltip', ContentEdit._('Styles'));
      domTabs.appendChild(this._domStylesTab);                         // 6477
      this._domAttributesTab = this.constructor.createDiv(['ct-control', 'ct-control--icon', 'ct-control--attributes']);
      this._domAttributesTab.setAttribute('data-tooltip', ContentEdit._('Attributes'));
      domTabs.appendChild(this._domAttributesTab);                     // 6480
      this._domCodeTab = this.constructor.createDiv(['ct-control', 'ct-control--icon', 'ct-control--code']);
      this._domCodeTab.setAttribute('data-tooltip', ContentEdit._('Code'));
      domTabs.appendChild(this._domCodeTab);                           // 6483
      if (!this._supportsCoding) {                                     // 6484
        ContentEdit.addCSSClass(this._domCodeTab, 'ct-control--muted');
      }                                                                //
      this._domRemoveAttribute = this.constructor.createDiv(['ct-control', 'ct-control--icon', 'ct-control--remove', 'ct-control--muted']);
      this._domRemoveAttribute.setAttribute('data-tooltip', ContentEdit._('Remove'));
      domTabs.appendChild(this._domRemoveAttribute);                   // 6489
      domActions = this.constructor.createDiv(['ct-control-group', 'ct-control-group--right']);
      this._domControls.appendChild(domActions);                       // 6491
      this._domApply = this.constructor.createDiv(['ct-control', 'ct-control--text', 'ct-control--apply']);
      this._domApply.textContent = ContentEdit._('Apply');             // 6493
      domActions.appendChild(this._domApply);                          // 6494
      lastTab = window.localStorage.getItem('ct-properties-dialog-tab');
      if (lastTab === 'attributes') {                                  // 6496
        ContentEdit.addCSSClass(this._domElement, 'ct-properties-dialog--attributes');
        ContentEdit.addCSSClass(this._domAttributesTab, 'ct-control--active');
      } else if (lastTab === 'code' && this._supportsCoding) {         //
        ContentEdit.addCSSClass(this._domElement, 'ct-properties-dialog--code');
        ContentEdit.addCSSClass(this._domCodeTab, 'ct-control--active');
      } else {                                                         //
        ContentEdit.addCSSClass(this._domElement, 'ct-properties-dialog--styles');
        ContentEdit.addCSSClass(this._domStylesTab, 'ct-control--active');
      }                                                                //
      return this._addDOMEventListeners();                             // 6506
    };                                                                 //
                                                                       //
    PropertiesDialog.prototype.save = function () {                    // 6509
      var innerHTML;                                                   // 6510
      innerHTML = null;                                                // 6511
      if (this._supportsCoding) {                                      // 6512
        innerHTML = this._domInnerHTML.value;                          // 6513
      }                                                                //
      return this.trigger('save', this.changedAttributes(), this.changedStyles(), innerHTML);
    };                                                                 //
                                                                       //
    PropertiesDialog.prototype._addAttributeUI = function (name, value) {
      var attributeUI, dialog;                                         // 6519
      dialog = this;                                                   // 6520
      attributeUI = new AttributeUI(name, value);                      // 6521
      this._attributeUIs.push(attributeUI);                            // 6522
      attributeUI.bind('blur', function () {                           // 6523
        var index, lastAttributeUI, length;                            // 6524
        dialog._focusedAttributeUI = null;                             // 6525
        ContentEdit.addCSSClass(dialog._domRemoveAttribute, 'ct-control--muted');
        index = dialog._attributeUIs.indexOf(this);                    // 6527
        length = dialog._attributeUIs.length;                          // 6528
        if (this.name() === '' && index < length - 1) {                // 6529
          this.unmount();                                              // 6530
          dialog._attributeUIs.splice(index, 1);                       // 6531
        }                                                              //
        lastAttributeUI = dialog._attributeUIs[length - 1];            // 6533
        if (lastAttributeUI) {                                         // 6534
          if (lastAttributeUI.name() && lastAttributeUI.value()) {     // 6535
            return dialog._addAttributeUI('', '');                     // 6536
          }                                                            //
        }                                                              //
      });                                                              //
      attributeUI.bind('focus', function () {                          // 6540
        dialog._focusedAttributeUI = this;                             // 6541
        return ContentEdit.removeCSSClass(dialog._domRemoveAttribute, 'ct-control--muted');
      });                                                              //
      attributeUI.bind('namechange', function () {                     // 6544
        var element, otherAttributeUI, restricted, valid, _i, _len, _ref;
        element = dialog.element;                                      // 6546
        name = this.name().toLowerCase();                              // 6547
        restricted = ContentTools.RESTRICTED_ATTRIBUTES[element.tagName()];
        valid = true;                                                  // 6549
        if (restricted && restricted.indexOf(name) !== -1) {           // 6550
          valid = false;                                               // 6551
        }                                                              //
        _ref = dialog._attributeUIs;                                   // 6553
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 6554
          otherAttributeUI = _ref[_i];                                 // 6555
          if (name === '') {                                           // 6556
            continue;                                                  // 6557
          }                                                            //
          if (otherAttributeUI === this) {                             // 6559
            continue;                                                  // 6560
          }                                                            //
          if (otherAttributeUI.name().toLowerCase() !== name) {        // 6562
            continue;                                                  // 6563
          }                                                            //
          valid = false;                                               // 6565
        }                                                              //
        this.valid(valid);                                             // 6567
        if (valid) {                                                   // 6568
          return ContentEdit.removeCSSClass(dialog._domApply, 'ct-control--muted');
        } else {                                                       //
          return ContentEdit.addCSSClass(dialog._domApply, 'ct-control--muted');
        }                                                              //
      });                                                              //
      attributeUI.mount(this._domAttributes);                          // 6574
      return attributeUI;                                              // 6575
    };                                                                 //
                                                                       //
    PropertiesDialog.prototype._addDOMEventListeners = function () {   // 6578
      var selectTab, validateCode;                                     // 6579
      PropertiesDialog.__super__._addDOMEventListeners.call(this);     // 6580
      selectTab = (function (_this) {                                  // 6581
        return function (selected) {                                   // 6582
          var selectedCap, tab, tabCap, tabs, _i, _len;                // 6583
          tabs = ['attributes', 'code', 'styles'];                     // 6584
          for (_i = 0, _len = tabs.length; _i < _len; _i++) {          // 6585
            tab = tabs[_i];                                            // 6586
            if (tab === selected) {                                    // 6587
              continue;                                                // 6588
            }                                                          //
            tabCap = tab.charAt(0).toUpperCase() + tab.slice(1);       // 6590
            ContentEdit.removeCSSClass(_this._domElement, "ct-properties-dialog--" + tab);
            ContentEdit.removeCSSClass(_this["_dom" + tabCap + "Tab"], 'ct-control--active');
          }                                                            //
          selectedCap = selected.charAt(0).toUpperCase() + selected.slice(1);
          ContentEdit.addCSSClass(_this._domElement, "ct-properties-dialog--" + selected);
          ContentEdit.addCSSClass(_this["_dom" + selectedCap + "Tab"], 'ct-control--active');
          return window.localStorage.setItem('ct-properties-dialog-tab', selected);
        };                                                             //
      })(this);                                                        //
      this._domStylesTab.addEventListener('mousedown', (function (_this) {
        return function () {                                           // 6601
          return selectTab('styles');                                  // 6602
        };                                                             //
      })(this));                                                       //
      this._domAttributesTab.addEventListener('mousedown', (function (_this) {
        return function () {                                           // 6606
          return selectTab('attributes');                              // 6607
        };                                                             //
      })(this));                                                       //
      this._domCodeTab.addEventListener('mousedown', (function (_this) {
        return function () {                                           // 6611
          return selectTab('code');                                    // 6612
        };                                                             //
      })(this));                                                       //
      this._domRemoveAttribute.addEventListener('mousedown', (function (_this) {
        return function (ev) {                                         // 6616
          var index, last;                                             // 6617
          ev.preventDefault();                                         // 6618
          if (_this._focusedAttributeUI) {                             // 6619
            index = _this._attributeUIs.indexOf(_this._focusedAttributeUI);
            last = index === _this._attributeUIs.length - 1;           // 6621
            _this._focusedAttributeUI.unmount();                       // 6622
            _this._attributeUIs.splice(index, 1);                      // 6623
            if (last) {                                                // 6624
              return _this._addAttributeUI('', '');                    // 6625
            }                                                          //
          }                                                            //
        };                                                             //
      })(this));                                                       //
      validateCode = (function (_this) {                               // 6630
        return function (ev) {                                         // 6631
          var content;                                                 // 6632
          try {                                                        // 6633
            content = new HTMLString.String(_this._domInnerHTML.value);
            ContentEdit.removeCSSClass(_this._domInnerHTML, 'ct-properties-dialog__inner-html--invalid');
            return ContentEdit.removeCSSClass(_this._domApply, 'ct-control--muted');
          } catch (_error) {                                           //
            ContentEdit.addCSSClass(_this._domInnerHTML, 'ct-properties-dialog__inner-html--invalid');
            return ContentEdit.addCSSClass(_this._domApply, 'ct-control--muted');
          }                                                            //
        };                                                             //
      })(this);                                                        //
      this._domInnerHTML.addEventListener('input', validateCode);      // 6643
      this._domInnerHTML.addEventListener('propertychange', validateCode);
      return this._domApply.addEventListener('click', (function (_this) {
        return function (ev) {                                         // 6646
          var cssClass;                                                // 6647
          ev.preventDefault();                                         // 6648
          cssClass = _this._domApply.getAttribute('class');            // 6649
          if (cssClass.indexOf('ct-control--muted') === -1) {          // 6650
            return _this.save();                                       // 6651
          }                                                            //
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    return PropertiesDialog;                                           // 6657
  })(ContentTools.DialogUI);                                           //
                                                                       //
  StyleUI = (function (_super) {                                       // 6661
    __extends(StyleUI, _super);                                        // 6662
                                                                       //
    function StyleUI(style, applied) {                                 // 6664
      this.style = style;                                              // 6665
      StyleUI.__super__.constructor.call(this);                        // 6666
      this._applied = applied;                                         // 6667
    }                                                                  //
                                                                       //
    StyleUI.prototype.applied = function (applied) {                   // 6670
      if (applied === void 0) {                                        // 6671
        return this._applied;                                          // 6672
      }                                                                //
      if (this._applied === applied) {                                 // 6674
        return;                                                        // 6675
      }                                                                //
      this._applied = applied;                                         // 6677
      if (this._applied) {                                             // 6678
        return ContentEdit.addCSSClass(this._domElement, 'ct-section--applied');
      } else {                                                         //
        return ContentEdit.removeCSSClass(this._domElement, 'ct-section--applied');
      }                                                                //
    };                                                                 //
                                                                       //
    StyleUI.prototype.mount = function (domParent, before) {           // 6685
      var label;                                                       // 6686
      if (before == null) {                                            // 6687
        before = null;                                                 // 6688
      }                                                                //
      this._domElement = this.constructor.createDiv(['ct-section']);   // 6690
      if (this._applied) {                                             // 6691
        ContentEdit.addCSSClass(this._domElement, 'ct-section--applied');
      }                                                                //
      label = this.constructor.createDiv(['ct-section__label']);       // 6694
      label.textContent = this.style.name();                           // 6695
      this._domElement.appendChild(label);                             // 6696
      this._domElement.appendChild(this.constructor.createDiv(['ct-section__switch']));
      return StyleUI.__super__.mount.call(this, domParent, before);    // 6698
    };                                                                 //
                                                                       //
    StyleUI.prototype._addDOMEventListeners = function () {            // 6701
      var toggleSection;                                               // 6702
      toggleSection = (function (_this) {                              // 6703
        return function (ev) {                                         // 6704
          ev.preventDefault();                                         // 6705
          if (_this.applied()) {                                       // 6706
            return _this.applied(false);                               // 6707
          } else {                                                     //
            return _this.applied(true);                                // 6709
          }                                                            //
        };                                                             //
      })(this);                                                        //
      return this._domElement.addEventListener('click', toggleSection);
    };                                                                 //
                                                                       //
    return StyleUI;                                                    // 6716
  })(ContentTools.AnchoredComponentUI);                                //
                                                                       //
  AttributeUI = (function (_super) {                                   // 6720
    __extends(AttributeUI, _super);                                    // 6721
                                                                       //
    function AttributeUI(name, value) {                                // 6723
      AttributeUI.__super__.constructor.call(this);                    // 6724
      this._initialName = name;                                        // 6725
      this._initialValue = value;                                      // 6726
    }                                                                  //
                                                                       //
    AttributeUI.prototype.name = function () {                         // 6729
      return this._domName.value.trim();                               // 6730
    };                                                                 //
                                                                       //
    AttributeUI.prototype.value = function () {                        // 6733
      return this._domValue.value.trim();                              // 6734
    };                                                                 //
                                                                       //
    AttributeUI.prototype.mount = function (domParent, before) {       // 6737
      if (before == null) {                                            // 6738
        before = null;                                                 // 6739
      }                                                                //
      this._domElement = this.constructor.createDiv(['ct-attribute']);
      this._domName = document.createElement('input');                 // 6742
      this._domName.setAttribute('class', 'ct-attribute__name');       // 6743
      this._domName.setAttribute('name', 'name');                      // 6744
      this._domName.setAttribute('placeholder', ContentEdit._('Name'));
      this._domName.setAttribute('type', 'text');                      // 6746
      this._domName.setAttribute('value', this._initialName);          // 6747
      this._domElement.appendChild(this._domName);                     // 6748
      this._domValue = document.createElement('input');                // 6749
      this._domValue.setAttribute('class', 'ct-attribute__value');     // 6750
      this._domValue.setAttribute('name', 'value');                    // 6751
      this._domValue.setAttribute('placeholder', ContentEdit._('Value'));
      this._domValue.setAttribute('type', 'text');                     // 6753
      this._domValue.setAttribute('value', this._initialValue);        // 6754
      this._domElement.appendChild(this._domValue);                    // 6755
      return AttributeUI.__super__.mount.call(this, domParent, before);
    };                                                                 //
                                                                       //
    AttributeUI.prototype.valid = function (valid) {                   // 6759
      if (valid) {                                                     // 6760
        return ContentEdit.removeCSSClass(this._domName, 'ct-attribute__name--invalid');
      } else {                                                         //
        return ContentEdit.addCSSClass(this._domName, 'ct-attribute__name--invalid');
      }                                                                //
    };                                                                 //
                                                                       //
    AttributeUI.prototype._addDOMEventListeners = function () {        // 6767
      this._domName.addEventListener('blur', (function (_this) {       // 6768
        return function () {                                           // 6769
          var name, nextDomAttribute, nextNameDom;                     // 6770
          name = _this.name();                                         // 6771
          nextDomAttribute = _this._domElement.nextSibling;            // 6772
          _this.trigger('blur');                                       // 6773
          if (name === '' && nextDomAttribute) {                       // 6774
            nextNameDom = nextDomAttribute.querySelector('.ct-attribute__name');
            return nextNameDom.focus();                                // 6776
          }                                                            //
        };                                                             //
      })(this));                                                       //
      this._domName.addEventListener('focus', (function (_this) {      // 6780
        return function () {                                           // 6781
          return _this.trigger('focus');                               // 6782
        };                                                             //
      })(this));                                                       //
      this._domName.addEventListener('input', (function (_this) {      // 6785
        return function () {                                           // 6786
          return _this.trigger('namechange');                          // 6787
        };                                                             //
      })(this));                                                       //
      this._domName.addEventListener('keydown', (function (_this) {    // 6790
        return function (ev) {                                         // 6791
          if (ev.keyCode === 13) {                                     // 6792
            return _this._domValue.focus();                            // 6793
          }                                                            //
        };                                                             //
      })(this));                                                       //
      this._domValue.addEventListener('blur', (function (_this) {      // 6797
        return function () {                                           // 6798
          return _this.trigger('blur');                                // 6799
        };                                                             //
      })(this));                                                       //
      this._domValue.addEventListener('focus', (function (_this) {     // 6802
        return function () {                                           // 6803
          return _this.trigger('focus');                               // 6804
        };                                                             //
      })(this));                                                       //
      return this._domValue.addEventListener('keydown', (function (_this) {
        return function (ev) {                                         // 6808
          var nextDomAttribute, nextNameDom;                           // 6809
          if (ev.keyCode !== 13 && (ev.keyCode !== 9 || ev.shiftKey)) {
            return;                                                    // 6811
          }                                                            //
          ev.preventDefault();                                         // 6813
          nextDomAttribute = _this._domElement.nextSibling;            // 6814
          if (!nextDomAttribute) {                                     // 6815
            _this._domValue.blur();                                    // 6816
            nextDomAttribute = _this._domElement.nextSibling;          // 6817
          }                                                            //
          if (nextDomAttribute) {                                      // 6819
            nextNameDom = nextDomAttribute.querySelector('.ct-attribute__name');
            return nextNameDom.focus();                                // 6821
          }                                                            //
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    return AttributeUI;                                                // 6827
  })(ContentTools.AnchoredComponentUI);                                //
                                                                       //
  ContentTools.TableDialog = (function (_super) {                      // 6831
    __extends(TableDialog, _super);                                    // 6832
                                                                       //
    function TableDialog(table) {                                      // 6834
      this.table = table;                                              // 6835
      if (this.table) {                                                // 6836
        TableDialog.__super__.constructor.call(this, 'Update table');  // 6837
      } else {                                                         //
        TableDialog.__super__.constructor.call(this, 'Insert table');  // 6839
      }                                                                //
    }                                                                  //
                                                                       //
    TableDialog.prototype.mount = function () {                        // 6843
      var cfg, domBodyLabel, domControlGroup, domFootLabel, domHeadLabel, footCSSClasses, headCSSClasses;
      TableDialog.__super__.mount.call(this);                          // 6845
      cfg = {                                                          // 6846
        columns: 3,                                                    // 6847
        foot: false,                                                   // 6848
        head: true                                                     // 6849
      };                                                               //
      if (this.table) {                                                // 6851
        cfg = {                                                        // 6852
          columns: this.table.firstSection().children[0].children.length,
          foot: this.table.tfoot(),                                    // 6854
          head: this.table.thead()                                     // 6855
        };                                                             //
      }                                                                //
      ContentEdit.addCSSClass(this._domElement, 'ct-table-dialog');    // 6858
      ContentEdit.addCSSClass(this._domView, 'ct-table-dialog__view');
      headCSSClasses = ['ct-section'];                                 // 6860
      if (cfg.head) {                                                  // 6861
        headCSSClasses.push('ct-section--applied');                    // 6862
      }                                                                //
      this._domHeadSection = this.constructor.createDiv(headCSSClasses);
      this._domView.appendChild(this._domHeadSection);                 // 6865
      domHeadLabel = this.constructor.createDiv(['ct-section__label']);
      domHeadLabel.textContent = ContentEdit._('Table head');          // 6867
      this._domHeadSection.appendChild(domHeadLabel);                  // 6868
      this._domHeadSwitch = this.constructor.createDiv(['ct-section__switch']);
      this._domHeadSection.appendChild(this._domHeadSwitch);           // 6870
      this._domBodySection = this.constructor.createDiv(['ct-section', 'ct-section--applied', 'ct-section--contains-input']);
      this._domView.appendChild(this._domBodySection);                 // 6872
      domBodyLabel = this.constructor.createDiv(['ct-section__label']);
      domBodyLabel.textContent = ContentEdit._('Table body (columns)');
      this._domBodySection.appendChild(domBodyLabel);                  // 6875
      this._domBodyInput = document.createElement('input');            // 6876
      this._domBodyInput.setAttribute('class', 'ct-section__input');   // 6877
      this._domBodyInput.setAttribute('maxlength', '2');               // 6878
      this._domBodyInput.setAttribute('name', 'columns');              // 6879
      this._domBodyInput.setAttribute('type', 'text');                 // 6880
      this._domBodyInput.setAttribute('value', cfg.columns);           // 6881
      this._domBodySection.appendChild(this._domBodyInput);            // 6882
      footCSSClasses = ['ct-section'];                                 // 6883
      if (cfg.foot) {                                                  // 6884
        footCSSClasses.push('ct-section--applied');                    // 6885
      }                                                                //
      this._domFootSection = this.constructor.createDiv(footCSSClasses);
      this._domView.appendChild(this._domFootSection);                 // 6888
      domFootLabel = this.constructor.createDiv(['ct-section__label']);
      domFootLabel.textContent = ContentEdit._('Table foot');          // 6890
      this._domFootSection.appendChild(domFootLabel);                  // 6891
      this._domFootSwitch = this.constructor.createDiv(['ct-section__switch']);
      this._domFootSection.appendChild(this._domFootSwitch);           // 6893
      domControlGroup = this.constructor.createDiv(['ct-control-group', 'ct-control-group--right']);
      this._domControls.appendChild(domControlGroup);                  // 6895
      this._domApply = this.constructor.createDiv(['ct-control', 'ct-control--text', 'ct-control--apply']);
      this._domApply.textContent = 'Apply';                            // 6897
      domControlGroup.appendChild(this._domApply);                     // 6898
      return this._addDOMEventListeners();                             // 6899
    };                                                                 //
                                                                       //
    TableDialog.prototype.save = function () {                         // 6902
      var footCSSClass, headCSSClass, tableCfg;                        // 6903
      footCSSClass = this._domFootSection.getAttribute('class');       // 6904
      headCSSClass = this._domHeadSection.getAttribute('class');       // 6905
      tableCfg = {                                                     // 6906
        columns: parseInt(this._domBodyInput.value),                   // 6907
        foot: footCSSClass.indexOf('ct-section--applied') > -1,        // 6908
        head: headCSSClass.indexOf('ct-section--applied') > -1         // 6909
      };                                                               //
      return this.trigger('save', tableCfg);                           // 6911
    };                                                                 //
                                                                       //
    TableDialog.prototype.unmount = function () {                      // 6914
      TableDialog.__super__.unmount.call(this);                        // 6915
      this._domBodyInput = null;                                       // 6916
      this._domBodySection = null;                                     // 6917
      this._domApply = null;                                           // 6918
      this._domHeadSection = null;                                     // 6919
      this._domHeadSwitch = null;                                      // 6920
      this._domFootSection = null;                                     // 6921
      return this._domFootSwitch = null;                               // 6922
    };                                                                 //
                                                                       //
    TableDialog.prototype._addDOMEventListeners = function () {        // 6925
      var toggleSection;                                               // 6926
      TableDialog.__super__._addDOMEventListeners.call(this);          // 6927
      toggleSection = function (ev) {                                  // 6928
        ev.preventDefault();                                           // 6929
        if (this.getAttribute('class').indexOf('ct-section--applied') > -1) {
          return ContentEdit.removeCSSClass(this, 'ct-section--applied');
        } else {                                                       //
          return ContentEdit.addCSSClass(this, 'ct-section--applied');
        }                                                              //
      };                                                               //
      this._domHeadSection.addEventListener('click', toggleSection);   // 6936
      this._domFootSection.addEventListener('click', toggleSection);   // 6937
      this._domBodySection.addEventListener('click', (function (_this) {
        return function (ev) {                                         // 6939
          return _this._domBodyInput.focus();                          // 6940
        };                                                             //
      })(this));                                                       //
      this._domBodyInput.addEventListener('input', (function (_this) {
        return function (ev) {                                         // 6944
          var valid;                                                   // 6945
          valid = /^[1-9]\d{0,1}$/.test(ev.target.value);              // 6946
          if (valid) {                                                 // 6947
            ContentEdit.removeCSSClass(_this._domBodyInput, 'ct-section__input--invalid');
            return ContentEdit.removeCSSClass(_this._domApply, 'ct-control--muted');
          } else {                                                     //
            ContentEdit.addCSSClass(_this._domBodyInput, 'ct-section__input--invalid');
            return ContentEdit.addCSSClass(_this._domApply, 'ct-control--muted');
          }                                                            //
        };                                                             //
      })(this));                                                       //
      return this._domApply.addEventListener('click', (function (_this) {
        return function (ev) {                                         // 6957
          var cssClass;                                                // 6958
          ev.preventDefault();                                         // 6959
          cssClass = _this._domApply.getAttribute('class');            // 6960
          if (cssClass.indexOf('ct-control--muted') === -1) {          // 6961
            return _this.save();                                       // 6962
          }                                                            //
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    return TableDialog;                                                // 6968
  })(ContentTools.DialogUI);                                           //
                                                                       //
  ContentTools.VideoDialog = (function (_super) {                      // 6972
    __extends(VideoDialog, _super);                                    // 6973
                                                                       //
    function VideoDialog() {                                           // 6975
      VideoDialog.__super__.constructor.call(this, 'Insert video');    // 6976
    }                                                                  //
                                                                       //
    VideoDialog.prototype.clearPreview = function () {                 // 6979
      if (this._domPreview) {                                          // 6980
        this._domPreview.parentNode.removeChild(this._domPreview);     // 6981
        return this._domPreview = void 0;                              // 6982
      }                                                                //
    };                                                                 //
                                                                       //
    VideoDialog.prototype.mount = function () {                        // 6986
      var domControlGroup;                                             // 6987
      VideoDialog.__super__.mount.call(this);                          // 6988
      ContentEdit.addCSSClass(this._domElement, 'ct-video-dialog');    // 6989
      ContentEdit.addCSSClass(this._domView, 'ct-video-dialog__preview');
      domControlGroup = this.constructor.createDiv(['ct-control-group']);
      this._domControls.appendChild(domControlGroup);                  // 6992
      this._domInput = document.createElement('input');                // 6993
      this._domInput.setAttribute('class', 'ct-video-dialog__input');  // 6994
      this._domInput.setAttribute('name', 'url');                      // 6995
      this._domInput.setAttribute('placeholder', ContentEdit._('Paste YouTube or Vimeo URL') + '...');
      this._domInput.setAttribute('type', 'text');                     // 6997
      domControlGroup.appendChild(this._domInput);                     // 6998
      this._domButton = this.constructor.createDiv(['ct-control', 'ct-control--text', 'ct-control--insert', 'ct-control--muted']);
      this._domButton.textContent = ContentEdit._('Insert');           // 7000
      domControlGroup.appendChild(this._domButton);                    // 7001
      return this._addDOMEventListeners();                             // 7002
    };                                                                 //
                                                                       //
    VideoDialog.prototype.preview = function (url) {                   // 7005
      this.clearPreview();                                             // 7006
      this._domPreview = document.createElement('iframe');             // 7007
      this._domPreview.setAttribute('frameborder', '0');               // 7008
      this._domPreview.setAttribute('height', '100%');                 // 7009
      this._domPreview.setAttribute('src', url);                       // 7010
      this._domPreview.setAttribute('width', '100%');                  // 7011
      return this._domView.appendChild(this._domPreview);              // 7012
    };                                                                 //
                                                                       //
    VideoDialog.prototype.save = function () {                         // 7015
      var embedURL, videoURL;                                          // 7016
      videoURL = this._domInput.value.trim();                          // 7017
      embedURL = ContentTools.getEmbedVideoURL(videoURL);              // 7018
      if (embedURL) {                                                  // 7019
        return this.trigger('save', embedURL);                         // 7020
      } else {                                                         //
        return this.trigger('save', videoURL);                         // 7022
      }                                                                //
    };                                                                 //
                                                                       //
    VideoDialog.prototype.show = function () {                         // 7026
      VideoDialog.__super__.show.call(this);                           // 7027
      return this._domInput.focus();                                   // 7028
    };                                                                 //
                                                                       //
    VideoDialog.prototype.unmount = function () {                      // 7031
      if (this.isMounted()) {                                          // 7032
        this._domInput.blur();                                         // 7033
      }                                                                //
      VideoDialog.__super__.unmount.call(this);                        // 7035
      this._domButton = null;                                          // 7036
      this._domInput = null;                                           // 7037
      return this._domPreview = null;                                  // 7038
    };                                                                 //
                                                                       //
    VideoDialog.prototype._addDOMEventListeners = function () {        // 7041
      VideoDialog.__super__._addDOMEventListeners.call(this);          // 7042
      this._domInput.addEventListener('input', (function (_this) {     // 7043
        return function (ev) {                                         // 7044
          var updatePreview;                                           // 7045
          if (ev.target.value) {                                       // 7046
            ContentEdit.removeCSSClass(_this._domButton, 'ct-control--muted');
          } else {                                                     //
            ContentEdit.addCSSClass(_this._domButton, 'ct-control--muted');
          }                                                            //
          if (_this._updatePreviewTimeout) {                           // 7051
            clearTimeout(_this._updatePreviewTimeout);                 // 7052
          }                                                            //
          updatePreview = function () {                                // 7054
            var embedURL, videoURL;                                    // 7055
            videoURL = _this._domInput.value.trim();                   // 7056
            embedURL = ContentTools.getEmbedVideoURL(videoURL);        // 7057
            if (embedURL) {                                            // 7058
              return _this.preview(embedURL);                          // 7059
            } else {                                                   //
              return _this.clearPreview();                             // 7061
            }                                                          //
          };                                                           //
          return _this._updatePreviewTimeout = setTimeout(updatePreview, 500);
        };                                                             //
      })(this));                                                       //
      this._domInput.addEventListener('keypress', (function (_this) {  // 7067
        return function (ev) {                                         // 7068
          if (ev.keyCode === 13) {                                     // 7069
            return _this.save();                                       // 7070
          }                                                            //
        };                                                             //
      })(this));                                                       //
      return this._domButton.addEventListener('click', (function (_this) {
        return function (ev) {                                         // 7075
          var cssClass;                                                // 7076
          ev.preventDefault();                                         // 7077
          cssClass = _this._domButton.getAttribute('class');           // 7078
          if (cssClass.indexOf('ct-control--muted') === -1) {          // 7079
            return _this.save();                                       // 7080
          }                                                            //
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    return VideoDialog;                                                // 7086
  })(ContentTools.DialogUI);                                           //
                                                                       //
  _EditorApp = (function (_super) {                                    // 7090
    __extends(_EditorApp, _super);                                     // 7091
                                                                       //
    function _EditorApp() {                                            // 7093
      _EditorApp.__super__.constructor.call(this);                     // 7094
      this.history = null;                                             // 7095
      this._state = ContentTools.EditorApp.DORMANT;                    // 7096
      this._regions = null;                                            // 7097
      this._orderedRegions = null;                                     // 7098
      this._rootLastModified = null;                                   // 7099
      this._regionsLastModified = {};                                  // 7100
      this._ignition = null;                                           // 7101
      this._inspector = null;                                          // 7102
      this._toolbox = null;                                            // 7103
    }                                                                  //
                                                                       //
    _EditorApp.prototype.ctrlDown = function () {                      // 7106
      return this._ctrlDown;                                           // 7107
    };                                                                 //
                                                                       //
    _EditorApp.prototype.domRegions = function () {                    // 7110
      return this._domRegions;                                         // 7111
    };                                                                 //
                                                                       //
    _EditorApp.prototype.orderedRegions = function () {                // 7114
      var name;                                                        // 7115
      return (function () {                                            // 7116
        var _i, _len, _ref, _results;                                  // 7117
        _ref = this._orderedRegions;                                   // 7118
        _results = [];                                                 // 7119
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 7120
          name = _ref[_i];                                             // 7121
          _results.push(this._regions[name]);                          // 7122
        }                                                              //
        return _results;                                               // 7124
      }).call(this);                                                   //
    };                                                                 //
                                                                       //
    _EditorApp.prototype.regions = function () {                       // 7128
      return this._regions;                                            // 7129
    };                                                                 //
                                                                       //
    _EditorApp.prototype.shiftDown = function () {                     // 7132
      return this._shiftDown;                                          // 7133
    };                                                                 //
                                                                       //
    _EditorApp.prototype.busy = function (busy) {                      // 7136
      return this._ignition.busy(busy);                                // 7137
    };                                                                 //
                                                                       //
    _EditorApp.prototype.init = function (queryOrDOMElements, namingProp) {
      if (namingProp == null) {                                        // 7141
        namingProp = 'id';                                             // 7142
      }                                                                //
      this._namingProp = namingProp;                                   // 7144
      if (queryOrDOMElements.length > 0 && queryOrDOMElements[0].nodeType === Node.ELEMENT_NODE) {
        this._domRegions = queryOrDOMElements;                         // 7146
      } else {                                                         //
        this._domRegions = document.querySelectorAll(queryOrDOMElements);
      }                                                                //
      if (this._domRegions.length === 0) {                             // 7150
        return;                                                        // 7151
      }                                                                //
      this.mount();                                                    // 7153
      this._ignition = new ContentTools.IgnitionUI();                  // 7154
      this.attach(this._ignition);                                     // 7155
      this._ignition.bind('start', (function (_this) {                 // 7156
        return function () {                                           // 7157
          return _this.start();                                        // 7158
        };                                                             //
      })(this));                                                       //
      this._ignition.bind('stop', (function (_this) {                  // 7161
        return function (save) {                                       // 7162
          var focused;                                                 // 7163
          focused = ContentEdit.Root.get().focused();                  // 7164
          if (focused && focused._syncContent !== void 0) {            // 7165
            focused._syncContent();                                    // 7166
          }                                                            //
          if (save) {                                                  // 7168
            _this.save();                                              // 7169
          } else {                                                     //
            if (!_this.revert()) {                                     // 7171
              _this._ignition.changeState('editing');                  // 7172
              return;                                                  // 7173
            }                                                          //
          }                                                            //
          return _this.stop();                                         // 7176
        };                                                             //
      })(this));                                                       //
      if (this._domRegions.length) {                                   // 7179
        this._ignition.show();                                         // 7180
      }                                                                //
      this._toolbox = new ContentTools.ToolboxUI(ContentTools.DEFAULT_TOOLS);
      this.attach(this._toolbox);                                      // 7183
      this._inspector = new ContentTools.InspectorUI();                // 7184
      this.attach(this._inspector);                                    // 7185
      this._state = ContentTools.EditorApp.READY;                      // 7186
      ContentEdit.Root.get().bind('detach', (function (_this) {        // 7187
        return function (element) {                                    // 7188
          return _this._preventEmptyRegions();                         // 7189
        };                                                             //
      })(this));                                                       //
      ContentEdit.Root.get().bind('paste', (function (_this) {         // 7192
        return function (element, ev) {                                // 7193
          return _this.paste(element, ev.clipboardData);               // 7194
        };                                                             //
      })(this));                                                       //
      ContentEdit.Root.get().bind('next-region', (function (_this) {   // 7197
        return function (region) {                                     // 7198
          var child, element, index, regions, _i, _len, _ref;          // 7199
          regions = _this.orderedRegions();                            // 7200
          index = regions.indexOf(region);                             // 7201
          if (index >= regions.length - 1) {                           // 7202
            return;                                                    // 7203
          }                                                            //
          region = regions[index + 1];                                 // 7205
          element = null;                                              // 7206
          _ref = region.descendants();                                 // 7207
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {          // 7208
            child = _ref[_i];                                          // 7209
            if (child.content !== void 0) {                            // 7210
              element = child;                                         // 7211
              break;                                                   // 7212
            }                                                          //
          }                                                            //
          if (element) {                                               // 7215
            element.focus();                                           // 7216
            element.selection(new ContentSelect.Range(0, 0));          // 7217
            return;                                                    // 7218
          }                                                            //
          return ContentEdit.Root.get().trigger('next-region', region);
        };                                                             //
      })(this));                                                       //
      return ContentEdit.Root.get().bind('previous-region', (function (_this) {
        return function (region) {                                     // 7224
          var child, descendants, element, index, length, regions, _i, _len;
          regions = _this.orderedRegions();                            // 7226
          index = regions.indexOf(region);                             // 7227
          if (index <= 0) {                                            // 7228
            return;                                                    // 7229
          }                                                            //
          region = regions[index - 1];                                 // 7231
          element = null;                                              // 7232
          descendants = region.descendants();                          // 7233
          descendants.reverse();                                       // 7234
          for (_i = 0, _len = descendants.length; _i < _len; _i++) {   // 7235
            child = descendants[_i];                                   // 7236
            if (child.content !== void 0) {                            // 7237
              element = child;                                         // 7238
              break;                                                   // 7239
            }                                                          //
          }                                                            //
          if (element) {                                               // 7242
            length = element.content.length();                         // 7243
            element.focus();                                           // 7244
            element.selection(new ContentSelect.Range(length, length));
            return;                                                    // 7246
          }                                                            //
          return ContentEdit.Root.get().trigger('previous-region', region);
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    _EditorApp.prototype.destroy = function () {                       // 7253
      return this.unmount();                                           // 7254
    };                                                                 //
                                                                       //
    _EditorApp.prototype.highlightRegions = function (highlight) {     // 7257
      var domRegion, _i, _len, _ref, _results;                         // 7258
      _ref = this._domRegions;                                         // 7259
      _results = [];                                                   // 7260
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 7261
        domRegion = _ref[_i];                                          // 7262
        if (highlight) {                                               // 7263
          _results.push(ContentEdit.addCSSClass(domRegion, 'ct--highlight'));
        } else {                                                       //
          _results.push(ContentEdit.removeCSSClass(domRegion, 'ct--highlight'));
        }                                                              //
      }                                                                //
      return _results;                                                 // 7269
    };                                                                 //
                                                                       //
    _EditorApp.prototype.mount = function () {                         // 7272
      this._domElement = this.constructor.createDiv(['ct-app']);       // 7273
      document.body.insertBefore(this._domElement, null);              // 7274
      return this._addDOMEventListeners();                             // 7275
    };                                                                 //
                                                                       //
    _EditorApp.prototype.paste = function (element, clipboardData) {   // 7278
      var className, content, cursor, encodeHTML, i, insertAt, insertIn, insertNode, item, itemText, lastItem, line, lineLength, lines, selection, tail, tip, _i, _len;
      content = clipboardData.getData('text/plain');                   // 7280
      lines = content.split('\n');                                     // 7281
      lines = lines.filter(function (line) {                           // 7282
        return line.trim() !== '';                                     // 7283
      });                                                              //
      if (!lines) {                                                    // 7285
        return;                                                        // 7286
      }                                                                //
      encodeHTML = HTMLString.String.encode;                           // 7288
      className = element.constructor.name;                            // 7289
      if ((lines.length > 1 || !element.content) && className !== 'PreText') {
        if (className === 'ListItemText') {                            // 7291
          insertNode = element.parent();                               // 7292
          insertIn = element.parent().parent();                        // 7293
          insertAt = insertIn.children.indexOf(insertNode) + 1;        // 7294
        } else {                                                       //
          insertNode = element;                                        // 7296
          if (insertNode.parent().constructor.name !== 'Region') {     // 7297
            insertNode = element.closest(function (node) {             // 7298
              return node.parent().constructor.name === 'Region';      // 7299
            });                                                        //
          }                                                            //
          insertIn = insertNode.parent();                              // 7302
          insertAt = insertIn.children.indexOf(insertNode) + 1;        // 7303
        }                                                              //
        for (i = _i = 0, _len = lines.length; _i < _len; i = ++_i) {   // 7305
          line = lines[i];                                             // 7306
          line = encodeHTML(line);                                     // 7307
          if (className === 'ListItemText') {                          // 7308
            item = new ContentEdit.ListItem();                         // 7309
            itemText = new ContentEdit.ListItemText(line);             // 7310
            item.attach(itemText);                                     // 7311
            lastItem = itemText;                                       // 7312
          } else {                                                     //
            item = new ContentEdit.Text('p', {}, line);                // 7314
            lastItem = item;                                           // 7315
          }                                                            //
          insertIn.attach(item, insertAt + i);                         // 7317
        }                                                              //
        lineLength = lastItem.content.length();                        // 7319
        lastItem.focus();                                              // 7320
        return lastItem.selection(new ContentSelect.Range(lineLength, lineLength));
      } else {                                                         //
        content = encodeHTML(content);                                 // 7323
        content = new HTMLString.String(content, className === 'PreText');
        selection = element.selection();                               // 7325
        cursor = selection.get()[0] + content.length();                // 7326
        tip = element.content.substring(0, selection.get()[0]);        // 7327
        tail = element.content.substring(selection.get()[1]);          // 7328
        element.content = tip.concat(content);                         // 7329
        element.content = element.content.concat(tail, false);         // 7330
        element.updateInnerHTML();                                     // 7331
        element.taint();                                               // 7332
        selection.set(cursor, cursor);                                 // 7333
        return element.selection(selection);                           // 7334
      }                                                                //
    };                                                                 //
                                                                       //
    _EditorApp.prototype.unmount = function () {                       // 7338
      if (!this.isMounted()) {                                         // 7339
        return;                                                        // 7340
      }                                                                //
      this._domElement.parentNode.removeChild(this._domElement);       // 7342
      this._domElement = null;                                         // 7343
      this._removeDOMEventListeners();                                 // 7344
      this._ignition = null;                                           // 7345
      this._inspector = null;                                          // 7346
      return this._toolbox = null;                                     // 7347
    };                                                                 //
                                                                       //
    _EditorApp.prototype.revert = function () {                        // 7350
      var confirmMessage;                                              // 7351
      confirmMessage = ContentEdit._('Your changes have not been saved, do you really want to lose them?');
      if (ContentEdit.Root.get().lastModified() > this._rootLastModified && !window.confirm(confirmMessage)) {
        return false;                                                  // 7354
      }                                                                //
      this.revertToSnapshot(this.history.goTo(0), false);              // 7356
      return true;                                                     // 7357
    };                                                                 //
                                                                       //
    _EditorApp.prototype.revertToSnapshot = function (snapshot, restoreEditable) {
      var domRegion, i, name, region, _i, _len, _ref, _ref1;           // 7361
      if (restoreEditable == null) {                                   // 7362
        restoreEditable = true;                                        // 7363
      }                                                                //
      _ref = this._regions;                                            // 7365
      for (name in babelHelpers.sanitizeForInObject(_ref)) {           // 7366
        region = _ref[name];                                           // 7367
        region.domElement().innerHTML = snapshot.regions[name];        // 7368
      }                                                                //
      if (restoreEditable) {                                           // 7370
        this._regions = {};                                            // 7371
        _ref1 = this._domRegions;                                      // 7372
        for (i = _i = 0, _len = _ref1.length; _i < _len; i = ++_i) {   // 7373
          domRegion = _ref1[i];                                        // 7374
          name = domRegion.getAttribute(this._namingProp);             // 7375
          if (!name) {                                                 // 7376
            name = i;                                                  // 7377
          }                                                            //
          this._regions[name] = new ContentEdit.Region(domRegion);     // 7379
        }                                                              //
        this.history.replaceRegions(this._regions);                    // 7381
        return this.history.restoreSelection(snapshot);                // 7382
      }                                                                //
    };                                                                 //
                                                                       //
    _EditorApp.prototype.save = function () {                          // 7386
      var args, child, html, modifiedRegions, name, passive, region, root, _ref;
      passive = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      root = ContentEdit.Root.get();                                   // 7389
      if (root.lastModified() === this._rootLastModified && passive) {
        return;                                                        // 7391
      }                                                                //
      modifiedRegions = {};                                            // 7393
      _ref = this._regions;                                            // 7394
      for (name in babelHelpers.sanitizeForInObject(_ref)) {           // 7395
        region = _ref[name];                                           // 7396
        html = region.html();                                          // 7397
        if (region.children.length === 1) {                            // 7398
          child = region.children[0];                                  // 7399
          if (child.content && !child.content.html()) {                // 7400
            html = '';                                                 // 7401
          }                                                            //
        }                                                              //
        if (!passive) {                                                // 7404
          region.domElement().innerHTML = html;                        // 7405
        }                                                              //
        if (region.lastModified() === this._regionsLastModified[name]) {
          continue;                                                    // 7408
        }                                                              //
        modifiedRegions[name] = html;                                  // 7410
      }                                                                //
      return this.trigger.apply(this, ['save', modifiedRegions].concat(__slice.call(args)));
    };                                                                 //
                                                                       //
    _EditorApp.prototype.setRegionOrder = function (regionNames) {     // 7415
      return this._orderedRegions = regionNames.slice();               // 7416
    };                                                                 //
                                                                       //
    _EditorApp.prototype.start = function () {                         // 7419
      var domRegion, i, name, _i, _len, _ref;                          // 7420
      this.busy(true);                                                 // 7421
      this._regions = {};                                              // 7422
      this._orderedRegions = [];                                       // 7423
      _ref = this._domRegions;                                         // 7424
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {      // 7425
        domRegion = _ref[i];                                           // 7426
        name = domRegion.getAttribute(this._namingProp);               // 7427
        if (!name) {                                                   // 7428
          name = i;                                                    // 7429
        }                                                              //
        this._regions[name] = new ContentEdit.Region(domRegion);       // 7431
        this._orderedRegions.push(name);                               // 7432
        this._regionsLastModified[name] = this._regions[name].lastModified();
      }                                                                //
      this._preventEmptyRegions();                                     // 7435
      this._rootLastModified = ContentEdit.Root.get().lastModified();  // 7436
      this.history = new ContentTools.History(this._regions);          // 7437
      this.history.watch();                                            // 7438
      this._state = ContentTools.EditorApp.EDITING;                    // 7439
      this._toolbox.show();                                            // 7440
      this._inspector.show();                                          // 7441
      return this.busy(false);                                         // 7442
    };                                                                 //
                                                                       //
    _EditorApp.prototype.stop = function () {                          // 7445
      if (ContentEdit.Root.get().focused()) {                          // 7446
        ContentEdit.Root.get().focused().blur();                       // 7447
      }                                                                //
      this.history.stopWatching();                                     // 7449
      this.history = null;                                             // 7450
      this._toolbox.hide();                                            // 7451
      this._inspector.hide();                                          // 7452
      this._regions = {};                                              // 7453
      return this._state = ContentTools.EditorApp.READY;               // 7454
    };                                                                 //
                                                                       //
    _EditorApp.prototype._addDOMEventListeners = function () {         // 7457
      this._handleHighlightOn = (function (_this) {                    // 7458
        return function (ev) {                                         // 7459
          var _ref;                                                    // 7460
          if ((_ref = ev.keyCode) === 17 || _ref === 224) {            // 7461
            _this._ctrlDown = true;                                    // 7462
            return;                                                    // 7463
          }                                                            //
          if (ev.keyCode === 16) {                                     // 7465
            if (_this._highlightTimeout) {                             // 7466
              return;                                                  // 7467
            }                                                          //
            _this._shiftDown = true;                                   // 7469
            return _this._highlightTimeout = setTimeout(function () {  // 7470
              return _this.highlightRegions(true);                     // 7471
            }, ContentTools.HIGHLIGHT_HOLD_DURATION);                  //
          }                                                            //
        };                                                             //
      })(this);                                                        //
      this._handleHighlightOff = (function (_this) {                   // 7476
        return function (ev) {                                         // 7477
          var _ref;                                                    // 7478
          if ((_ref = ev.keyCode) === 17 || _ref === 224) {            // 7479
            _this._ctrlDown = false;                                   // 7480
            return;                                                    // 7481
          }                                                            //
          if (ev.keyCode === 16) {                                     // 7483
            _this._shiftDown = false;                                  // 7484
            if (_this._highlightTimeout) {                             // 7485
              clearTimeout(_this._highlightTimeout);                   // 7486
              _this._highlightTimeout = null;                          // 7487
            }                                                          //
            return _this.highlightRegions(false);                      // 7489
          }                                                            //
        };                                                             //
      })(this);                                                        //
      document.addEventListener('keydown', this._handleHighlightOn);   // 7493
      document.addEventListener('keyup', this._handleHighlightOff);    // 7494
      window.onbeforeunload = (function (_this) {                      // 7495
        return function (ev) {                                         // 7496
          if (_this._state === ContentTools.EditorApp.EDITING) {       // 7497
            return ContentEdit._('Your changes have not been saved, do you really want to lose them?');
          }                                                            //
        };                                                             //
      })(this);                                                        //
      return window.addEventListener('unload', (function (_this) {     // 7502
        return function (ev) {                                         // 7503
          return _this.destroy();                                      // 7504
        };                                                             //
      })(this));                                                       //
    };                                                                 //
                                                                       //
    _EditorApp.prototype._preventEmptyRegions = function () {          // 7509
      var name, placeholder, region, _ref, _results;                   // 7510
      _ref = this._regions;                                            // 7511
      _results = [];                                                   // 7512
      for (name in babelHelpers.sanitizeForInObject(_ref)) {           // 7513
        region = _ref[name];                                           // 7514
        if (region.children.length > 0) {                              // 7515
          continue;                                                    // 7516
        }                                                              //
        placeholder = new ContentEdit.Text('p', {}, '');               // 7518
        region.attach(placeholder);                                    // 7519
        _results.push(region.commit());                                // 7520
      }                                                                //
      return _results;                                                 // 7522
    };                                                                 //
                                                                       //
    _EditorApp.prototype._removeDOMEventListeners = function () {      // 7525
      document.removeEventListener('keydown', this._handleHighlightOn);
      return document.removeEventListener('keyup', this._handleHighlightOff);
    };                                                                 //
                                                                       //
    return _EditorApp;                                                 // 7530
  })(ContentTools.ComponentUI);                                        //
                                                                       //
  ContentTools.EditorApp = (function () {                              // 7534
    var instance;                                                      // 7535
                                                                       //
    function EditorApp() {}                                            // 7537
                                                                       //
    EditorApp.DORMANT = 'dormant';                                     // 7539
                                                                       //
    EditorApp.READY = 'ready';                                         // 7541
                                                                       //
    EditorApp.EDITING = 'editing';                                     // 7543
                                                                       //
    instance = null;                                                   // 7545
                                                                       //
    EditorApp.get = function () {                                      // 7547
      var cls;                                                         // 7548
      cls = ContentTools.EditorApp.getCls();                           // 7549
      return instance != null ? instance : instance = new cls();       // 7550
    };                                                                 //
                                                                       //
    EditorApp.getCls = function () {                                   // 7553
      return _EditorApp;                                               // 7554
    };                                                                 //
                                                                       //
    return EditorApp;                                                  // 7557
  })();                                                                //
                                                                       //
  ContentTools.History = (function () {                                // 7561
    function History(regions) {                                        // 7562
      this._lastSnapshotTaken = null;                                  // 7563
      this._regions = {};                                              // 7564
      this.replaceRegions(regions);                                    // 7565
      this._snapshotIndex = -1;                                        // 7566
      this._snapshots = [];                                            // 7567
      this._store();                                                   // 7568
    }                                                                  //
                                                                       //
    History.prototype.canRedo = function () {                          // 7571
      return this._snapshotIndex < this._snapshots.length - 1;         // 7572
    };                                                                 //
                                                                       //
    History.prototype.canUndo = function () {                          // 7575
      return this._snapshotIndex > 0;                                  // 7576
    };                                                                 //
                                                                       //
    History.prototype.length = function () {                           // 7579
      return this._snapshots.length;                                   // 7580
    };                                                                 //
                                                                       //
    History.prototype.snapshot = function () {                         // 7583
      return this._snapshots[this._snapshotIndex];                     // 7584
    };                                                                 //
                                                                       //
    History.prototype.goTo = function (index) {                        // 7587
      this._snapshotIndex = Math.min(this._snapshots.length - 1, Math.max(0, index));
      return this.snapshot();                                          // 7589
    };                                                                 //
                                                                       //
    History.prototype.redo = function () {                             // 7592
      return this.goTo(this._snapshotIndex + 1);                       // 7593
    };                                                                 //
                                                                       //
    History.prototype.replaceRegions = function (regions) {            // 7596
      var k, v, _results;                                              // 7597
      this._regions = {};                                              // 7598
      _results = [];                                                   // 7599
      for (k in babelHelpers.sanitizeForInObject(regions)) {           // 7600
        v = regions[k];                                                // 7601
        _results.push(this._regions[k] = v);                           // 7602
      }                                                                //
      return _results;                                                 // 7604
    };                                                                 //
                                                                       //
    History.prototype.restoreSelection = function (snapshot) {         // 7607
      var element, region;                                             // 7608
      if (!snapshot.selected) {                                        // 7609
        return;                                                        // 7610
      }                                                                //
      region = this._regions[snapshot.selected.region];                // 7612
      element = region.descendants()[snapshot.selected.element];       // 7613
      element.focus();                                                 // 7614
      if (element.selection && snapshot.selected.selection) {          // 7615
        return element.selection(snapshot.selected.selection);         // 7616
      }                                                                //
    };                                                                 //
                                                                       //
    History.prototype.stopWatching = function () {                     // 7620
      if (this._watchInterval) {                                       // 7621
        clearInterval(this._watchInterval);                            // 7622
      }                                                                //
      if (this._delayedStoreTimeout) {                                 // 7624
        return clearTimeout(this._delayedStoreTimeout);                // 7625
      }                                                                //
    };                                                                 //
                                                                       //
    History.prototype.undo = function () {                             // 7629
      return this.goTo(this._snapshotIndex - 1);                       // 7630
    };                                                                 //
                                                                       //
    History.prototype.watch = function () {                            // 7633
      var watch;                                                       // 7634
      this._lastSnapshotTaken = Date.now();                            // 7635
      watch = (function (_this) {                                      // 7636
        return function () {                                           // 7637
          var delayedStore, lastModified;                              // 7638
          lastModified = ContentEdit.Root.get().lastModified();        // 7639
          if (lastModified === null) {                                 // 7640
            return;                                                    // 7641
          }                                                            //
          if (lastModified > _this._lastSnapshotTaken) {               // 7643
            if (_this._delayedStoreRequested === lastModified) {       // 7644
              return;                                                  // 7645
            }                                                          //
            if (_this._delayedStoreTimeout) {                          // 7647
              clearTimeout(_this._delayedStoreTimeout);                // 7648
            }                                                          //
            delayedStore = function () {                               // 7650
              _this._lastSnapshotTaken = lastModified;                 // 7651
              return _this._store();                                   // 7652
            };                                                         //
            _this._delayedStoreRequested = lastModified;               // 7654
            return _this._delayedStoreTimeout = setTimeout(delayedStore, 500);
          }                                                            //
        };                                                             //
      })(this);                                                        //
      return this._watchInterval = setInterval(watch, 50);             // 7659
    };                                                                 //
                                                                       //
    History.prototype._store = function () {                           // 7662
      var element, name, other_region, region, snapshot, _ref, _ref1;  // 7663
      snapshot = {                                                     // 7664
        regions: {},                                                   // 7665
        selected: null                                                 // 7666
      };                                                               //
      _ref = this._regions;                                            // 7668
      for (name in babelHelpers.sanitizeForInObject(_ref)) {           // 7669
        region = _ref[name];                                           // 7670
        snapshot.regions[name] = region.html();                        // 7671
      }                                                                //
      element = ContentEdit.Root.get().focused();                      // 7673
      if (element) {                                                   // 7674
        snapshot.selected = {};                                        // 7675
        region = element.closest(function (node) {                     // 7676
          return node.constructor.name === 'Region';                   // 7677
        });                                                            //
        if (!region) {                                                 // 7679
          return;                                                      // 7680
        }                                                              //
        _ref1 = this._regions;                                         // 7682
        for (name in babelHelpers.sanitizeForInObject(_ref1)) {        // 7683
          other_region = _ref1[name];                                  // 7684
          if (region === other_region) {                               // 7685
            snapshot.selected.region = name;                           // 7686
            break;                                                     // 7687
          }                                                            //
        }                                                              //
        snapshot.selected.element = region.descendants().indexOf(element);
        if (element.selection) {                                       // 7691
          snapshot.selected.selection = element.selection();           // 7692
        }                                                              //
      }                                                                //
      if (this._snapshotIndex < this._snapshots.length - 1) {          // 7695
        this._snapshots = this._snapshots.slice(0, this._snapshotIndex + 1);
      }                                                                //
      this._snapshotIndex++;                                           // 7698
      return this._snapshots.splice(this._snapshotIndex, 0, snapshot);
    };                                                                 //
                                                                       //
    return History;                                                    // 7702
  })();                                                                //
                                                                       //
  ContentTools.StylePalette = (function () {                           // 7706
    function StylePalette() {}                                         // 7707
                                                                       //
    StylePalette._styles = [];                                         // 7709
                                                                       //
    StylePalette.add = function (styles) {                             // 7711
      return this._styles = this._styles.concat(styles);               // 7712
    };                                                                 //
                                                                       //
    StylePalette.styles = function (tagName) {                         // 7715
      if (tagName === void 0) {                                        // 7716
        return this._styles.slice();                                   // 7717
      }                                                                //
      return this._styles.filter(function (style) {                    // 7719
        if (!style._applicableTo) {                                    // 7720
          return true;                                                 // 7721
        }                                                              //
        return style._applicableTo.indexOf(tagName) !== -1;            // 7723
      });                                                              //
    };                                                                 //
                                                                       //
    return StylePalette;                                               // 7727
  })();                                                                //
                                                                       //
  ContentTools.Style = (function () {                                  // 7731
    function Style(name, cssClass, applicableTo) {                     // 7732
      this._name = name;                                               // 7733
      this._cssClass = cssClass;                                       // 7734
      if (applicableTo) {                                              // 7735
        this._applicableTo = applicableTo;                             // 7736
      } else {                                                         //
        this._applicableTo = null;                                     // 7738
      }                                                                //
    }                                                                  //
                                                                       //
    Style.prototype.applicableTo = function () {                       // 7742
      return this._applicableTo;                                       // 7743
    };                                                                 //
                                                                       //
    Style.prototype.cssClass = function () {                           // 7746
      return this._cssClass;                                           // 7747
    };                                                                 //
                                                                       //
    Style.prototype.name = function () {                               // 7750
      return this._name;                                               // 7751
    };                                                                 //
                                                                       //
    return Style;                                                      // 7754
  })();                                                                //
                                                                       //
  ContentTools.ToolShelf = (function () {                              // 7758
    function ToolShelf() {}                                            // 7759
                                                                       //
    ToolShelf._tools = {};                                             // 7761
                                                                       //
    ToolShelf.stow = function (cls, name) {                            // 7763
      return this._tools[name] = cls;                                  // 7764
    };                                                                 //
                                                                       //
    ToolShelf.fetch = function (name) {                                // 7767
      if (!this._tools[name]) {                                        // 7768
        throw new Error("`" + name + "` has not been stowed on the tool shelf");
      }                                                                //
      return this._tools[name];                                        // 7771
    };                                                                 //
                                                                       //
    return ToolShelf;                                                  // 7774
  })();                                                                //
                                                                       //
  ContentTools.Tool = (function () {                                   // 7778
    function Tool() {}                                                 // 7779
                                                                       //
    Tool.label = 'Tool';                                               // 7781
                                                                       //
    Tool.icon = 'tool';                                                // 7783
                                                                       //
    Tool.canApply = function (element, selection) {                    // 7785
      return false;                                                    // 7786
    };                                                                 //
                                                                       //
    Tool.isApplied = function (element, selection) {                   // 7789
      return false;                                                    // 7790
    };                                                                 //
                                                                       //
    Tool.apply = function (element, selection, callback) {             // 7793
      throw new Error('Not implemented');                              // 7794
    };                                                                 //
                                                                       //
    Tool._insertAt = function (element) {                              // 7797
      var insertIndex, insertNode;                                     // 7798
      insertNode = element;                                            // 7799
      if (insertNode.parent().constructor.name !== 'Region') {         // 7800
        insertNode = element.closest(function (node) {                 // 7801
          return node.parent().constructor.name === 'Region';          // 7802
        });                                                            //
      }                                                                //
      insertIndex = insertNode.parent().children.indexOf(insertNode) + 1;
      return [insertNode, insertIndex];                                // 7806
    };                                                                 //
                                                                       //
    return Tool;                                                       // 7809
  })();                                                                //
                                                                       //
  ContentTools.Tools.Bold = (function (_super) {                       // 7813
    __extends(Bold, _super);                                           // 7814
                                                                       //
    function Bold() {                                                  // 7816
      return Bold.__super__.constructor.apply(this, arguments);        // 7817
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Bold, 'bold');                         // 7820
                                                                       //
    Bold.label = 'Bold';                                               // 7822
                                                                       //
    Bold.icon = 'bold';                                                // 7824
                                                                       //
    Bold.tagName = 'b';                                                // 7826
                                                                       //
    Bold.canApply = function (element, selection) {                    // 7828
      if (!element.content) {                                          // 7829
        return false;                                                  // 7830
      }                                                                //
      return selection && !selection.isCollapsed();                    // 7832
    };                                                                 //
                                                                       //
    Bold.isApplied = function (element, selection) {                   // 7835
      var from, to, _ref;                                              // 7836
      if (element.content === void 0 || !element.content.length()) {   // 7837
        return false;                                                  // 7838
      }                                                                //
      _ref = selection.get(), from = _ref[0], to = _ref[1];            // 7840
      if (from === to) {                                               // 7841
        to += 1;                                                       // 7842
      }                                                                //
      return element.content.slice(from, to).hasTags(this.tagName, true);
    };                                                                 //
                                                                       //
    Bold.apply = function (element, selection, callback) {             // 7847
      var from, to, _ref;                                              // 7848
      element.storeState();                                            // 7849
      _ref = selection.get(), from = _ref[0], to = _ref[1];            // 7850
      if (this.isApplied(element, selection)) {                        // 7851
        element.content = element.content.unformat(from, to, new HTMLString.Tag(this.tagName));
      } else {                                                         //
        element.content = element.content.format(from, to, new HTMLString.Tag(this.tagName));
      }                                                                //
      element.updateInnerHTML();                                       // 7856
      element.taint();                                                 // 7857
      element.restoreState();                                          // 7858
      return callback(true);                                           // 7859
    };                                                                 //
                                                                       //
    return Bold;                                                       // 7862
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.Italic = (function (_super) {                     // 7866
    __extends(Italic, _super);                                         // 7867
                                                                       //
    function Italic() {                                                // 7869
      return Italic.__super__.constructor.apply(this, arguments);      // 7870
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Italic, 'italic');                     // 7873
                                                                       //
    Italic.label = 'Italic';                                           // 7875
                                                                       //
    Italic.icon = 'italic';                                            // 7877
                                                                       //
    Italic.tagName = 'i';                                              // 7879
                                                                       //
    return Italic;                                                     // 7881
  })(ContentTools.Tools.Bold);                                         //
                                                                       //
  ContentTools.Tools.Link = (function (_super) {                       // 7885
    __extends(Link, _super);                                           // 7886
                                                                       //
    function Link() {                                                  // 7888
      return Link.__super__.constructor.apply(this, arguments);        // 7889
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Link, 'link');                         // 7892
                                                                       //
    Link.label = 'Link';                                               // 7894
                                                                       //
    Link.icon = 'link';                                                // 7896
                                                                       //
    Link.tagName = 'a';                                                // 7898
                                                                       //
    Link.getHref = function (element, selection) {                     // 7900
      var c, from, selectedContent, tag, to, _i, _j, _len, _len1, _ref, _ref1, _ref2;
      if (element.constructor.name === 'Image') {                      // 7902
        if (element.a) {                                               // 7903
          return element.a.href;                                       // 7904
        }                                                              //
      } else {                                                         //
        _ref = selection.get(), from = _ref[0], to = _ref[1];          // 7907
        selectedContent = element.content.slice(from, to);             // 7908
        _ref1 = selectedContent.characters;                            // 7909
        for (_i = 0, _len = _ref1.length; _i < _len; _i++) {           // 7910
          c = _ref1[_i];                                               // 7911
          if (!c.hasTags('a')) {                                       // 7912
            continue;                                                  // 7913
          }                                                            //
          _ref2 = c.tags();                                            // 7915
          for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {       // 7916
            tag = _ref2[_j];                                           // 7917
            if (tag.name() === 'a') {                                  // 7918
              return tag.attr('href');                                 // 7919
            }                                                          //
          }                                                            //
        }                                                              //
      }                                                                //
      return '';                                                       // 7924
    };                                                                 //
                                                                       //
    Link.canApply = function (element, selection) {                    // 7927
      if (element.constructor.name === 'Image') {                      // 7928
        return true;                                                   // 7929
      } else {                                                         //
        return Link.__super__.constructor.canApply.call(this, element, selection);
      }                                                                //
    };                                                                 //
                                                                       //
    Link.isApplied = function (element, selection) {                   // 7935
      if (element.constructor.name === 'Image') {                      // 7936
        return element.a;                                              // 7937
      } else {                                                         //
        return Link.__super__.constructor.isApplied.call(this, element, selection);
      }                                                                //
    };                                                                 //
                                                                       //
    Link.apply = function (element, selection, callback) {             // 7943
      var allowScrolling, app, applied, dialog, domElement, from, measureSpan, modal, rect, selectTag, to, transparent, _ref;
      applied = false;                                                 // 7945
      if (element.constructor.name === 'Image') {                      // 7946
        rect = element.domElement().getBoundingClientRect();           // 7947
      } else {                                                         //
        element.storeState();                                          // 7949
        selectTag = new HTMLString.Tag('span', {                       // 7950
          'class': 'ct--puesdo-select'                                 // 7951
        });                                                            //
        _ref = selection.get(), from = _ref[0], to = _ref[1];          // 7953
        element.content = element.content.format(from, to, selectTag);
        element.updateInnerHTML();                                     // 7955
        domElement = element.domElement();                             // 7956
        measureSpan = domElement.getElementsByClassName('ct--puesdo-select');
        rect = measureSpan[0].getBoundingClientRect();                 // 7958
      }                                                                //
      app = ContentTools.EditorApp.get();                              // 7960
      modal = new ContentTools.ModalUI(transparent = true, allowScrolling = true);
      modal.bind('click', function () {                                // 7962
        this.unmount();                                                // 7963
        dialog.hide();                                                 // 7964
        if (element.content) {                                         // 7965
          element.content = element.content.unformat(from, to, selectTag);
          element.updateInnerHTML();                                   // 7967
          element.restoreState();                                      // 7968
        }                                                              //
        return callback(applied);                                      // 7970
      });                                                              //
      dialog = new ContentTools.LinkDialog(this.getHref(element, selection));
      dialog.position([rect.left + rect.width / 2 + window.scrollX, rect.top + rect.height / 2 + window.scrollY]);
      dialog.bind('save', function (href) {                            // 7974
        var a;                                                         // 7975
        dialog.unbind('save');                                         // 7976
        applied = true;                                                // 7977
        if (element.constructor.name === 'Image') {                    // 7978
          if (href) {                                                  // 7979
            element.a = {                                              // 7980
              href: href                                               // 7981
            };                                                         //
          } else {                                                     //
            element.a = null;                                          // 7984
          }                                                            //
        } else {                                                       //
          element.content = element.content.unformat(from, to, 'a');   // 7987
          if (href) {                                                  // 7988
            a = new HTMLString.Tag('a', {                              // 7989
              href: href                                               // 7990
            });                                                        //
            element.content = element.content.format(from, to, a);     // 7992
          }                                                            //
          element.updateInnerHTML();                                   // 7994
          element.taint();                                             // 7995
        }                                                              //
        return modal.trigger('click');                                 // 7997
      });                                                              //
      app.attach(modal);                                               // 7999
      app.attach(dialog);                                              // 8000
      modal.show();                                                    // 8001
      return dialog.show();                                            // 8002
    };                                                                 //
                                                                       //
    return Link;                                                       // 8005
  })(ContentTools.Tools.Bold);                                         //
                                                                       //
  ContentTools.Tools.Heading = (function (_super) {                    // 8009
    __extends(Heading, _super);                                        // 8010
                                                                       //
    function Heading() {                                               // 8012
      return Heading.__super__.constructor.apply(this, arguments);     // 8013
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Heading, 'heading');                   // 8016
                                                                       //
    Heading.label = 'Heading';                                         // 8018
                                                                       //
    Heading.icon = 'heading';                                          // 8020
                                                                       //
    Heading.tagName = 'h1';                                            // 8022
                                                                       //
    Heading.canApply = function (element, selection) {                 // 8024
      return element.content !== void 0 && element.parent().constructor.name === 'Region';
    };                                                                 //
                                                                       //
    Heading.apply = function (element, selection, callback) {          // 8028
      var content, insertAt, parent, textElement;                      // 8029
      element.storeState();                                            // 8030
      if (element.constructor.name === 'PreText') {                    // 8031
        content = element.content.html().replace(/&nbsp;/g, ' ');      // 8032
        textElement = new ContentEdit.Text(this.tagName, {}, content);
        parent = element.parent();                                     // 8034
        insertAt = parent.children.indexOf(element);                   // 8035
        parent.detach(element);                                        // 8036
        parent.attach(textElement, insertAt);                          // 8037
        element.blur();                                                // 8038
        textElement.focus();                                           // 8039
        textElement.selection(selection);                              // 8040
      } else {                                                         //
        element.tagName(this.tagName);                                 // 8042
        element.restoreState();                                        // 8043
      }                                                                //
      return callback(true);                                           // 8045
    };                                                                 //
                                                                       //
    return Heading;                                                    // 8048
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.Subheading = (function (_super) {                 // 8052
    __extends(Subheading, _super);                                     // 8053
                                                                       //
    function Subheading() {                                            // 8055
      return Subheading.__super__.constructor.apply(this, arguments);  // 8056
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Subheading, 'subheading');             // 8059
                                                                       //
    Subheading.label = 'Subheading';                                   // 8061
                                                                       //
    Subheading.icon = 'subheading';                                    // 8063
                                                                       //
    Subheading.tagName = 'h2';                                         // 8065
                                                                       //
    return Subheading;                                                 // 8067
  })(ContentTools.Tools.Heading);                                      //
                                                                       //
  ContentTools.Tools.Paragraph = (function (_super) {                  // 8071
    __extends(Paragraph, _super);                                      // 8072
                                                                       //
    function Paragraph() {                                             // 8074
      return Paragraph.__super__.constructor.apply(this, arguments);   // 8075
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Paragraph, 'paragraph');               // 8078
                                                                       //
    Paragraph.label = 'Paragraph';                                     // 8080
                                                                       //
    Paragraph.icon = 'paragraph';                                      // 8082
                                                                       //
    Paragraph.tagName = 'p';                                           // 8084
                                                                       //
    Paragraph.canApply = function (element, selection) {               // 8086
      return element !== void 0;                                       // 8087
    };                                                                 //
                                                                       //
    Paragraph.apply = function (element, selection, callback) {        // 8090
      var app, forceAdd, paragraph, region;                            // 8091
      app = ContentTools.EditorApp.get();                              // 8092
      forceAdd = app.ctrlDown();                                       // 8093
      if (ContentTools.Tools.Heading.canApply(element) && !forceAdd) {
        return Paragraph.__super__.constructor.apply.call(this, element, selection, callback);
      } else {                                                         //
        if (element.parent().constructor.name !== 'Region') {          // 8097
          element = element.closest(function (node) {                  // 8098
            return node.parent().constructor.name === 'Region';        // 8099
          });                                                          //
        }                                                              //
        region = element.parent();                                     // 8102
        paragraph = new ContentEdit.Text('p');                         // 8103
        region.attach(paragraph, region.children.indexOf(element) + 1);
        paragraph.focus();                                             // 8105
        return callback(true);                                         // 8106
      }                                                                //
    };                                                                 //
                                                                       //
    return Paragraph;                                                  // 8110
  })(ContentTools.Tools.Heading);                                      //
                                                                       //
  ContentTools.Tools.Preformatted = (function (_super) {               // 8114
    __extends(Preformatted, _super);                                   // 8115
                                                                       //
    function Preformatted() {                                          // 8117
      return Preformatted.__super__.constructor.apply(this, arguments);
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Preformatted, 'preformatted');         // 8121
                                                                       //
    Preformatted.label = 'Preformatted';                               // 8123
                                                                       //
    Preformatted.icon = 'preformatted';                                // 8125
                                                                       //
    Preformatted.tagName = 'pre';                                      // 8127
                                                                       //
    Preformatted.apply = function (element, selection, callback) {     // 8129
      var insertAt, parent, preText, text;                             // 8130
      text = element.content.text();                                   // 8131
      preText = new ContentEdit.PreText('pre', {}, HTMLString.String.encode(text));
      parent = element.parent();                                       // 8133
      insertAt = parent.children.indexOf(element);                     // 8134
      parent.detach(element);                                          // 8135
      parent.attach(preText, insertAt);                                // 8136
      element.blur();                                                  // 8137
      preText.focus();                                                 // 8138
      preText.selection(selection);                                    // 8139
      return callback(true);                                           // 8140
    };                                                                 //
                                                                       //
    return Preformatted;                                               // 8143
  })(ContentTools.Tools.Heading);                                      //
                                                                       //
  ContentTools.Tools.AlignLeft = (function (_super) {                  // 8147
    __extends(AlignLeft, _super);                                      // 8148
                                                                       //
    function AlignLeft() {                                             // 8150
      return AlignLeft.__super__.constructor.apply(this, arguments);   // 8151
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(AlignLeft, 'align-left');              // 8154
                                                                       //
    AlignLeft.label = 'Align left';                                    // 8156
                                                                       //
    AlignLeft.icon = 'align-left';                                     // 8158
                                                                       //
    AlignLeft.className = 'text-left';                                 // 8160
                                                                       //
    AlignLeft.canApply = function (element, selection) {               // 8162
      return element.content !== void 0;                               // 8163
    };                                                                 //
                                                                       //
    AlignLeft.isApplied = function (element, selection) {              // 8166
      var _ref;                                                        // 8167
      if (!this.canApply(element)) {                                   // 8168
        return false;                                                  // 8169
      }                                                                //
      if ((_ref = element.constructor.name) === 'ListItemText' || _ref === 'TableCellText') {
        element = element.parent();                                    // 8172
      }                                                                //
      return element.hasCSSClass(this.className);                      // 8174
    };                                                                 //
                                                                       //
    AlignLeft.apply = function (element, selection, callback) {        // 8177
      var className, _i, _len, _ref, _ref1;                            // 8178
      if ((_ref = element.constructor.name) === 'ListItemText' || _ref === 'TableCellText') {
        element = element.parent();                                    // 8180
      }                                                                //
      _ref1 = ['text-center', 'text-left', 'text-right'];              // 8182
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {             // 8183
        className = _ref1[_i];                                         // 8184
        if (element.hasCSSClass(className)) {                          // 8185
          element.removeCSSClass(className);                           // 8186
          if (className === this.className) {                          // 8187
            return callback(true);                                     // 8188
          }                                                            //
        }                                                              //
      }                                                                //
      element.addCSSClass(this.className);                             // 8192
      return callback(true);                                           // 8193
    };                                                                 //
                                                                       //
    return AlignLeft;                                                  // 8196
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.AlignCenter = (function (_super) {                // 8200
    __extends(AlignCenter, _super);                                    // 8201
                                                                       //
    function AlignCenter() {                                           // 8203
      return AlignCenter.__super__.constructor.apply(this, arguments);
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(AlignCenter, 'align-center');          // 8207
                                                                       //
    AlignCenter.label = 'Align center';                                // 8209
                                                                       //
    AlignCenter.icon = 'align-center';                                 // 8211
                                                                       //
    AlignCenter.className = 'text-center';                             // 8213
                                                                       //
    return AlignCenter;                                                // 8215
  })(ContentTools.Tools.AlignLeft);                                    //
                                                                       //
  ContentTools.Tools.AlignRight = (function (_super) {                 // 8219
    __extends(AlignRight, _super);                                     // 8220
                                                                       //
    function AlignRight() {                                            // 8222
      return AlignRight.__super__.constructor.apply(this, arguments);  // 8223
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(AlignRight, 'align-right');            // 8226
                                                                       //
    AlignRight.label = 'Align right';                                  // 8228
                                                                       //
    AlignRight.icon = 'align-right';                                   // 8230
                                                                       //
    AlignRight.className = 'text-right';                               // 8232
                                                                       //
    return AlignRight;                                                 // 8234
  })(ContentTools.Tools.AlignLeft);                                    //
                                                                       //
  ContentTools.Tools.UnorderedList = (function (_super) {              // 8238
    __extends(UnorderedList, _super);                                  // 8239
                                                                       //
    function UnorderedList() {                                         // 8241
      return UnorderedList.__super__.constructor.apply(this, arguments);
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(UnorderedList, 'unordered-list');      // 8245
                                                                       //
    UnorderedList.label = 'Bullet list';                               // 8247
                                                                       //
    UnorderedList.icon = 'unordered-list';                             // 8249
                                                                       //
    UnorderedList.listTag = 'ul';                                      // 8251
                                                                       //
    UnorderedList.canApply = function (element, selection) {           // 8253
      var _ref;                                                        // 8254
      return element.content !== void 0 && ((_ref = element.parent().constructor.name) === 'Region' || _ref === 'ListItem');
    };                                                                 //
                                                                       //
    UnorderedList.apply = function (element, selection, callback) {    // 8258
      var insertAt, list, listItem, listItemText, parent;              // 8259
      if (element.parent().constructor.name === 'ListItem') {          // 8260
        element.storeState();                                          // 8261
        list = element.closest(function (node) {                       // 8262
          return node.constructor.name === 'List';                     // 8263
        });                                                            //
        list.tagName(this.listTag);                                    // 8265
        element.restoreState();                                        // 8266
      } else {                                                         //
        listItemText = new ContentEdit.ListItemText(element.content.copy());
        listItem = new ContentEdit.ListItem();                         // 8269
        listItem.attach(listItemText);                                 // 8270
        list = new ContentEdit.List(this.listTag, {});                 // 8271
        list.attach(listItem);                                         // 8272
        parent = element.parent();                                     // 8273
        insertAt = parent.children.indexOf(element);                   // 8274
        parent.detach(element);                                        // 8275
        parent.attach(list, insertAt);                                 // 8276
        listItemText.focus();                                          // 8277
        listItemText.selection(selection);                             // 8278
      }                                                                //
      return callback(true);                                           // 8280
    };                                                                 //
                                                                       //
    return UnorderedList;                                              // 8283
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.OrderedList = (function (_super) {                // 8287
    __extends(OrderedList, _super);                                    // 8288
                                                                       //
    function OrderedList() {                                           // 8290
      return OrderedList.__super__.constructor.apply(this, arguments);
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(OrderedList, 'ordered-list');          // 8294
                                                                       //
    OrderedList.label = 'Numbers list';                                // 8296
                                                                       //
    OrderedList.icon = 'ordered-list';                                 // 8298
                                                                       //
    OrderedList.listTag = 'ol';                                        // 8300
                                                                       //
    return OrderedList;                                                // 8302
  })(ContentTools.Tools.UnorderedList);                                //
                                                                       //
  ContentTools.Tools.Table = (function (_super) {                      // 8306
    __extends(Table, _super);                                          // 8307
                                                                       //
    function Table() {                                                 // 8309
      return Table.__super__.constructor.apply(this, arguments);       // 8310
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Table, 'table');                       // 8313
                                                                       //
    Table.label = 'Table';                                             // 8315
                                                                       //
    Table.icon = 'table';                                              // 8317
                                                                       //
    Table.canApply = function (element, selection) {                   // 8319
      return element !== void 0;                                       // 8320
    };                                                                 //
                                                                       //
    Table.apply = function (element, selection, callback) {            // 8323
      var app, dialog, modal, table;                                   // 8324
      if (element.storeState) {                                        // 8325
        element.storeState();                                          // 8326
      }                                                                //
      app = ContentTools.EditorApp.get();                              // 8328
      modal = new ContentTools.ModalUI();                              // 8329
      table = element.closest(function (node) {                        // 8330
        return node && node.constructor.name === 'Table';              // 8331
      });                                                              //
      dialog = new ContentTools.TableDialog(table);                    // 8333
      dialog.bind('cancel', (function (_this) {                        // 8334
        return function () {                                           // 8335
          dialog.unbind('cancel');                                     // 8336
          modal.hide();                                                // 8337
          dialog.hide();                                               // 8338
          if (element.restoreState) {                                  // 8339
            element.restoreState();                                    // 8340
          }                                                            //
          return callback(false);                                      // 8342
        };                                                             //
      })(this));                                                       //
      dialog.bind('save', (function (_this) {                          // 8345
        return function (tableCfg) {                                   // 8346
          var index, keepFocus, node, _ref;                            // 8347
          dialog.unbind('save');                                       // 8348
          keepFocus = true;                                            // 8349
          if (table) {                                                 // 8350
            _this._updateTable(tableCfg, table);                       // 8351
            keepFocus = element.closest(function (node) {              // 8352
              return node && node.constructor.name === 'Table';        // 8353
            });                                                        //
          } else {                                                     //
            table = _this._createTable(tableCfg);                      // 8356
            _ref = _this._insertAt(element), node = _ref[0], index = _ref[1];
            node.parent().attach(table, index);                        // 8358
            keepFocus = false;                                         // 8359
          }                                                            //
          if (keepFocus) {                                             // 8361
            element.restoreState();                                    // 8362
          } else {                                                     //
            table.firstSection().children[0].children[0].children[0].focus();
          }                                                            //
          modal.hide();                                                // 8366
          dialog.hide();                                               // 8367
          return callback(true);                                       // 8368
        };                                                             //
      })(this));                                                       //
      app.attach(modal);                                               // 8371
      app.attach(dialog);                                              // 8372
      modal.show();                                                    // 8373
      return dialog.show();                                            // 8374
    };                                                                 //
                                                                       //
    Table._adjustColumns = function (section, columns) {               // 8377
      var cell, cellTag, cellText, currentColumns, diff, i, row, _i, _len, _ref, _results;
      _ref = section.children;                                         // 8379
      _results = [];                                                   // 8380
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {              // 8381
        row = _ref[_i];                                                // 8382
        cellTag = row.children[0].tagName();                           // 8383
        currentColumns = row.children.length;                          // 8384
        diff = columns - currentColumns;                               // 8385
        if (diff < 0) {                                                // 8386
          _results.push((function () {                                 // 8387
            var _j, _results1;                                         // 8388
            _results1 = [];                                            // 8389
            for (i = _j = diff; diff <= 0 ? _j < 0 : _j > 0; i = diff <= 0 ? ++_j : --_j) {
              cell = row.children[row.children.length - 1];            // 8391
              _results1.push(row.detach(cell));                        // 8392
            }                                                          //
            return _results1;                                          // 8394
          })());                                                       //
        } else if (diff > 0) {                                         //
          _results.push((function () {                                 // 8397
            var _j, _results1;                                         // 8398
            _results1 = [];                                            // 8399
            for (i = _j = 0; 0 <= diff ? _j < diff : _j > diff; i = 0 <= diff ? ++_j : --_j) {
              cell = new ContentEdit.TableCell(cellTag);               // 8401
              row.attach(cell);                                        // 8402
              cellText = new ContentEdit.TableCellText('');            // 8403
              _results1.push(cell.attach(cellText));                   // 8404
            }                                                          //
            return _results1;                                          // 8406
          })());                                                       //
        } else {                                                       //
          _results.push(void 0);                                       // 8409
        }                                                              //
      }                                                                //
      return _results;                                                 // 8412
    };                                                                 //
                                                                       //
    Table._createTable = function (tableCfg) {                         // 8415
      var body, foot, head, table;                                     // 8416
      table = new ContentEdit.Table();                                 // 8417
      if (tableCfg.head) {                                             // 8418
        head = this._createTableSection('thead', 'th', tableCfg.columns);
        table.attach(head);                                            // 8420
      }                                                                //
      body = this._createTableSection('tbody', 'td', tableCfg.columns);
      table.attach(body);                                              // 8423
      if (tableCfg.foot) {                                             // 8424
        foot = this._createTableSection('tfoot', 'td', tableCfg.columns);
        table.attach(foot);                                            // 8426
      }                                                                //
      return table;                                                    // 8428
    };                                                                 //
                                                                       //
    Table._createTableSection = function (sectionTag, cellTag, columns) {
      var cell, cellText, i, row, section, _i;                         // 8432
      section = new ContentEdit.TableSection(sectionTag);              // 8433
      row = new ContentEdit.TableRow();                                // 8434
      section.attach(row);                                             // 8435
      for (i = _i = 0; 0 <= columns ? _i < columns : _i > columns; i = 0 <= columns ? ++_i : --_i) {
        cell = new ContentEdit.TableCell(cellTag);                     // 8437
        row.attach(cell);                                              // 8438
        cellText = new ContentEdit.TableCellText('');                  // 8439
        cell.attach(cellText);                                         // 8440
      }                                                                //
      return section;                                                  // 8442
    };                                                                 //
                                                                       //
    Table._updateTable = function (tableCfg, table) {                  // 8445
      var columns, foot, head, section, _i, _len, _ref;                // 8446
      if (!tableCfg.head && table.thead()) {                           // 8447
        table.detach(table.thead());                                   // 8448
      }                                                                //
      if (!tableCfg.foot && table.tfoot()) {                           // 8450
        table.detach(table.tfoot());                                   // 8451
      }                                                                //
      columns = table.firstSection().children[0].children.length;      // 8453
      if (tableCfg.columns !== columns) {                              // 8454
        _ref = table.children;                                         // 8455
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {            // 8456
          section = _ref[_i];                                          // 8457
          this._adjustColumns(section, tableCfg.columns);              // 8458
        }                                                              //
      }                                                                //
      if (tableCfg.head && !table.thead()) {                           // 8461
        head = this._createTableSection('thead', 'th', tableCfg.columns);
        table.attach(head);                                            // 8463
      }                                                                //
      if (tableCfg.foot && !table.tfoot()) {                           // 8465
        foot = this._createTableSection('tfoot', 'td', tableCfg.columns);
        return table.attach(foot);                                     // 8467
      }                                                                //
    };                                                                 //
                                                                       //
    return Table;                                                      // 8471
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.Indent = (function (_super) {                     // 8475
    __extends(Indent, _super);                                         // 8476
                                                                       //
    function Indent() {                                                // 8478
      return Indent.__super__.constructor.apply(this, arguments);      // 8479
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Indent, 'indent');                     // 8482
                                                                       //
    Indent.label = 'Indent';                                           // 8484
                                                                       //
    Indent.icon = 'indent';                                            // 8486
                                                                       //
    Indent.canApply = function (element, selection) {                  // 8488
      return element.parent().constructor.name === 'ListItem' && element.parent().parent().children.indexOf(element.parent()) > 0;
    };                                                                 //
                                                                       //
    Indent.apply = function (element, selection, callback) {           // 8492
      element.parent().indent();                                       // 8493
      return callback(true);                                           // 8494
    };                                                                 //
                                                                       //
    return Indent;                                                     // 8497
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.Unindent = (function (_super) {                   // 8501
    __extends(Unindent, _super);                                       // 8502
                                                                       //
    function Unindent() {                                              // 8504
      return Unindent.__super__.constructor.apply(this, arguments);    // 8505
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Unindent, 'unindent');                 // 8508
                                                                       //
    Unindent.label = 'Unindent';                                       // 8510
                                                                       //
    Unindent.icon = 'unindent';                                        // 8512
                                                                       //
    Unindent.canApply = function (element, selection) {                // 8514
      return element.parent().constructor.name === 'ListItem';         // 8515
    };                                                                 //
                                                                       //
    Unindent.apply = function (element, selection, callback) {         // 8518
      element.parent().unindent();                                     // 8519
      return callback(true);                                           // 8520
    };                                                                 //
                                                                       //
    return Unindent;                                                   // 8523
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.LineBreak = (function (_super) {                  // 8527
    __extends(LineBreak, _super);                                      // 8528
                                                                       //
    function LineBreak() {                                             // 8530
      return LineBreak.__super__.constructor.apply(this, arguments);   // 8531
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(LineBreak, 'line-break');              // 8534
                                                                       //
    LineBreak.label = 'Line break';                                    // 8536
                                                                       //
    LineBreak.icon = 'line-break';                                     // 8538
                                                                       //
    LineBreak.canApply = function (element, selection) {               // 8540
      return element.content;                                          // 8541
    };                                                                 //
                                                                       //
    LineBreak.apply = function (element, selection, callback) {        // 8544
      var br, cursor, tail, tip;                                       // 8545
      cursor = selection.get()[0] + 1;                                 // 8546
      tip = element.content.substring(0, selection.get()[0]);          // 8547
      tail = element.content.substring(selection.get()[1]);            // 8548
      br = new HTMLString.String('<br>', element.content.preserveWhitespace());
      element.content = tip.concat(br, tail);                          // 8550
      element.updateInnerHTML();                                       // 8551
      element.taint();                                                 // 8552
      selection.set(cursor, cursor);                                   // 8553
      element.selection(selection);                                    // 8554
      return callback(true);                                           // 8555
    };                                                                 //
                                                                       //
    return LineBreak;                                                  // 8558
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.Image = (function (_super) {                      // 8562
    __extends(Image, _super);                                          // 8563
                                                                       //
    function Image() {                                                 // 8565
      return Image.__super__.constructor.apply(this, arguments);       // 8566
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Image, 'image');                       // 8569
                                                                       //
    Image.label = 'Image';                                             // 8571
                                                                       //
    Image.icon = 'image';                                              // 8573
                                                                       //
    Image.canApply = function (element, selection) {                   // 8575
      return true;                                                     // 8576
    };                                                                 //
                                                                       //
    Image.apply = function (element, selection, callback) {            // 8579
      var app, dialog, modal;                                          // 8580
      if (element.storeState) {                                        // 8581
        element.storeState();                                          // 8582
      }                                                                //
      app = ContentTools.EditorApp.get();                              // 8584
      modal = new ContentTools.ModalUI();                              // 8585
      dialog = new ContentTools.ImageDialog();                         // 8586
      dialog.bind('cancel', (function (_this) {                        // 8587
        return function () {                                           // 8588
          dialog.unbind('cancel');                                     // 8589
          modal.hide();                                                // 8590
          dialog.hide();                                               // 8591
          if (element.restoreState) {                                  // 8592
            element.restoreState();                                    // 8593
          }                                                            //
          return callback(false);                                      // 8595
        };                                                             //
      })(this));                                                       //
      dialog.bind('save', (function (_this) {                          // 8598
        return function (imageURL, imageSize, imageAttrs) {            // 8599
          var image, index, node, _ref;                                // 8600
          dialog.unbind('save');                                       // 8601
          if (!imageAttrs) {                                           // 8602
            imageAttrs = {};                                           // 8603
          }                                                            //
          imageAttrs.height = imageSize[1];                            // 8605
          imageAttrs.src = imageURL;                                   // 8606
          imageAttrs.width = imageSize[0];                             // 8607
          image = new ContentEdit.Image(imageAttrs);                   // 8608
          _ref = _this._insertAt(element), node = _ref[0], index = _ref[1];
          node.parent().attach(image, index);                          // 8610
          image.focus();                                               // 8611
          modal.hide();                                                // 8612
          dialog.hide();                                               // 8613
          return callback(true);                                       // 8614
        };                                                             //
      })(this));                                                       //
      app.attach(modal);                                               // 8617
      app.attach(dialog);                                              // 8618
      modal.show();                                                    // 8619
      return dialog.show();                                            // 8620
    };                                                                 //
                                                                       //
    return Image;                                                      // 8623
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.Video = (function (_super) {                      // 8627
    __extends(Video, _super);                                          // 8628
                                                                       //
    function Video() {                                                 // 8630
      return Video.__super__.constructor.apply(this, arguments);       // 8631
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Video, 'video');                       // 8634
                                                                       //
    Video.label = 'Video';                                             // 8636
                                                                       //
    Video.icon = 'video';                                              // 8638
                                                                       //
    Video.canApply = function (element, selection) {                   // 8640
      return true;                                                     // 8641
    };                                                                 //
                                                                       //
    Video.apply = function (element, selection, callback) {            // 8644
      var app, dialog, modal;                                          // 8645
      if (element.storeState) {                                        // 8646
        element.storeState();                                          // 8647
      }                                                                //
      app = ContentTools.EditorApp.get();                              // 8649
      modal = new ContentTools.ModalUI();                              // 8650
      dialog = new ContentTools.VideoDialog();                         // 8651
      dialog.bind('cancel', (function (_this) {                        // 8652
        return function () {                                           // 8653
          dialog.unbind('cancel');                                     // 8654
          modal.hide();                                                // 8655
          dialog.hide();                                               // 8656
          if (element.restoreState) {                                  // 8657
            element.restoreState();                                    // 8658
          }                                                            //
          return callback(false);                                      // 8660
        };                                                             //
      })(this));                                                       //
      dialog.bind('save', (function (_this) {                          // 8663
        return function (videoURL) {                                   // 8664
          var index, node, video, _ref;                                // 8665
          dialog.unbind('save');                                       // 8666
          if (videoURL) {                                              // 8667
            video = new ContentEdit.Video('iframe', {                  // 8668
              'frameborder': 0,                                        // 8669
              'height': ContentTools.DEFAULT_VIDEO_HEIGHT,             // 8670
              'src': videoURL,                                         // 8671
              'width': ContentTools.DEFAULT_VIDEO_WIDTH                // 8672
            });                                                        //
            _ref = _this._insertAt(element), node = _ref[0], index = _ref[1];
            node.parent().attach(video, index);                        // 8675
            video.focus();                                             // 8676
          } else {                                                     //
            if (element.restoreState) {                                // 8678
              element.restoreState();                                  // 8679
            }                                                          //
          }                                                            //
          modal.hide();                                                // 8682
          dialog.hide();                                               // 8683
          return callback(videoURL !== '');                            // 8684
        };                                                             //
      })(this));                                                       //
      app.attach(modal);                                               // 8687
      app.attach(dialog);                                              // 8688
      modal.show();                                                    // 8689
      return dialog.show();                                            // 8690
    };                                                                 //
                                                                       //
    return Video;                                                      // 8693
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.Undo = (function (_super) {                       // 8697
    __extends(Undo, _super);                                           // 8698
                                                                       //
    function Undo() {                                                  // 8700
      return Undo.__super__.constructor.apply(this, arguments);        // 8701
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Undo, 'undo');                         // 8704
                                                                       //
    Undo.label = 'Undo';                                               // 8706
                                                                       //
    Undo.icon = 'undo';                                                // 8708
                                                                       //
    Undo.canApply = function (element, selection) {                    // 8710
      var app;                                                         // 8711
      app = ContentTools.EditorApp.get();                              // 8712
      return app.history && app.history.canUndo();                     // 8713
    };                                                                 //
                                                                       //
    Undo.apply = function (element, selection, callback) {             // 8716
      var app, snapshot;                                               // 8717
      app = ContentTools.EditorApp.get();                              // 8718
      app.history.stopWatching();                                      // 8719
      snapshot = app.history.undo();                                   // 8720
      app.revertToSnapshot(snapshot);                                  // 8721
      return app.history.watch();                                      // 8722
    };                                                                 //
                                                                       //
    return Undo;                                                       // 8725
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.Redo = (function (_super) {                       // 8729
    __extends(Redo, _super);                                           // 8730
                                                                       //
    function Redo() {                                                  // 8732
      return Redo.__super__.constructor.apply(this, arguments);        // 8733
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Redo, 'redo');                         // 8736
                                                                       //
    Redo.label = 'Redo';                                               // 8738
                                                                       //
    Redo.icon = 'redo';                                                // 8740
                                                                       //
    Redo.canApply = function (element, selection) {                    // 8742
      var app;                                                         // 8743
      app = ContentTools.EditorApp.get();                              // 8744
      return app.history && app.history.canRedo();                     // 8745
    };                                                                 //
                                                                       //
    Redo.apply = function (element, selection, callback) {             // 8748
      var app, snapshot;                                               // 8749
      app = ContentTools.EditorApp.get();                              // 8750
      app.history.stopWatching();                                      // 8751
      snapshot = app.history.redo();                                   // 8752
      app.revertToSnapshot(snapshot);                                  // 8753
      return app.history.watch();                                      // 8754
    };                                                                 //
                                                                       //
    return Redo;                                                       // 8757
  })(ContentTools.Tool);                                               //
                                                                       //
  ContentTools.Tools.Remove = (function (_super) {                     // 8761
    __extends(Remove, _super);                                         // 8762
                                                                       //
    function Remove() {                                                // 8764
      return Remove.__super__.constructor.apply(this, arguments);      // 8765
    }                                                                  //
                                                                       //
    ContentTools.ToolShelf.stow(Remove, 'remove');                     // 8768
                                                                       //
    Remove.label = 'Remove';                                           // 8770
                                                                       //
    Remove.icon = 'remove';                                            // 8772
                                                                       //
    Remove.canApply = function (element, selection) {                  // 8774
      return true;                                                     // 8775
    };                                                                 //
                                                                       //
    Remove.apply = function (element, selection, callback) {           // 8778
      var app, list, row, table;                                       // 8779
      app = ContentTools.EditorApp.get();                              // 8780
      element.blur();                                                  // 8781
      if (element.nextContent()) {                                     // 8782
        element.nextContent().focus();                                 // 8783
      } else if (element.previousContent()) {                          //
        element.previousContent().focus();                             // 8785
      }                                                                //
      switch (element.constructor.name) {                              // 8787
        case 'ListItemText':                                           // 8788
          if (app.ctrlDown()) {                                        // 8789
            list = element.closest(function (node) {                   // 8790
              return node.parent().constructor.name === 'Region';      // 8791
            });                                                        //
            list.parent().detach(list);                                // 8793
          } else {                                                     //
            element.parent().parent().detach(element.parent());        // 8795
          }                                                            //
          break;                                                       // 8797
        case 'TableCellText':                                          // 8798
          if (app.ctrlDown()) {                                        // 8799
            table = element.closest(function (node) {                  // 8800
              return node.constructor.name === 'Table';                // 8801
            });                                                        //
            table.parent().detach(table);                              // 8803
          } else {                                                     //
            row = element.parent().parent();                           // 8805
            row.parent().detach(row);                                  // 8806
          }                                                            //
          break;                                                       // 8808
        default:                                                       // 8809
          element.parent().detach(element);                            // 8810
          break;                                                       // 8811
      }                                                                // 8811
      return callback(true);                                           // 8813
    };                                                                 //
                                                                       //
    return Remove;                                                     // 8816
  })(ContentTools.Tool);                                               //
}).call(this);                                                         //
/////////////////////////////////////////////////////////////////////////

}).call(this);
