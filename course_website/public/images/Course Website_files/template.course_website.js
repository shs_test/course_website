(function(){
Template.body.addContent((function() {
  var view = this;
  return HTML.SCRIPT({
    src: "/app/client/assets/content-tools.min.js"
  });
}));
Meteor.startup(Template.body.renderToDocument);

Template.__checkName("nav_bar");
Template["nav_bar"] = new Template("Template.nav_bar", (function() {
  var view = this;
  return HTML.DIV({
    id: "nav_container"
  }, "\n    ", HTML.UL({
    id: "nav_tabs"
  }, "\n      ", Spacebars.include(view.lookupTemplate("home_link")), "\n      ", Blaze.Each(function() {
    return Spacebars.call(view.lookup("course_links"));
  }, function() {
    return [ "\n        ", Spacebars.include(view.lookupTemplate("course_link")), "\n      " ];
  }), "\n    "), "\n  ");
}));

Template.__checkName("home_link");
Template["home_link"] = new Template("Template.home_link", (function() {
  var view = this;
  return HTML.LI({
    "class": "nav_item"
  }, HTML.A({
    "class": "nav_link",
    href: function() {
      return Spacebars.mustache(view.lookup("pathFor"), Spacebars.kw({
        route: "home"
      }));
    },
    onclick: "var active = document.getElementById('active_link'); if (active) active.id = '';this.id='active_link'"
  }, "HOME"));
}));

Template.__checkName("course_link");
Template["course_link"] = new Template("Template.course_link", (function() {
  var view = this;
  return HTML.LI({
    "class": "nav_item"
  }, HTML.A({
    "class": "nav_link",
    href: function() {
      return Spacebars.mustache(view.lookup("pathFor"), Spacebars.kw({
        route: "course"
      }));
    },
    onclick: "var active = document.getElementById('active_link'); if (active) active.id = '';this.id='active_link'"
  }, Blaze.View("lookup:text", function() {
    return Spacebars.mustache(view.lookup("text"));
  })));
}));

Template.__checkName("main_content");
Template["main_content"] = new Template("Template.main_content", (function() {
  var view = this;
  return Blaze.View("lookup:page_content", function() {
    return Spacebars.makeRaw(Spacebars.mustache(view.lookup("page_content")));
  });
}));

Template.__checkName("header");
Template["header"] = new Template("Template.header", (function() {
  var view = this;
  return Blaze.View("lookup:header_content", function() {
    return Spacebars.makeRaw(Spacebars.mustache(view.lookup("header_content")));
  });
}));

Template.__checkName("page");
Template["page"] = new Template("Template.page", (function() {
  var view = this;
  return Blaze.If(function() {
    return Spacebars.call(view.lookup("currentUser"));
  }, function() {
    return [ "\n    ", Blaze.If(function() {
      return Spacebars.dataMustache(view.lookup("isInRole"), "admin");
    }, function() {
      return [ "\n      ", HTML.DIV({
        "data-editable": "",
        "data-name": "page_title",
        id: "header"
      }, "\n        ", Spacebars.include(view.lookupTemplate("header")), "\n      "), "\n      ", Spacebars.include(view.lookupTemplate("nav_bar")), "\n      ", Spacebars.include(view.lookupTemplate("loginButtons")), "\n      ", HTML.DIV({
        "data-editable": "",
        "data-name": "page_content",
        id: "content"
      }, "\n        ", Spacebars.include(view.lookupTemplate("main_content")), "\n      "), "\n    " ];
    }, function() {
      return [ "\n      ", HTML.DIV({
        id: "header"
      }, "\n        ", Spacebars.include(view.lookupTemplate("header")), "\n      "), "\n      ", Spacebars.include(view.lookupTemplate("nav_bar")), "\n      ", Spacebars.include(view.lookupTemplate("loginButtons")), "\n      ", HTML.DIV({
        id: "content"
      }, "\n        ", Spacebars.include(view.lookupTemplate("main_content")), "\n      "), "\n    " ];
    }), "\n  " ];
  }, function() {
    return [ "\n    ", Spacebars.include(view.lookupTemplate("loginButtons")), "\n    ", HTML.DIV({
      id: "header"
    }, "\n      ", Spacebars.include(view.lookupTemplate("header")), "\n    "), "\n  ", HTML.DIV({
      id: "content"
    }, "\n  "), "\n    ", Spacebars.include(view.lookupTemplate("welcome")), "\n  " ];
  });
}));

Template.__checkName("welcome");
Template["welcome"] = new Template("Template.welcome", (function() {
  var view = this;
  return HTML.Raw('<div id="content">\n    <h1>Welcome</h1>\n    <h2>Please sign in</h2>\n  </div>');
}));

}).call(this);
