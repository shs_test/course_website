(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// course_website.js                                                   //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
// Content stores current content for all pages.                       //
Content = new Mongo.Collection("content");                             // 2
                                                                       //
if (Meteor.isClient) {                                                 // 4
  Meteor.subscribe('content');                                         // 5
  var editor = ContentTools.EditorApp.get();                           // 6
  ContentTools.StylePalette.add([new ContentTools.Style('Author', 'author', ['p'])]);
  editor.bind('save', function (regions) {                             // 10
    if (regions.page_title) {                                          // 11
      Session.set('edited_header', true);                              // 12
    }                                                                  //
    if (regions.page_content) {                                        // 14
      Session.set('edited_page', true);                                // 15
    }                                                                  //
    Meteor.call('save_content', Session.get('current_page'), regions.page_title, regions.page_content);
  });                                                                  //
                                                                       //
  Accounts.ui.config({                                                 // 20
    passwordSignupFields: "USERNAME_ONLY"                              // 21
  });                                                                  //
                                                                       //
  Template.nav_bar.helpers({                                           // 24
    course_links: [{ text: "WEB APPS I", _id: '1' }, { text: "WEB APPS II", _id: '2' }, { text: "ANDROID", _id: '3' }, { text: "ADVANCED TOPICS", _id: '4' }]
  });                                                                  //
                                                                       //
  Template.page.helpers({                                              // 33
    'page_content': function () {                                      // 34
      return Content.findOne({ page_name: this.page_name }).main_content;
    },                                                                 //
    'header_content': function () {                                    // 37
      return Content.findOne({ page_name: this.page_name }).header_content;
    }                                                                  //
  });                                                                  //
                                                                       //
  Template.page.rendered = function () {                               // 42
    editor.init('*[data-editable]', 'data-name');                      // 43
    var header = document.getElementById('header');                    // 44
    var content = Content.findOne({ page_name: Session.get('current_page') });
    header.innerHTML = content.header_content;                         // 46
    var page = document.getElementById('content');                     // 47
    content = Content.findOne({ page_name: Session.get('current_page') });
    page.innerHTML = content.main_content;                             // 49
  };                                                                   //
                                                                       //
  Template.main_content.helpers({                                      // 52
    'page_content': function () {                                      // 53
      var page = document.getElementById('content');                   // 54
      var content = Content.findOne({ page_name: Session.get('current_page') });
      page.innerHTML = content.main_content;                           // 56
    }                                                                  //
  });                                                                  //
                                                                       //
  Template.header.helpers({                                            // 60
    'header_content': function () {                                    // 61
      var header = document.getElementById('header');                  // 62
      console.log(Session.get('current_page'));                        // 63
      var content = Content.findOne({ page_name: Session.get('current_page') }, function () {
        ;                                                              // 65
        header.innerHTML = content.header_content;                     // 66
      });                                                              //
    }                                                                  //
  });                                                                  //
}                                                                      //
                                                                       //
if (Meteor.isServer) {                                                 // 73
  Meteor.publish('content', function () {                              // 74
    return Content.find();                                             // 75
  });                                                                  //
}                                                                      //
                                                                       //
Meteor.methods({                                                       // 79
  save_content: function (name, title, content) {                      // 80
    if (title) {                                                       // 81
      Content.update({ page_name: name }, { $set: { header_content: title } });
    }                                                                  //
    if (content) {                                                     // 84
      Content.update({ page_name: name }, { $set: { main_content: content } });
    }                                                                  //
  }                                                                    //
});                                                                    //
                                                                       //
Router.route('/', {                                                    // 90
  name: 'home',                                                        // 91
  template: 'page',                                                    // 92
  data: function () {                                                  // 93
                                                                       //
    if (Meteor.userId()) {                                             // 95
      Session.set('current_page', 'home');                             // 96
      return { page_name: 'home' };                                    // 97
    } else {                                                           //
      Session.set('current_page', 'welcome');                          // 99
      return { page_name: 'welcome' };                                 // 100
    }                                                                  //
  }                                                                    //
});                                                                    //
                                                                       //
Router.route('/course', {                                              // 105
  path: '/course/:_id',                                                // 106
  template: 'page',                                                    // 107
  data: function () {                                                  // 108
    if (Meteor.userId()) {                                             // 109
      Session.set('current_page', 'course_' + this.params._id);        // 110
      return { _id: this.params._id, page_name: 'course_' + this.params._id };
    } else {                                                           //
      Session.set('current_page', 'welcome');                          // 113
      return { page_name: 'welcome' };                                 // 114
    }                                                                  //
  }                                                                    //
});                                                                    //
/////////////////////////////////////////////////////////////////////////

}).call(this);
