// Content stores current content for all pages.
Content = new Mongo.Collection('content');

if (Meteor.isClient) {
  // The ContentTools WYSIWYG editor.
  var editor = null;

  Accounts.ui.config({
    passwordSignupFields: 'USERNAME_ONLY'
  });
  
  // Navigation bar.
  Template.nav_bar.helpers({
    course_links: [
      {text: 'HTML/CSS', _id: '1'},
      {text: 'INTRO TO JS', _id: '2'},
      {text: 'SQL', _id: '3'},
      {text: 'JQUERY', _id: '4'}
    ],
  });
  
  Template.home_link.helpers({
    colorLink: function () {
      var url = Session.get('current_page');
      if (Session.get('current_page') === 'home') {
        return 'color: red';
      }
      return 'color: white';
    },    
  });
  
  Template.course_link.helpers({
    colorLink: function () {
      if (Session.get('current_page') === 'course_' + this._id) {
        return 'color: red';
      }
      return 'color: white';
    }  
  });
  
  // Sets up the editor. 
  var initEditor = function () {
    if (!editor) {
      // Create and initialize the editor.
      editor = ContentTools.EditorApp.get();
      editor.init('*[data-editable]', 'data-name');
      // Add a new style.
      ContentTools.StylePalette.add([
        new ContentTools.Style('Author', 'author', ['p'])
      ]);
      // When we hit save, store page content in our database.
      editor.bind('save', function (regions) {
        Meteor.call('save_content', Session.get('current_page'), 
                    regions.page_content);
      });
      // Keep track of when we're editing a page.
      $('.ct-ignition__button--edit').click(function () {
        Session.set('editing', true);
      });
      $('.ct-ignition__button--confirm').click(function () {
        Session.set('editing', false);
      });
      $('.ct-ignition__button--cancel').click(function () {
        Session.set('editing', false);
      });
      // Disable navigation links while we're editing.
      $('.nav_link').on('click', function (e) {
        if (Session.get('editing')) {
          e.preventDefault();
        }
      });
    }
    // Update the editable regions when the page changes.
    editor._domRegions = $('[data-editable]');
  }
  
  // Get the content for the current page.
  Template.main_content.helpers({
    page_content: function () {
      // Look up page content in database and display it.
      Meteor.call('get_content', Session.get('current_page'), 
                  function (err, data) {
        Session.set('page_content', data.main_content);
      });
      return Session.get('page_content');
    },
    editable_page_content: function () {
      Meteor.call('get_content', Session.get('current_page'), 
                  function (err, data) {
        $('#page_content').html(
          '<div data-editable ' + 
          'data-name="page_content" id="content">' + 
          data.main_content + '</div>');
        initEditor();
      });
    }
  });
}

Meteor.methods({
  // Saves page content to the database.
  save_content: function (name, content) {
    if (content) {
      Content.update({page_name: name}, {$set: {main_content: content}});
    }
  },
  // Retrieves page content from the database.
  get_content: function (name) {
    return Content.findOne({page_name: name});
  }
});

// Iron router configuration.
Router.configure({
  layoutTemplate: 'layout'
});

Router.route('/', {
  name: 'home',
  template: 'page',
  data: function () {
    if (Meteor.userId()) {
      Session.set('current_page', 'home');
      return {page_name: 'home'};
    } else {
      Session.set('current_page', 'welcome');
      return {page_name: 'welcome'}; 
    }
  }
});

Router.route('/course', {
  path: '/course/:_id',
  template: 'page',
  data: function () {
    if (Meteor.userId()) {
      var course_name = 'course_' + this.params._id;
      Session.set('current_page', course_name);
      return {_id: this.params._id, page_name: course_name};
    } else {
      Session.set('current_page', 'welcome');
      return {page_name: 'welcome'}; 
    }
  }
});